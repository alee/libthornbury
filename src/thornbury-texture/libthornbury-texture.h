/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:thornbury-texture/libthornbury-texture.h
 * @title: TextureUtility
 * @short_description: Thornbury texture utility to display and manipulating the images.
 *
 * Texture utility is the library for displaying and manipulating pixel buffer
 * type data.
 */

#ifndef _THORNBURY_TEXTURE_H
#define _THORNBURY_TEXTURE_H

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include <gdk/gdk.h>
#include <cogl/cogl-framebuffer.h>
#include <gio/gio.h>


G_BEGIN_DECLS
/**
 * thornbury_image_created_cb:
 * @pImage: a #ClutterActor
 * @pError: GError pointer
 * @pUserData: User data passed to the function.
 *
 * A function callback on creation of an image .
 *
 */
typedef void(*thornbury_image_created_cb)(ClutterActor *pImage, GError *pError, gpointer pUserData);

/**
 * icontent_created_cb:
 * @pContent: a #ClutterContent
 * @pError: GError pointer
 * @pUserData: User data passed to the function
 *
 * A function callback on creation of content for image .
 *
 */
typedef void(*content_created_cb)(ClutterContent *pContent, GError *pError, gpointer pUserData);

ClutterActor *thornbury_texture_new (void);

ClutterActor *thornbury_texture_new_from_icon (GIcon *icon, gfloat width, gfloat height);
ClutterActor *thornbury_texture_create_sync (gchar *pFilePath, gfloat flWidth, gfloat flHeight, GError **pErr);
ClutterActor *thornbury_texture_create_from_resource_sync (const gchar *path, gfloat width, gfloat height, GError **error);
void thornbury_texture_create_async (gchar *pFilePath, gfloat flWidth, gfloat flHeight, gpointer pData, thornbury_image_created_cb pFunCB);

ClutterContent *thornbury_texture_create_content_sync (gchar *pFilePath, gfloat flWidth, gfloat flHeight, GError **pErr);
ClutterContent *thornbury_texture_create_content_from_resource_sync (const gchar *path, gfloat width, gfloat height, GError **error);
void thornbury_texture_create_content_async (gchar *pFilePath, gfloat flWidth, gfloat flHeight, gpointer pData, content_created_cb pFunc);

void thornbury_texture_set_from_file (ClutterActor *pBox, gchar *pFilePath, gfloat flWidth, gfloat flHeight, GError **pErr);
void thornbury_texture_set_from_resource (ClutterActor *box, const gchar *path, gfloat width, gfloat height, GError **error);
gboolean thornbury_texture_get_stage_screenshot(ClutterStage *pStage, glong inXStartPos, glong inYStartPos, glong inWidth, glong inHeight, gchar *pFilename);

G_END_DECLS

#endif /* _THORNBURY_TEXTURE_H */
