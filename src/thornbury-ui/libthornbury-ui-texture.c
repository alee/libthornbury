/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
/*  libthornbury-ui-texture.c
 *
 *  libthornbury-ui-texture.c
 */

#include "libthornbury-ui-texture.h"
#include <curl/curl.h>
#include <libsoup/soup.h>
#include <stdlib.h>
#include <string.h>

/***********************************************************************************
        @Macro Definitions
************************************************************************************/
#define UI_TEXTURE_DEBUG(...)   //g_print( __VA_ARGS__)
#define USE_CURL        FALSE


/************************************************************************************
 * Description of Fixes/Functionality:
-----------------------------------------------------------------------------------
        Description                             Date                    Name
        ----------                              ----                    ----
* 1) Added functionality to support texture 
*    creation/update from Url(http)          	16-Jan-2013             Abhiruchi
* 2) content and GFile freed 					25-Mar-2014				Abhiruchi	
************************************************************************************/



/*********************************************************************
 *internal functions 
 *********************************************************************/
#if USE_CURL
static gchar *pBuffer = NULL;
static size_t bufferSize = 0;
/********************************************************
 * Function : ui_texture_write_image_data_buffer
 * Description: using curl write image data 
 * Parameters:  vPtr, size, nmemb, vStream*
 * Return value: SoupMessage*
 ********************************************************/
static size_t ui_texture_write_image_data_buffer (void *vPtr, size_t size, size_t nmemb, void *vStream)
{
        if (pBuffer == NULL)
        {
                pBuffer = (gchar *) malloc (size * nmemb);
                memcpy (pBuffer, vPtr, size * nmemb);
        } else
        {
                pBuffer = (gchar *) realloc (pBuffer, bufferSize + (size * nmemb));
                memcpy (pBuffer + bufferSize, vPtr, size * nmemb);
        }
        bufferSize += (size * nmemb);

        return (size * nmemb);
}
#else
/********************************************************
 * Function : p_ui_texture_download_url_using_soup
 * Description: download url using soup 
 * Parameters:  pSession, pMethod, pUrl
 * Return value: SoupMessage*
 ********************************************************/
static SoupMessage* p_ui_texture_download_url_using_soup (SoupSession *pSession, const char *pMethod, const char *pUrl)
{
        SoupMessage *pMsg;
        const char *pHeader;
        pMsg = soup_message_new (pMethod, pUrl);
        //soup_message_set_flags (msg, SOUP_MESSAGE_NO_REDIRECT);

        soup_session_send_message (pSession, pMsg);

        //SoupMessageHeadersIter iter;
        //soup_message_headers_iter_init (&iter, pMsg->request_headers);

        if (SOUP_STATUS_IS_REDIRECTION (pMsg->status_code)) 
	{
                pHeader = soup_message_headers_get_one (pMsg->response_headers, "Location");
                if (pHeader) 
		{
                        SoupURI *pUri;
                        char *pUriString;
                        UI_TEXTURE_DEBUG ("  Redirect handling -> %s\n", pHeader);
                        pUri = soup_uri_new_with_base (soup_message_get_uri (pMsg), pHeader);
                        pUriString = soup_uri_to_string (pUri, FALSE);
                        p_ui_texture_download_url_using_soup (pSession, pMethod, pUriString);
                        g_free (pUriString);
                        soup_uri_free (pUri);
                }
        } 
	else if (SOUP_STATUS_IS_SUCCESSFUL (pMsg->status_code)) 
	{
                return pMsg;
        }
        return NULL;
}
#endif

/********************************************************
 * function : p_ui_texture_download_file
 * description: download url using soup/curl 
 * parameters:  puri*, basync
 * return value: soupmessage*
 ********************************************************/
static GdkPixbuf *p_ui_texture_download_file (const gchar *pUri, gboolean bAsync)
{
	GdkPixbufLoader *pLoader;
	GdkPixbuf *pixbuf = NULL;
	GError *err = NULL;

#if USE_CURL

	CURL *cpUrl;
	CURLcode res;

	/*Use CpUrl to fetch data */
	cpUrl = curl_easy_init ();
	if (cpUrl)
	{
		curl_easy_setopt (cpUrl, CURLOPT_URL, pUri);
		curl_easy_setopt (cpUrl, CURLOPT_WRITEFUNCTION, ui_texture_write_image_data_buffer);
		res = curl_easy_perform (cpUrl);
		curl_easy_cleanup (cpUrl);
		if (res != 0 ||pBuffer == NULL)
		{
			UI_TEXTURE_DEBUG ("%s: CpUrl perform failed -- %s\n", __FUNCTION__, curl_easy_strerror (res));
			return NULL;
		}
	} else
	{
		g_warning ("%s: CpUrl init failed\n", __FUNCTION__);
		return NULL;
	}
#else
	SoupMessage *pMessage = NULL;
	SoupSession *pSession = NULL;
	const char *pMethod = NULL;
	SoupURI *pUriParsed;
	pMethod = SOUP_METHOD_GET;
	//Check the URI Can be parsed
	pUriParsed = soup_uri_new (pUri);

	if (! pUriParsed) 
	{
		g_warning ("%s %d Couldn't parse the uri \n", __FUNCTION__, __LINE__);
		return NULL;
	}
	soup_uri_free (pUriParsed);

	/* timeout setting for soup -> SOUP_SESSION_IDLE_TIMEOUT, 30,*/
	if (bAsync) 
	{
		pSession = soup_session_async_new_with_options (
				SOUP_SESSION_ADD_FEATURE_BY_TYPE, SOUP_TYPE_COOKIE_JAR,
				SOUP_SESSION_USER_AGENT, "get ",
				NULL);
	} 
	else 
	{
		pSession = soup_session_sync_new_with_options (
				SOUP_SESSION_ADD_FEATURE_BY_TYPE, SOUP_TYPE_COOKIE_JAR,
				SOUP_SESSION_USER_AGENT, "get ",
				NULL);

	}

	pMessage = p_ui_texture_download_url_using_soup(pSession, pMethod, pUri);
	if(pMessage==NULL)
		return NULL;

#endif
	pLoader = gdk_pixbuf_loader_new ();
#if USE_CURL
	if (gdk_pixbuf_loader_write (pLoader, (const guchar *)pBuffer, bufferSize, &err) == FALSE)
#else
	if (gdk_pixbuf_loader_write (pLoader,(const guchar *)pMessage->response_body->data, pMessage->response_body->length, &err) == FALSE)
#endif
	{
		if(NULL != err)
		{
			g_warning ("%s %d: %s\n", __FUNCTION__, __LINE__, err->message);
			gdk_pixbuf_loader_close (pLoader, NULL);
			if(G_IS_OBJECT(pLoader))
				g_object_unref(pLoader);
			return NULL;
		}
	} else
	{
		if (gdk_pixbuf_loader_close (pLoader, &err) == FALSE)
		{
			if(NULL != err)
			{
				g_warning ("%s %d: %s\n", __FUNCTION__, __LINE__, err->message);
				if(G_IS_OBJECT(pLoader))
					g_object_unref(pLoader);
				return NULL;
			}
		} else
		{
			pixbuf = g_object_ref (gdk_pixbuf_loader_get_pixbuf (pLoader));
			if (pixbuf == NULL)
			{
				if(G_IS_OBJECT(pLoader))
					g_object_unref(pLoader);
				return NULL;
			}
		}
	}

#if USE_CURL

	bufferSize = 0;
	free (pBuffer);
	pBuffer = NULL;
#else
	soup_message_body_free (pMessage->response_body);
	if(G_IS_OBJECT(pLoader))
		g_object_unref(pLoader);

#endif
	return pixbuf;
}

/********************************************************
 * function : p_ui_texture_from_file
 * description: texture creation for a image file 
 * parameters:  pFile, inWidth, inHeight, bPreserveAspectRatio, bAsync
 * return value: ClutterActor*
 ********************************************************/
static ClutterActor *p_ui_texture_from_file(GFile *pFile, gint inWidth, gint inHeight, gboolean bPreserveAspectRatio, gboolean bAsync)
{
	gchar *pUri = NULL;
        GError *pErr = NULL;
	GdkPixbuf *pixbuf = NULL;
	ClutterActor *pBox = NULL;
	ClutterContent *pTexture = NULL;

	g_return_val_if_fail (pFile != NULL, NULL);

        pUri = g_file_get_path (pFile);
		
        /* if file path is proper, create the image */
	if (pUri == NULL)
		return NULL;

	UI_TEXTURE_DEBUG ("File uri = %s\n", pUri);

	/* if width/height is provided as 0, image will get created with original image size */
	if (inWidth == 0 || inHeight == 0)
	{
		pixbuf = gdk_pixbuf_new_from_file(pUri, &pErr);
		if (pixbuf == NULL || pErr != NULL)
		{
			g_debug ("%s\n", pErr->message);
			g_free (pUri);
			return NULL;
		}
	} 
	else
	{
		/* if width/height is non-zero, image will get scaled to given width/height */
		pixbuf = gdk_pixbuf_new_from_file_at_scale (pUri, inWidth, inHeight, bPreserveAspectRatio, &pErr);
		if (pixbuf == NULL || pErr != NULL)
		{
			g_debug ("%s %d: %s\n", __FUNCTION__, __LINE__, pErr->message);
			g_free (pUri);
			return NULL;
		}
	}

	pTexture = clutter_image_new ();
        UI_TEXTURE_DEBUG("width = %d, height = %d \n", gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf) );

	clutter_image_set_data(CLUTTER_IMAGE (pTexture),
			gdk_pixbuf_get_pixels (pixbuf), 
			gdk_pixbuf_get_has_alpha (pixbuf)? COGL_PIXEL_FORMAT_RGBA_8888 : COGL_PIXEL_FORMAT_RGB_888,
			gdk_pixbuf_get_width (pixbuf),
			gdk_pixbuf_get_height (pixbuf),
			gdk_pixbuf_get_rowstride (pixbuf),
			&pErr);
	/* if error in setting image data, return */
	if (pErr != NULL)
	{
		g_debug ("%s %d: %s\n", __FUNCTION__, __LINE__, pErr->message);
		if(G_IS_OBJECT(pixbuf))
	                g_object_unref (pixbuf);
		g_free (pUri);
		return NULL;
	}

	/* create the content box with image size and set content to it */
	pBox = clutter_actor_new ();
	clutter_actor_set_content (pBox, pTexture);

	clutter_actor_set_size (pBox, gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf));
	
	if(pTexture)
		g_object_unref(pTexture);

	if(G_IS_OBJECT(pixbuf))	
		g_object_unref (pixbuf);

	if(NULL != pUri)
		g_free (pUri);

	return pBox;
}

static GdkPixbuf *
create_ui_texture_from_resource (const gchar *path,
                                 gfloat width,
                                 gfloat height,
                                 GError **error)
{
  GdkPixbuf *pixbuf = NULL;

  g_return_val_if_fail (path != NULL, NULL);
  g_return_if_fail (error == NULL || *error == NULL);

  g_debug (G_STRLOC "from resource %s", path);

  if (width == 0 || height == 0)
    {
      pixbuf = gdk_pixbuf_new_from_resource (path, error);
    }
  else
    {
      pixbuf = gdk_pixbuf_new_from_resource_at_scale (path, width, height, FALSE, error);
    }

  return pixbuf;
}

/********************************************************
 * function : p_ui_texture_from_url
 * description: texture creation for a image url 
 * parameters:  pFileUrl, inWidth, inHeight, bPreserveAspectRatio, bAsync
 * return value: ClutterActor*
 ********************************************************/ 
static ClutterActor *p_ui_texture_from_url(GFile *pFileUrl, gint inWidth, gint inHeight, gboolean bPreserveAspectRatio, gboolean bAsync)
{
	gchar *pUri = NULL;
	GError *pErr = NULL;
	GdkPixbuf *pixbuf = NULL;
        GdkPixbuf *newPixbuf = NULL;
        ClutterActor *pBox = NULL;
	ClutterContent *pTexture = NULL;

	g_return_val_if_fail (pFileUrl != NULL, NULL);

	pUri = g_file_get_uri (pFileUrl);
        
	if(NULL == pUri)        
		return NULL;

        UI_TEXTURE_DEBUG("uri = %s\n", pUri);

        pixbuf = p_ui_texture_download_file (pUri, bAsync);
        if (pixbuf == NULL)
        {
                g_free (pUri);
                return NULL;
        }

        if (inWidth != 0 && inHeight != 0)
        {
                newPixbuf = gdk_pixbuf_scale_simple (pixbuf, inWidth, inHeight, GDK_INTERP_BILINEAR);
                if(G_IS_OBJECT(pixbuf))
                        g_object_unref (pixbuf);
        } else
        {
                newPixbuf = pixbuf;
        }

        pTexture = clutter_image_new ();
        UI_TEXTURE_DEBUG("width = %d, height = %d \n", gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf) );

        clutter_image_set_data(CLUTTER_IMAGE (pTexture),
                        gdk_pixbuf_get_pixels (newPixbuf),
                        gdk_pixbuf_get_has_alpha(newPixbuf) ? COGL_PIXEL_FORMAT_RGBA_8888 : COGL_PIXEL_FORMAT_RGB_888,
                        gdk_pixbuf_get_width (newPixbuf),
                        gdk_pixbuf_get_height (newPixbuf),
                        gdk_pixbuf_get_rowstride (newPixbuf),
                        &pErr);

	/* if error in setting image data, return */
        if (pErr != NULL)
        {
                g_warning ("%s %d: %s\n", __FUNCTION__, __LINE__, pErr->message);
                g_free (pUri);
                return NULL;
        }

        /* create the content box with image size and set content to it */
        pBox = clutter_actor_new ();
        clutter_actor_set_content (pBox, pTexture);
        clutter_actor_set_size (pBox, gdk_pixbuf_get_width (newPixbuf), gdk_pixbuf_get_height (newPixbuf));

	if(pTexture)
		g_object_unref(pTexture);
	if(G_IS_OBJECT(pixbuf))
        	g_object_unref (pixbuf);

        if(G_IS_OBJECT(newPixbuf))
                g_object_unref (newPixbuf);

	if(NULL != pUri)
                g_free (pUri);

	return pBox;
}

/********************************************************
 * function : v_ui_texture_update_image_data
 * description: content update from pixbuf 
 * parameters:  pImageActor*, pixbuf*, pUri*
 * return value: ClutterActor*
 ********************************************************/
static void v_ui_texture_update_image_data(ClutterActor *pImageActor, GdkPixbuf *pixbuf, gchar *pUri)
{
	ClutterContent *pTexture = NULL;
	GError *pErr = NULL;

	pTexture = clutter_actor_get_content(pImageActor);
	if(! CLUTTER_IS_IMAGE(pTexture))
	{
		g_warning("not a image actor\n");
		return;
	}
	UI_TEXTURE_DEBUG("width = %d, height = %d \n", gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf) );
	clutter_image_set_data(CLUTTER_IMAGE (pTexture), 
			gdk_pixbuf_get_pixels (pixbuf), 
			gdk_pixbuf_get_has_alpha (pixbuf)? COGL_PIXEL_FORMAT_RGBA_8888 : COGL_PIXEL_FORMAT_RGB_888, 
			gdk_pixbuf_get_width (pixbuf), 
			gdk_pixbuf_get_height (pixbuf), 
			gdk_pixbuf_get_rowstride (pixbuf), 
			&pErr);
	/* if error in setting image data, return */
	if (pErr != NULL)
	{
		g_warning ("%s %d: %s\n", __FUNCTION__, __LINE__, pErr->message);
		g_free (pUri);
		return;
	}

	/* update the content box with image size and set content to it */
	clutter_actor_set_size (pImageActor, gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf));

        if(G_IS_OBJECT(pixbuf))
          g_object_unref (pixbuf);

	if(NULL != pUri)
		g_free (pUri);
}

/********************************************************
 * function : v_ui_texture_update_from_file
 * description: texture creation from a file 
 * parameters:  pImageActor*, pFilePath, inWidth, inHeight, bPreserveAspectRatio, bAsync
 * return value: ClutterActor*
 ********************************************************/
static void v_ui_texture_update_from_file(ClutterActor *pImageActor, GFile *pFilePath, gint inWidth, gint inHeight, gboolean bPreserveAspectRatio, gboolean bAsync)
{
        GdkPixbuf *pixbuf = NULL;
        gchar *pUri = NULL;
        GError *pErr = NULL;

	g_return_if_fail (pFilePath != NULL);

	pUri = g_file_get_path (pFilePath);

	if (pUri == NULL)
		return;

         UI_TEXTURE_DEBUG ("File uri = %s\n", pUri);
	/* if file path is proper, create the image */

	/* if width/height is provided as 0, image will get created with original image size */
	if (inWidth == 0 || inHeight == 0)
	{
		pixbuf = gdk_pixbuf_new_from_file(pUri, &pErr);
	} 
	else
	{
		/* if width/height is non-zero, image will get scaled to given width/height */
		pixbuf = gdk_pixbuf_new_from_file_at_scale (pUri, inWidth, inHeight, bPreserveAspectRatio, &pErr);
	}
	/* if pixbuf is null/ return */
	if (pixbuf == NULL || pErr != NULL)
	{
		g_warning ("%s %d: %s\n", __FUNCTION__, __LINE__, pErr->message);
		g_free (pUri);
		return;
	}

	v_ui_texture_update_image_data(pImageActor, pixbuf, pUri);
}

/********************************************************
 * function : v_ui_texture_update_from_url
 * description: texture creation for a image url 
 * parameters:  pImageActor*, pFileUrl, inWidth, inHeight, bPreserveAspectRatio, bAsync
 * return value: 
 ********************************************************/
static void v_ui_texture_update_from_url(ClutterActor *pImageActor, GFile *pFileUrl, gint inWidth, gint inHeight, gboolean bPreserveAspectRatio, gboolean bAsync)
{
        GdkPixbuf *pixbuf = NULL;
        GdkPixbuf *newPixbuf = NULL;
        gchar *pUri = NULL;

	g_return_if_fail (pFileUrl != NULL);

	pUri = g_file_get_uri (pFileUrl);

        if(NULL == pUri)
                return;

        UI_TEXTURE_DEBUG("uri = %s\n", pUri);
        pixbuf = p_ui_texture_download_file (pUri, bAsync);
        if (pixbuf == NULL)
        {
                g_free (pUri);
                return;
        }

        if (inWidth == 0 && inHeight == 0)
        {
                newPixbuf = pixbuf;
        } else
        {
		newPixbuf = gdk_pixbuf_scale_simple (pixbuf, inWidth, inHeight, GDK_INTERP_BILINEAR);
        }

	/* if file path is proper, create the image */

	/* if width/height is provided as 0, image will get created with original image size */
	v_ui_texture_update_image_data(pImageActor, newPixbuf, pUri);

	if(G_IS_OBJECT(pixbuf))
		g_object_unref (pixbuf);
}

/**
 * thornbury_ui_texture_create_new_for_resource:
 * @path: Gresource Path of the image file from whom the texture needs to be
 *                created
 * @width: The width of the texture
 * @height: The height of the texture 
 * @error: output param for error, one can see error message if return value is NULL.
 *
 * Create a ClutterActor from the given image file having the given
 * height and width.The boolean value preserve_aspect_ratio behave 
 * in the same way as in the api gdk_pixbuf_new_from_file_at_scale. 
 * Also the scaling is done to width and height if width and height are non zero.
 * If either width or height is zero, then the texture
 * is created with width and height of the image.Possible errors with pixbuf are in the
 * GDK_PIXBUF_ERROR and G_RESOURCE_ERROR domains.
 *
 * Returns: (transfer full): The newly created #ClutterActor having the #ClutterImage
 *              with properties as specified
 */
ClutterActor *
thornbury_ui_texture_create_new_for_resource (const gchar *path,
                                              gfloat width, gfloat height,
                                              GError **error)
{
	/* box for image content */
	ClutterActor *box = NULL;
	GError *tmp_error = NULL;
	ClutterContent *texture = NULL;
	GdkPixbuf *pix_buf = NULL;

	g_return_val_if_fail (error == NULL || *error == NULL, NULL);
	g_return_val_if_fail (path != NULL, NULL);

	g_debug ("g_resource file path %s ", path);

	pix_buf = create_ui_texture_from_resource (path, width, height, &tmp_error);

	if (tmp_error != NULL)
	{
		g_propagate_error (error, tmp_error);
		return NULL;
	}

	/* create the content actor */
	texture = clutter_image_new ();

	/* set the content to image actor */
	clutter_image_set_data (CLUTTER_IMAGE (texture),
			gdk_pixbuf_get_pixels (pix_buf),
			gdk_pixbuf_get_has_alpha (pix_buf) ?
			COGL_PIXEL_FORMAT_RGBA_8888 :
			COGL_PIXEL_FORMAT_RGB_888,
			gdk_pixbuf_get_width (pix_buf),
			gdk_pixbuf_get_height (pix_buf),
			gdk_pixbuf_get_rowstride (pix_buf), &tmp_error);
	g_debug ("width = %d, height = %d", gdk_pixbuf_get_width (pix_buf), gdk_pixbuf_get_height (pix_buf));

	/* if error in setting image data, return */
	if (tmp_error != NULL)
	{
		g_object_unref (texture);
		g_object_unref (pix_buf);
		g_propagate_error (error, tmp_error);
		return NULL;
	}
	box = clutter_actor_new ();
	/* set content to actor */
	clutter_actor_set_content (box, texture);
	clutter_actor_set_size (box, gdk_pixbuf_get_width (pix_buf), gdk_pixbuf_get_height (pix_buf));

	g_debug ("width = %d, height = %d", gdk_pixbuf_get_width (pix_buf), gdk_pixbuf_get_height (pix_buf));

	g_object_unref (texture);
	g_object_unref (pix_buf);

	return box;
}
	

/**
 * thornbury_ui_texture_create_new:
 * @path: Path of the image file from whom the texture needs to be
 *                created
 * @width: The width of the texture
 * @height: The height of the texture
 * @preserve_aspect_ratio: Boolean value to indicate if the aspect
 *			    ratio is required	
 * @async: Boolean value to indicate if the texture creation should be
 *                 done as asynchronously
 * Returns: (transfer full): The newly created #ClutterActor having 
 *          the #ClutterImage with properties as specified
 *
 * Create a ClutterActor from the given image file having the given
 * height and width.The boolean value preserve_aspect_ratio behave 
 * in the same way as in the api gdk_pixbuf_new_from_file_at_scale. 
 * Also the scaleing is done to width
 * and height if width and height are non zero numbers)
 *
 */
ClutterActor *
thornbury_ui_texture_create_new (const gchar *path,
                                 gint width,
                                 gint height,
                                 gboolean preserve_aspect_ratio,
                                 gboolean async)
{
        GFile *pFile;
	ClutterActor *pBox = NULL;

	g_return_val_if_fail (path != NULL, NULL);

        pFile = g_file_new_for_commandline_arg (path);

        if (pFile == NULL)
	{
		g_warning ("%s: file Path is NULL\n", __FUNCTION__);
                return NULL;
	}

	// Its neither a file:// or a local path, check if its http://
        // TODO Do we need https and ftp ?!
	if (g_file_has_uri_scheme (pFile, "file"))
                pBox = p_ui_texture_from_file (pFile, width, height, preserve_aspect_ratio, async);
        else if (g_file_has_uri_scheme (pFile, "http"))
		pBox = p_ui_texture_from_url (pFile, width, height, preserve_aspect_ratio, async);

	if(pFile)
		g_object_unref(pFile);
	return pBox;
}

/**
 * thornbury_ui_texture_set_from_resource:
 * @image_actor: actor returned by "thornbury_ui_texture_create_new"
 * @path:Gresoucre Path of the image file from whom the texture needs to be
 *        created
 * @width: The width of the texture
 * @height: The height of the texture
 * @error: output param for error, one can see error message if return value is NULL.
 *
 * Updates the given ClutterActor with the given image file having the given
 * height and width.The boolean value preserve_aspect_ratio behave 
 * in the same way as in the api gdk_pixbuf_new_from_file_at_scale. 
 * Also the scaling is done to width and height if width and height are
 * non zero.If either width or height is zero, then the texture
 * is created with width and height of the image.
 */
void
thornbury_ui_texture_set_from_resource (ClutterActor *image_actor,
                                        const gchar *path, gfloat width,
                                        gfloat height, GError **error)
{
	GError *tmp_error = NULL;
	GdkPixbuf *pix_buf = NULL;

	g_return_if_fail (CLUTTER_IS_ACTOR (image_actor));
	g_return_if_fail (path != NULL);
	g_return_if_fail (error == NULL || *error == NULL);

	g_debug ("g_resource file path %s ", path);

	pix_buf = create_ui_texture_from_resource (path, width ,height ,&tmp_error);

	if (tmp_error != NULL)
	{
		g_propagate_error (error, tmp_error);
		tmp_error = NULL;
		return;
	}

	v_ui_texture_update_image_data (image_actor, pix_buf, NULL);
}

/**
 * thornbury_ui_texture_set_from_file:
 * @image_actor: actor returned by "thornbury_ui_texture_create_new"
 * @path: Path of the image file from whom the texture needs to be
 *        created
 * @width: The width of the texture
 * @height: The height of the texture
 * @preserve_aspect_ratio: Boolean value to indicate if the aspect
 *			    ratio is required	
 * @async: Boolean value to indicate if the texture creation should be
 *                 done as asynchronously
 *
 * Updates the given ClutterActor with the given image file having the given
 * height and width.The boolean value preserve_aspect_ratio behave 
 * in the same way as in the api gdk_pixbuf_new_from_file_at_scale. 
 * Also the scaleing is done to width
 * and height if width and height are non zero numbers)
 *
 */
void thornbury_ui_texture_set_from_file (ClutterActor *image_actor,
                                         const gchar *path,
                                         gint width,
                                         gint height,
                                         gboolean preserve_aspect_ratio,
                                         gboolean async)
{
        GFile *pFile;

	g_return_if_fail (image_actor != NULL);
	g_return_if_fail (path != NULL);

        /* box for image content */
        pFile = g_file_new_for_commandline_arg (path);
        if (pFile == NULL)
        {
                g_warning ("%s: file Path is NULL\n", __FUNCTION__);
                return;
        }

        // Its neither a file:// or a local path, check if its http://
        // TODO Do we need https and ftp ?!
        if (g_file_has_uri_scheme (pFile, "file"))
                v_ui_texture_update_from_file (image_actor, pFile, width, height, preserve_aspect_ratio, async);
        else if (g_file_has_uri_scheme (pFile, "http"))
                v_ui_texture_update_from_url (image_actor, pFile, width, height, preserve_aspect_ratio, async);

	if(pFile)
		g_object_unref(pFile);
}
