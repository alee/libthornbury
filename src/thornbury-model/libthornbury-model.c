/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* thornbury-model.c */
/**
 * ThornburyModel::ThornburyModel:
 * @Title:ThornburyModel
 * @Short_Description: Thornbury Model Provides the data to views.
 * @See_Also: #ClutterActor #ClutterModel
 * 
 * #ThornburyModel acts as a Model in the MVC architecture in Thornbury.Model notifies to view and controller if there are any changes
 * in the data through ThornburyModel.
 * ThornburyModel instance can be created using thornbury_list_model_newv() and thornbury_list_model_new() during which ThornburyModel 
 * needs to know the total number of columns,its type and the column name. It is the wrapper on #ClutterModel.
 * ## Sample C Code
 * |[<!-- language="C" -->
 * ThornburyModel *pModel = thornbury_list_model_new(COLUMN_LAST, //3
 *				G_TYPE_STRING,"Label",
 *				G_TYPE_INT,"internal data",
 *				G_TYPE_POINTER,"userData"
 *				);
 * 
 * //row can be added dynamically 
 * ClutterActor *g_object_data = NULL;
 * g_object_data = create_actor();
 * for(i = 0;i < 5;i ++) 
 *{
 * 
 * thornbury_model_append (pModel, THORNBURY_COLOUMN_FIRST_ACTOR,"string",// string
 *                               THORNBURY_COLOUMN_SECOND_DATA,2 ,//integer
 *                               THORNBURY_COLOUMN_THIRD_DATA, g_object_data, //any gobject 
 *                               -1);
 *}
 *]|
 * Each row can be modified with thornbury_model_insert(),and prepend with thornbury_model_prepend().
 * Row can also be removed from the model using thornbury_model_remove(). To get the total number of rows,use thornbury_model_get_n_rows().
 *
 * ## Traversing through the model
 * 
 * |[<!-- language="C" -->
 * ThornburyModelIter *iter = thornbury_model_get_first_iter(pModel);
 * while(thornbury_model_iter_is_last(pModel))
 *{
 *	//do something;
 *	thornbury_model_iter_next(pModel);
 *} 
 *]|
 *
*/
#include "libthornbury-model.h"
#include <glib-object.h>
#include <gobject/gvaluecollector.h>

enum
{
  PROPERTY_0,
  PROPERTY_COLUMN_CHANGED,
};

struct _ThornburyModelPrivate
{
};

G_DEFINE_TYPE_WITH_PRIVATE (ThornburyModel, thornbury_model, CLUTTER_TYPE_LIST_MODEL)

/********************************************************
 * Function : thornbury_model_get_property
 * Description: Gets the Property value 
 * Parameters: 
 * Return value: 
 ********************************************************/
static void
thornbury_model_get_property (GObject    *object,
		guint       property_id,
		GValue     *value,
		GParamSpec *pspec)
{
  switch (property_id)
  {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

/********************************************************
 * Function : thornbury_model_set_property
 * Description: sets the Property value 
 * Parameters: 
 * Return value: 
 ********************************************************/
static void
thornbury_model_set_property (GObject      *object,
		guint         property_id,
		const GValue *value,
		GParamSpec   *pspec)
{
  switch (property_id)
  {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

/********************************************************
 * Function : thornbury_model_dispose
 * Description: 
 * Parameters: 
 * Return value: 
 ********************************************************/
static void
thornbury_model_dispose (GObject *object)
{
	G_OBJECT_CLASS (thornbury_model_parent_class)->dispose (object);
}

/********************************************************
 * Function : thornbury_model_finalize
 * Description: 
 * Parameters: 
 * Return value: 
 ********************************************************/
static void
thornbury_model_finalize (GObject *object)
{
	G_OBJECT_CLASS (thornbury_model_parent_class)->finalize (object);
}

/********************************************************
 * Function : thornbury_model_class_init
 * Description: 
 * Parameters: 
 * Return value: 
 ********************************************************/
static void
thornbury_model_class_init (ThornburyModelClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->get_property = thornbury_model_get_property;
	object_class->set_property = thornbury_model_set_property;
	object_class->dispose = thornbury_model_dispose;
	object_class->finalize = thornbury_model_finalize;
	
}

/********************************************************
 * Function : thornbury_model_init
 * Description: 
 * Parameters: 
 * Return value: 
 ********************************************************/
static void
thornbury_model_init (ThornburyModel *self)
{
	//g_print("%s\n",__FUNCTION__);
	self->priv = thornbury_model_get_instance_private (self);
}
/**
 * thornbury_model_set_types :
 * @model               : Model Object reference, from which function is invoked
 * @n_columns           : total number of columns 
 * @types               : array of types
 *
 *
 */
void thornbury_model_set_types(ThornburyModel *model, guint n_columns, GType *types) 
{

	g_return_if_fail (THORNBURY_IS_MODEL (model));
	clutter_model_set_types(CLUTTER_MODEL(model),n_columns,types);
}
/**
 * thornbury_model_set_names:
 * @model: Object reference, from which function is invoked
 * @n_columns: total number of columns
 * @names: Array of names 
 * 
 */
void thornbury_model_set_names (ThornburyModel     *model,
		guint             n_columns,
		const gchar * const names[])
{
	g_return_if_fail (THORNBURY_IS_MODEL (model));
	clutter_model_set_names(CLUTTER_MODEL(model),n_columns,names);
}

#if 0
static guint 
_get_total_columns (ThornburyModel *model,va_list args)
{
        gint col = va_arg (args, gint);
        gint i;
        /* get total no of column changes */
        for (i = 0 ; col != -1; i++)
        {
                GType col_type;
                gchar *error = NULL;
                GValue value = G_VALUE_INIT;
                col_type = clutter_model_get_column_type (CLUTTER_MODEL(model), col);
                G_VALUE_COLLECT_INIT (&value, col_type, args, 0, &error);
                col = va_arg (args, gint);
                g_value_unset(&value);
        }
	return i;
}
#endif
/**
 * thornbury_model_append   :
 * @model: Object reference, from which function is invoked
 * @...: %-1 terminated column values of the row
 *
 * thornbury_model_append, appends rows to the model
 */
void thornbury_model_append(ThornburyModel     *model,
		...)
{
	va_list args;
	g_return_if_fail (THORNBURY_IS_MODEL (model));
	va_start (args, model);
	clutter_model_append_valist(CLUTTER_MODEL(model),args);	
	va_end (args);

}
/**
 * thornbury_model_appendv :
 * @model              : Object reference, from which function is invoked
 * @n_columns          : Total no of columns in the array
 * @columns            : column array
 * @values             : values of the corresponding array
 *
 * Appends row to #ThornburyModel  
 */
void thornbury_model_appendv(ThornburyModel     *model,
		guint n_columns,
                guint *columns,
                GValue *values)
{
	guint i;
	ThornburyModel *objInstance;

	g_return_if_fail (THORNBURY_IS_MODEL (model));
	g_return_if_fail (n_columns != 0);
	g_return_if_fail (columns != NULL);
	g_return_if_fail (values != NULL);

	clutter_model_appendv (CLUTTER_MODEL(model),n_columns,columns,values);
}

/**
 * thornbury_model_prepend  :
 * @model: Object reference, from which function is invoked
 * @...: %-1 terminated column values of the row
 *
 * Prepends row to #ThornburyModel
 */
void thornbury_model_prepend(ThornburyModel     *model,
                ...)
{
        va_list args;
	g_return_if_fail (THORNBURY_IS_MODEL (model));
	va_start (args, model);
        clutter_model_prepend_valist(CLUTTER_MODEL(model),args);
        va_end (args);

}

/**
 * thornbury_model_prependv :
 * @model               : Object reference, from which function is invoked
 * @n_columns           : Total no of columns in the array
 * @columns            : column array
 * @values             : values of the corresponding array
 *
 */
void thornbury_model_prependv (ThornburyModel *model,
		guint n_columns,
		guint *columns,
		GValue *values)
{
	guint i;
	ThornburyModel *objInstance;

	g_return_if_fail (THORNBURY_IS_MODEL (model));
	g_return_if_fail (n_columns != 0);
	g_return_if_fail (columns != NULL);
	g_return_if_fail (values != NULL);

	clutter_model_prependv (CLUTTER_MODEL(model),n_columns,columns,values);
}
/**
 * thornbury_model_insert   :
 * @model               : Object reference, from which function is invoked
 * @row                 : Value at which row needs to be inserted 
 * @...: %-1 terminated column values of the row
 *
 * inserts row to #ThornburyModel
 */
void thornbury_model_insert (ThornburyModel     *model,
		guint             row,
		...)
{
	va_list args;
	g_return_if_fail (THORNBURY_IS_MODEL (model));
	va_start (args, row);
        clutter_model_insert_valist(CLUTTER_MODEL(model),row,args);
        va_end (args);

}
/**
 * thornbury_model_insertv :
 * @model: Object reference, from which function is invoked
 * @n_columns: Total no of columns in the array
 * @columns: columns array
 * @values: values of the corresponding array
 *
 */
void thornbury_model_insertv (ThornburyModel *model,
		guint row,
		guint n_columns,
		guint *columns,
		GValue *values)
{
	guint i;
	ThornburyModel *objInstance;

	g_return_if_fail (THORNBURY_IS_MODEL (model));
	g_return_if_fail (n_columns != 0);
	g_return_if_fail (columns != NULL);
	g_return_if_fail (values != NULL);

	clutter_model_insertv(CLUTTER_MODEL(model),row,n_columns,columns,values);
}
/**
 * thornbury_model_insert_value :
 * @model               : Object reference, from which function is invoked
 * @row           	: Row of the model
 * @column            	: column of the row
 * @value             	: values of the corresponding array
 *
 */
void thornbury_model_insert_value (ThornburyModel *model,
							guint row,
							guint column,
							const GValue *value)
{
	ThornburyModel *objInstance;

	g_return_if_fail (THORNBURY_IS_MODEL (model));
	g_return_if_fail (value != NULL);

	clutter_model_insert_value(CLUTTER_MODEL(model),row,column,value);
}
/**
 * thornbury_model_remove :
 * @model             : Object reference, from which function is invoked
 * @row               : row to be removed.
 *
 * roller_container_get_property gets the property value
 */
void  thornbury_model_remove (ThornburyModel *model,
							guint row)
{
	g_return_if_fail (THORNBURY_IS_MODEL (model));
	clutter_model_remove (CLUTTER_MODEL(model),row);
}

/**
 * thornbury_model_get_n_rows :
 * @model: Object reference, from which function is invoked
 * 
 * gets the total number of items in the model 
 *
 * Returns: total number of rows in the model
 */
guint thornbury_model_get_n_rows (ThornburyModel     *model)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), 0);
	return clutter_model_get_n_rows (CLUTTER_MODEL(model));
}
/**
 * thornbury_model_get_n_columns :
 * @model              : Object reference, from which function is invoked
 *
 * Gets the total no of columns.
 *
 * Returns: total number of columns in the model
 */
guint thornbury_model_get_n_columns (ThornburyModel     *model)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), 0);
	return clutter_model_get_n_columns (CLUTTER_MODEL(model));
}
/**
 * thornbury_model_get_column_name :
 * @model              : Object reference, from which function is invoked
 * @column         : column number 
 *
 * Returns: Name of the column
 */
const gchar *thornbury_model_get_column_name (ThornburyModel     *model,
											guint column)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), 0);
	return clutter_model_get_column_name (CLUTTER_MODEL(model),column);
}
/**
 * thornbury_model_get_column_type :
 * @model               : Object reference, from which function is invoked
 * @column		: Clolumn number.
 *
 * Returns: Type of the column.
 */
GType thornbury_model_get_column_type    (ThornburyModel *model,
		guint column)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), G_TYPE_INVALID);
	return clutter_model_get_column_type (CLUTTER_MODEL(model),column);
}

/**
 * thornbury_model_get_first_iter :
 * @model              : Object reference, from which function is invoked
 *
 * Returns: (transfer full): row pointer to first row
 */
ThornburyModelIter *thornbury_model_get_first_iter(ThornburyModel     *model)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), NULL);
	return (ThornburyModelIter *)clutter_model_get_first_iter(CLUTTER_MODEL(model));
}
/**
 * thornbury_model_get_last_iter :
 * @model              : Object reference,
 *
 * gets the last row pointer
 *
 * Returns: (transfer full): row pointer to last row
 */
ThornburyModelIter *thornbury_model_get_last_iter(ThornburyModel     *model)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), NULL);
	return (ThornburyModelIter *)clutter_model_get_last_iter (CLUTTER_MODEL(model));
}
/**
 * thornbury_model_get_iter_at_row :
 * @model              : Object reference, from which function is invoked
 * @row               : row to get ite iter
 *
 * gets the row pointer to the specified row.
 *
 * Returns: (transfer full): row pointer to specified row
 * 
 */
ThornburyModelIter *thornbury_model_get_iter_at_row(ThornburyModel     *model,
											guint row)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), NULL);
	return (ThornburyModelIter *)clutter_model_get_iter_at_row(CLUTTER_MODEL(model),row);
}

/**
 * thornbury_model_set_sorting_column :
 * @model              : Object reference, from which function is invoked
 * @column               :set the sorting column in the model
 *
 * sorts a perticular column.
 */
void thornbury_model_set_sorting_column (ThornburyModel *model,
						gint column)
{
	g_return_if_fail (THORNBURY_IS_MODEL (model));
	clutter_model_set_sorting_column(CLUTTER_MODEL(model),column);
}
/**
 * thornbury_model_get_sorting_column :
 * @model              : Object reference, from which function is invoked
 *
 * Returns: column value where,Sorting opeartion performed 
 *
 */
gint thornbury_model_get_sorting_column (ThornburyModel *model)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), -1);
	return clutter_model_get_sorting_column (CLUTTER_MODEL(model));
}

/**
 * thornbury_model_foreach :
 * @model              : Object reference, from which function is invoked
 * @func               : function pointer
 * @user_data          : data to the function.
 *
 * 
 */
void thornbury_model_foreach (ThornburyModel     *model,
		ThornburyModelForeachFunc func,
		gpointer          user_data)
{
	g_return_if_fail (THORNBURY_IS_MODEL (model));
	clutter_model_foreach (CLUTTER_MODEL(model),(ClutterModelForeachFunc)func,user_data);
}
/**
 * thornbury_model_set_sort :
 * @model              : Object reference, from which function is invoked
 * @func         : function pointer
 * @user_data               : data to the function.
 * @notify               :
 *
 */
void thornbury_model_set_sort (ThornburyModel     *model,
							gint column,
							ThornburyModelSortFunc func,
							gpointer          user_data,
							GDestroyNotify    notify)
{
	g_return_if_fail (THORNBURY_IS_MODEL (model));
	clutter_model_set_sort (CLUTTER_MODEL(model),column,(ClutterModelSortFunc)func,user_data,notify);
}
/**
 * thornbury_model_set_filter :
 * @model              : Object reference, from which function is invoked
 * @func         : function pointer
 * @user_data               : data to the function.
 * @notify         :
 */
void thornbury_model_set_filter (ThornburyModel *model,
							ThornburyModelFilterFunc func,
							gpointer          user_data,
							GDestroyNotify    notify)
{
	g_return_if_fail (THORNBURY_IS_MODEL (model));
	clutter_model_set_filter(CLUTTER_MODEL(model),(ThornburyModelFilterFunc)func,user_data,notify);
}
/**
 * thornbury_model_get_filter_set :
 * @model              : Object reference,
 *
 * Returns: %TRUE/%FALSE
 */
gboolean thornbury_model_get_filter_set(ThornburyModel*model)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), FALSE);
	return clutter_model_get_filter_set(CLUTTER_MODEL(model));
}

/**
 * thornbury_model_resort :
 * @model              : Object reference, from which function is invoked
 *
 */
void thornbury_model_resort (ThornburyModel *model)
{
	g_return_if_fail (THORNBURY_IS_MODEL (model));
	clutter_model_resort(CLUTTER_MODEL(model));
}
/**
 * thornbury_model_filter_row :
 * @model              : Object reference,
 *
 * Returns: %TRUE/%FALSE
 * 
 */
gboolean thornbury_model_filter_row (ThornburyModel *model,
								guint row)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), TRUE);
	return clutter_model_filter_row(CLUTTER_MODEL(model),row);
}
/**
 * thornbury_model_filter_iter :
 * @model              : Object reference, from which function is invoked
 * @iter               :
 *
 * Returns: %TRUE/%FALSE
 */
gboolean thornbury_model_filter_iter (ThornburyModel *model,
		ThornburyModelIter *iter)
{
	g_return_val_if_fail (THORNBURY_IS_MODEL (model), TRUE);
	return clutter_model_filter_iter(CLUTTER_MODEL(model),(ClutterModelIter *)iter);
}

/* 
 * ThornburyModelIter Object
 *
 */

G_DEFINE_ABSTRACT_TYPE (ThornburyModelIter, thornbury_model_iter, CLUTTER_TYPE_MODEL_ITER)

/********************************************************
 * Function : thornbury_model_get_property
 * Description: Gets the Property value 
 * Parameters: 
 * Return value: 
 ********************************************************/
static void
thornbury_model_iter_get_property (GObject    *object,
		guint       property_id,
		GValue     *value,
		GParamSpec *pspec)
{
	switch (property_id)
	{
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

/********************************************************
 * Function : thornbury_model_get_property
 * Description: Gets the Property value 
 * Parameters: 
 * Return value: 
 ********************************************************/
static void
thornbury_model_iter_set_property (GObject      *object,
		guint         property_id,
		const GValue *value,
		GParamSpec   *pspec)
{
	switch (property_id)
	{
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

/********************************************************
 * Function : thornbury_model_get_property
 * Description: Gets the Property value 
 * Parameters: 
 * Return value: 
 ********************************************************/


static void
thornbury_model_iter_dispose (GObject *object)
{
	G_OBJECT_CLASS (thornbury_model_iter_parent_class)->dispose (object);
}

/********************************************************
 * Function : thornbury_model_get_property
 * Description: Gets the Property value 
 * Parameters: 
 * Return value: 
 ********************************************************/
static void
thornbury_model_iter_finalize (GObject *object)
{
	G_OBJECT_CLASS (thornbury_model_iter_parent_class)->finalize (object);
}

/********************************************************
 * Function : thornbury_model_get_property
 * Description: Gets the Property value 
 * Parameters: 
 * Return value: 
 ********************************************************/

static void
thornbury_model_iter_class_init (ThornburyModelIterClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->get_property = thornbury_model_iter_get_property;
	object_class->set_property = thornbury_model_iter_set_property;
	object_class->dispose = thornbury_model_iter_dispose;
	object_class->finalize = thornbury_model_iter_finalize;
}

/********************************************************
 * Function : thornbury_model_get_property
 * Description: Gets the Property value 
 * Parameters: 
 * Return value: 
 ********************************************************/
static void
thornbury_model_iter_init (ThornburyModelIter *self)
{
}

/**
 * thornbury_model_iter_get :
 * @iter:Pointer to iter.
 * @...: %NULL terminated column values of the row
 * 
 */
void thornbury_model_iter_get(ThornburyModelIter *iter,
                          ...)
{
	va_list args;
        va_start (args, iter);
	clutter_model_iter_get_valist(CLUTTER_MODEL_ITER(iter),args);
	va_end (args);
}
/**
 * thornbury_model_iter_get_valist :
 * @iter:Pointer to iter
 * @args:variable number of arguments.
 *
 * roller_container_get_property gets the property value
 */
void thornbury_model_iter_get_valist(ThornburyModelIter *iter,
                                 va_list args)
{
	clutter_model_iter_get_valist ((ClutterModelIter *)iter,args);
}
/**
 * thornbury_model_iter_get_value :
 * @iter: row pointer
 * @column: column at which data needs to be retrieved.
 * @value: value of the property(out parameter)
 * 
 */
void thornbury_model_iter_get_value(ThornburyModelIter *iter,
                                guint column,
                                GValue *value)
{
	clutter_model_iter_get_value((ClutterModelIter *)iter,column,value);
}
/**
 * thornbury_model_iter_set :
 * @iter: row pointer
 * @...: %NULL terminated column values of the row
 *
 */
void thornbury_model_iter_set (ThornburyModelIter *iter,
                           ...)
{
	va_list args;
	va_start (args, iter);
	clutter_model_iter_set_valist(CLUTTER_MODEL_ITER(iter),args);
	va_end (args);
	//thornbury_model_iter_set(CLUTTER_MODEL_ITER(iter),...);
}
/**
 * thornbury_model_iter_set_valist :
 * @iter: row pointer
 * @args:
 *
 */
void thornbury_model_iter_set_valist(ThornburyModelIter *iter,
                                va_list args)
{
	clutter_model_iter_set_valist ((ClutterModelIter *)iter,args);
}
/**
 * thornbury_model_iter_set_value :
 * @iter: row pointer
 * @column: column at which data needs to be retrieved.
 * @value: value of the property(out parameter)
 * 
 */
void thornbury_model_iter_set_value (ThornburyModelIter *iter,
                                 guint column,
                                 const GValue *value)
{
	ThornburyModel *model = (ThornburyModel *)clutter_model_iter_get_model((ClutterModelIter *)iter);
	clutter_model_iter_set_value ((ClutterModelIter *)iter,column,value);
}

/**
 * thornbury_model_iter_is_first :
 * @iter: row pointer
 *
 * Returns: %TRUE/%FALSE
 */
gboolean thornbury_model_iter_is_first (ThornburyModelIter *iter)
{
	return clutter_model_iter_is_first ((ClutterModelIter *)iter);
}
/**
 * thornbury_model_iter_is_last :
 * @iter: row pointer
 *
 * gets the last row pointer.
 *
 * Returns: %TRUE if last row pointer,%FALSE if Not.
 */
gboolean thornbury_model_iter_is_last (ThornburyModelIter *iter)
{
	return clutter_model_iter_is_last ((ClutterModelIter *)iter);
}
/**
 * thornbury_model_iter_next :
 * @iter: row pointer
 *
 * gets next row pointer with respect to the @iter.
 *
 * Returns: (transfer none): next row iter pointer. 
 */
ThornburyModelIter *thornbury_model_iter_next (ThornburyModelIter *iter)
{
	return (ThornburyModelIter *)clutter_model_iter_next ((ClutterModelIter *)iter);
}
/**
 * thornbury_model_iter_prev :
 * @iter: row pointer
 * Returns: (transfer none): prev row iter pointer. 
 */
ThornburyModelIter *thornbury_model_iter_prev (ThornburyModelIter *iter)
{
	return (ThornburyModelIter *)clutter_model_iter_prev((ClutterModelIter *)iter);
}

/**
 * thornbury_model_iter_get_model :
 * @iter:row pointer 
 *
 *  Given a iter pointer returns corresponding #ThornburyModel 
 *
 * Returns: (transfer none): Model
 */
ThornburyModel *thornbury_model_iter_get_model(ThornburyModelIter *iter)
{
	return (ThornburyModel *)clutter_model_iter_get_model((ClutterModelIter *)iter);
}
/**
 * thornbury_model_iter_get_row :
 * @iter:row pointer 
 *
 *
 * Returns: row value at which iter is pointing to.
 */
guint thornbury_model_iter_get_row (ThornburyModelIter *iter)
{
	return clutter_model_iter_get_row((ClutterModelIter *)iter);
}

/**
 * thornbury_model_iter_copy :
 * @iter: pointer to iter 
 *
 * Returns: (transfer full): row pointer,g_object_unref() has be called to free it.
 * 
 */
ThornburyModelIter *thornbury_model_iter_copy(ThornburyModelIter *iter)
{
	return (ThornburyModelIter *)clutter_model_iter_copy((ClutterModelIter *)iter);
}

#ifndef THORNBURY_DISABLE_DEPRECATED

/**
 * thornbury_model_register_column_changed_cb:
 * @model: a model instance
 * @column_changed_cb: a callback function
 * @userdata: a pointer of user data
 *
 * Register a callback function to model.
 *
 * Deprecated: UNRELEASED
 */
void
thornbury_model_register_column_changed_cb(ThornburyModel *model,void (*column_chnaged_cb)(gint,gpointer),gpointer userdata)
{
}
#endif

/**
 * thornbury_list_model_new:
 * @n_columns: total number of columns.
 * @...: %NULL terminated, type of the column and name of the column pairs.
 * 
 * Creates New #ThornburyModel instance 
 *
 * Returns: (transfer full): Model instance
 */
GObject *
thornbury_list_model_new (guint n_columns,
                          ...)
{
  GObject *model;
  g_autoptr (GArray) types = g_array_sized_new (FALSE, FALSE, sizeof (GType), n_columns);
  g_autoptr (GPtrArray) names = g_ptr_array_sized_new (n_columns);
  va_list args;
  guint i;

  g_return_val_if_fail (n_columns > 0, NULL);

  va_start (args, n_columns);
  for (i = 0; i < n_columns; i++)
    {
      GType type = va_arg (args, GType);
      g_array_append_val (types, type);
      g_ptr_array_add (names, va_arg (args, gchar *));
    }
  va_end (args);

  model = thornbury_list_model_newv (n_columns,
      (GType *) types->data,
      (const gchar * const *) names->pdata);

  return model;
}

/**
 * thornbury_list_model_newv:
 * @n_columns: Id of the Property
 * @types: Array of types.
 * @names: Array of names
 *
 * Creates New #ThornburyModel instance 
 *
 * Returns: (transfer full): Model instance
 */
GObject *
thornbury_list_model_newv (guint n_columns,
                           GType *types,
                           const gchar * const names[])
{
  GObject *model;

  g_return_val_if_fail (n_columns > 0, NULL);

  model = g_object_new (THORNBURY_TYPE_MODEL, NULL);

  clutter_model_set_types (CLUTTER_MODEL (model), n_columns, types);
  clutter_model_set_names (CLUTTER_MODEL (model), n_columns, names);

  return model;
}

#ifndef THORNBURY_DISABLE_DEPRECATED

/**
 * thornbury_list_model_destroy:
 * @model: Object reference
 *
 * Decrease the reference count of #ThornburyModel object.
 * When its reference count drops to 0, the object is finalized.
 *
 * Deprecated: UNRELEASED
 */
void thornbury_list_model_destroy(ThornburyModel *model)
{
  g_return_if_fail (THORNBURY_IS_MODEL (model));
  g_object_unref (model);
}

#endif
