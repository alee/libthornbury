/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/******************************************************************************
 *	@Filename : view-manager.c
 *	@Project: --
 *-----------------------------------------------------------------------------
 *  @Description :  Implementation of View manager framework .
 *
 *
 *
 * Description of FIXES:
 * -----------------------------------------------------------------------------------
 *	Description															Date			Name
 *	----------															----			----
 *	1. maintaining view history sequence corrected						13Mar
 *	2. Storage pointers for signal and model in							26Mar
 *		ViewManagerWidgetData structure dropped.
 *	3. Control is provided to plug-in to carry out any operations		26Mar
 *		related to view-switching like checking for intermediate
 *		request for view switch when view switch animation is in
 *		progress.
 *
 *
 *
 *
 *******************************************************************************/

/**
 * ThornburyViewManager::ThornburyViewManager:
 * @Title:ThornburyViewManager
 * @Short_Description: Provides framework for handling views.
 * @See_Also: #GValue #GObject #ClutterActor
 *
 *
 * All applications have rich UI associated with them. Each application contains different “views”
 * to represent its data. An application is required to make transitions between these views based on user or
 * external events. Usually transitions between views have some sort of an animation associated with it.
 * Each of these views is created by using the view definition file. To reduce repetitive code and complexity
 * for application developers, a utility library , the View manager can be used to provide view construction and
 * transition facilities. The main purpose of view manager is to provide the View Management capabilities,
 *  widget creation , Event management and Theme / Skinning capabilities for application .
 *
 *
 * The view manager is designed to support the application which is developed based on the MVC concepts for providing UI
 * theme and skinning capabilities with a clear separation of UI components and the backend services.
 *
 * To know more about MVC refer <link linkend="MVC_concept_document">MVC Overview</link>
 * The model knows about all the data that needs to be displayed. It is the model who is aware about all the operations
 * that can be applied to transform an object. It only represents the data of an application. The model represents enterprise
 * data and the business rules that govern access to and updates of this data. Model is not aware about the presentation data
 * and how that data will be displayed. It knows that a widget exists and needs a way of obtaining information and notification.
 *
 * Application’s interface to the views is via the model. Application feeds the required data to the model; it then notifies
 * the view about the change in data via events. It’s the views responsibility to then update the contents immediately.
 *
 * In the MVC framework context, Views receive such events and pass them to the Controller for processing.View manager
 * provides convenient APIs to connect / disconnect to the widget signals from the application. Application can provide an
 * event handler for interested events and pass application data to connect to the required signals.
 *
 * 	Widget’s API reference manual describes a complete list of signals emitted by it along with call-back handler prototype.
 * Application can request View manager to connect handlers directly to the widget’s signals.
 *
 * Some Functionalities of View-Manager are:
 *
 * 	<itemizedlist>
 * 	<listitem> The default view is loaded when the application is started for the first time.
 * 	Subsequent launch starts in the previous view     </listitem>
 * 	<listitem> Construct and layout widgets based on the application skin
 * 	 and current system theme and language    </listitem>
 * 	<listitem> A top-level container holds all the widgets in a view for better control on the
 * 	view during destruction and view switching     </listitem>
 * 	<listitem> Provides a separate animation class to support view switch animation which has flexibility to be configured
 * 	differently for different variants</listitem>
 * 	<listitem> Maintains a view stack to switch between them and navigates in the reverse order of
 * 	traverse upon press of BACK button   </listitem>
 * 	<listitem> Stores all the widget data models created by the application and correlate these models
 * 	 to their respective views.    </listitem>
 * 	<listitem>  Destroys only the views and not the model to recreate the views when required.   </listitem>
 * 	<listitem> Provide signals for view switch start and view switch end to applications.    </listitem>
 * 	<listitem> Support implementation of full screen animation framework.    </listitem>
 * 	<listitem> Support complete handling of Back button state machine and also provides flexibility to
 * 	have a view not being added to history at all..    </listitem>
 * 	<listitem>  Support sharing of widgets across different views    </listitem>
 * 	<listitem> Skeleton view will be shown before the actual view is constructed and rendered    </listitem>
 * 	<listitem>  Watches for changes in Global UI preferences like theme, skin, and language etc.   </listitem>
 * 	</itemizedlist>
 *
 *	## Initializing View-Manager inside Application.
 *
 * The view manager needs to be initialized by the application. Application needs to feed some parameters during the
 * initialization. Preferably, call `g_new0(ThornburyViewManagerAppData)` to create the needed data structure with pre-initialized
 * values. View manager registers for the global preferences to get the current theme, skin and language information.
 * Based on the app name and the current skin info, it locates the view definition files for the application.
 *
 * Based on the resource folder location provided by the application, it parses the view definition file for the default
 * view or the last active view for the application and starts creating the widgets for that view.
 * It feeds the current theme and language information to the widget.The widgets use the theme information to load
 * appropriate CSS files.
 *
 * The generic form of a view json file is as shown below.
 * |[<!-- language="JSON" -->
 * {
 *	 "viewId":"ViewName",
 *   	"widgets":{
 *      "WidgetName":["WidgetGType","file_name",pos_x, pos_y, layer]
 * }
 * ]|
 *
 * "ViewName" is useful to identify the view and associate widgets to the mentioned view.
 * "file_name" corresponds to the Property json file which the widget demands. "file_name" should have ".json" extension
 * which will be appended by default. 'layer' can have positive value
 * starting from 0 which indicates the layer to which widget has to be added with Layer 0 being the bottom most layer.
 * "WidgetGType" specifies the GType of the widget. "WidgetName" is the name given to uniquely identify the widget and applications
 * can use the same name for further reference. Its application developer responsibility to give unique "WidgetName" for widget without
 * overlapping with any other widget, in which case it will be treated as a global widget.
 *
 * A Sample view json file for a view named "list_view" is as shown below.
 *
 *  |[<!-- language="JSON" -->
 *  {
 *	 "viewId":"list_view",
 *   	"widgets":{
 *      "WidgetOne":["WidgetOneType","widget_one_prop",10, 10, 1],
 *      "WidgetTwo":["WidgetTwoType","widget_two_prop",100, 100, 0]
 * }
 * ]|
 * In case of multiple views, View-json files have to be created for each such views.All the widget's property json files
 * should be grouped under directory having same name as that of ViewName. If a widget needs to be present in one or more
 * view, which is termed a global widget, can be added in multiple view-json files sharing same "WidgetName" .
 * However only one instance of that widget will be created.
 *
 * The View manager instantiates project specific view transition plug-ins to support view switching.
 * It uses this plug-in to create view switch animations.It also supports animations like normal to full screen view transition.
 *
 * Applications should pass the default view info to the view-manager. The default view is loaded when the application is started for the first time.
 * and on subsequent launch, the view in use before app-termination in its previous session will be loaded.
 * The start-up performance now becomes independent of number of views an application have, because the views
 * are created dynamically on request. There could be an impact on view switch as new views have to be created.
 * By having view switch transition animations and also by showing empty widget containers on new view creation,
 * there should be minimal impact on user perception.
 *
 * |[<!-- language="C" -->
 *
 * gchar *pRestoredViewName = NULL ;
 * GHashTable *pAppDataHash = NULL ;
 *
 *  ThornburyViewManagerAppData *pData = g_new0(ThornburyViewManagerAppData);
 *  // Application Object which is added to the stage.
 *	pData->app = CLUTTER_ACTOR(MyAppObj);
 *
 *	// Mention the Default view
 *	pData->default_view = "list_view";
 *
 *	// Application Run-time name.
 *	pData->app_name = "MyAppName" ;
 *
 *	// Resource directory Path
 *	pData->viewspath = "/Applications/MyApp/share/" ;
 *
 *	pData->lang_basefolderpath = "/Applications/MyApp/share/language-files";
 *	pData->lang_basefilename = "language";
 *
 *	// Explicitly have a back handling function to control view-navigation. Usually %NULL
 *	pData->pAppBackFunc = NULL ;
 *	// create the view manager instance.
 *	ThornburyViewManager *pViewMgrObj = thornbury_view_manager_new(pData);
 *
 *	// Use build_and_restore_last_view() restore the view shown during the last app session.
 *	// If it is the very first session default view will be loaded.
 *
 *	ThornburyViewManagerError enErrorCode = build_and_restore_last_view(pViewMgrObj , &pRestoredViewName , &pAppDataHash);
 *	if(THORNBURY_VIEW_MANAGER_OK != enErrorCode)
 *	{
 *		g_warning("Error building views: Error Code:%d\n" ,enErrorCode );
 *	}
 *
 * ]|
 *
 *
 * ## Associating model to widgets.
 *
 * Model contains data in a recognized format between a widget and an application. Since model are run-time information,
 * they need to be associated with the widget. View manager links the model to the mentioned widget on request by the application.
 *
 *  |[<!-- language="C" -->
 *
 *	GHashTable *pListViewModels = g_hash_table_new(g_str_hash,g_str_equal);
 *	ThornburyModel *pModel = (ThornburyModel *)thornbury_list_model_new (2,
 *			G_TYPE_STRING, NULL,
 *			G_TYPE_POINTER, NULL,-1);
 *	g_hash_table_insert(pListViewModel,"WidgetName" ,pModel);
 *	// Model for multiple widgets can be passed by appending the details to hash table
 *	set_widgets_models(pViewMgrObj,pListViewModels);
 *
 * ]|
 *
 * ## Associating signal handlers.
 *
 * Widgets will provide signals to indicate the event change in the view so that the application can further handle the event specific
 * functions. Application can register to such events to fine tune the overall behavior.
 *
 *  |[<!-- language="C" -->
 *
 *	GHashTable *pSignals = g_hash_table_new(g_direct_hash,g_str_equal);
 *	GList *pWidgetOneSignals = NULL;
 *
 *	ThornburyViewManagerSignalData *pData = g_new0(ThornburyViewManagerSignalData,1);
 *	// Signal provided by the widget
 *	pData->signalname = ("touch-event");
 *	// Callback function to be registered
 *	pData->func =  G_CALLBACK(touch_event_clb));
 *	// User data that needs to be passed which connecting to signal.
 *	pData->data = NULL;
 *	pWidgetOneSignals = g_list_append(pWidgetOneSignals,pData);
 *
 *	g_hash_table_insert(pSignals,"WidgetName",pWidgetOneSignals);
 *	// Signals for multiple widgets can be passed by appending the details to hash table
 *
 *	set_widgets_controllers(pViewMgrObj,pSignals);
 *  ]|
 *
 * ## Switching views.
 *
 *  View manager provides basic mechanism for view switching with or without animation. A trigger to switch the view
 *  should come from the application. Application can have a views drawer widget to help manage the view switching.
 *  Application when it is in need of a different view can call thornbury_switch_view() to achieve the same.An application
 *  can have multiple views and also views which are relevant to some views only(coupled).
 *  View Manager allows the application to selectively discard such views from being added to the view history.
 *  Views in the order of their traverse is added to the view history unless they are explicitly requested not to add.
 *  Call thornbury_set_view_switch_animation() to customize the view switch animation for views using #ThornburyViewManagerViewSwitchAnimData
 *  structure.
 *
 * ## Controlling the view navigation.
 *
 * 	View manager watches for the "back-press" event which can originate on pressing <emphasis>BACK</emphasis> button.
 * 	View manager will initiate the view switch logic implicitly on reception of such signal and navigate to a last view
 * 	as stored in the view stack. However before implicitly switching the view, it has mechanism to convey the event to application,
 * 	such that view switch can be blocked or withheld. If some non-suspendable task is in progress, application can convey the
 * 	information to the user via popup stating the reason. Application has to register a call back function at the time of
 * 	#ThornburyViewManager initialization to handle back events. The call back function so registered should be non-blocking and
 * 	should return immediately at any cost.
 *
 *	The callback function should be as shown below:
 * 	|[<!-- language="C" -->
 *	 gboolean clb_on_back_press(ThornburyViewManager * pViewManager, const gchar* const pViewToSwitch)
 *	 {
 *		gboolean bBackConsumedByApp = FALSE ;
 *		// Note :: Return TRUE , when app does not want to switch to previous view in
 *		// the history provided current view is not default one.
 *		// If speller is shown and BACK is pressed then speller can be hidden instead of
 *		// switching the view
 *		return bBackConsumedByApp ;
 *	 }
 * 	]|
 *
 *
 *
 * ## Restoring session.
 *
 *  There can be a limitation in number of application being run and shown in the system which in most of the case
 *  can be configurable. Due to this limitation certain application may have to be killed and re-launched on explicit request
 *  by the user. When launched, User may expect his application to be in the state as it was in the previous session, as user
 *  is not aware of this application management. To achieve this feel , view manager helps in storing some crucial
 *  data which may help application in recreating the view. If in next session thornbury_build_and_restore_last_view() is called by the application
 *  the last view from the previous session will be shown and the view order will be restored. Application can use the data obtained by calling
 *  thornbury_build_and_restore_last_view() to recreate the view and updating the model.
 *
 *  Call thornbury_view_manager_store_view_order_before_finishing() to  store the application  data when it receives a signal
 *  from app-manager which can force the application to go to background state. The Data must have a key-value association
 *  for easy retrieval. Key must be a string and currently only string can be stored in the value field.
 *
 *  |[<!-- language="C" -->
 *
 *  GHashTable *pSaveAppDataHash =  g_hash_table_new(g_str_hash,g_str_equal );
 *  gchar *pValue = "MyName" ;
 *  // Pass string "NULL" to indicate unavailable information.But not compulsory to pass so.
 * 	g_hash_table_insert(pAppDataHash , APP_DATA_USER_NAME, (NULL == pValue ? g_strdup("NULL") : pValue ));
 * 	g_hash_table_insert(pAppDataHash , APP_DATA_USER_SECOND_NAME, "SecondName");
 * 	// Save Data and view order to help restoring the view on next start-up
 *  thornbury_view_manager_store_view_order_before_finishing((const GHashTable* )pSaveAppDataHash);
 *
 *  // Destroy the hash table.
 *  g_hash_table_destroy(pSaveAppDataHash);
 *  ]|
 *
 *
 * Refer <link linkend="ThornburyModelIter">ThornburyModel</link> for model details.
 *
 * #ThornburyViewManager is available since libthornbury 1.0
 */


#include "libthornbury-viewmanager.h"
#include "libthornbury-viewmanager-internal.h"
#include <gobject/gvaluecollector.h>
#include <gmodule.h>
#include "libthornbury-ui-texture.h"
#include "libthornbury-ui-utility.h"
//#include "preference_manager_fi.h"
#include "libthornbury-language.h"
#include <glib/gprintf.h>

#include <canterbury/gdbus/canterbury.h>

#define CANTERBURY_BUS_NAME                     "org.apertis.Canterbury"
#define CANTERBURY_HARDKEYS_OBJECT_PATH         "/org/apertis/Canterbury/HardKeys"

#define THORNBURY_VIEWMANAGER_SCHEMA_ID         "org.apertis.Thornbury.ViewManager"

/* Flag to enable adding a dummy back button for debugging purpose */
#define ADD_DEBUG_BACK_BUTTON	0


#define VM_SCHEMA_KEY_BACKGROUND_IMG      	"view-manager-bkgrndimg"
#define VM_SCHEMA_KEY_VIEW_BACKGROUND_IMG 	"view-manager-viewbkgrndimg"
#define VM_SCHEMA_KEY_LIBPATH 				"view-manager-libpath"
#define VM_SCHEMA_KEY_SCREEN_WIDTH 			"view-manager-screenwidth"
#define VM_SCHEMA_KEY_SCREEN_HEIGHT			"view-manager-screenheight"
#define VM_SCHEMA_KEY_ENABLE_SCREENSHOT		"view-manager-screenshot"
#define VM_SCHEMA_KEY_VIEW_BG_X				"view-bg-x"
#define VM_SCHEMA_KEY_VIEW_BG_Y				"view-bg-y"

enum {
  SIGNAL_VIEW_CREATED = 1,
  SIGNAL_SWITCH_BEGIN,
  SIGNAL_SWITCH_END,
  SIGNAL_EXIT_ANIMATION_COMPLETED,
  SIGNAL_ENTRY_ANIMATION_COMPLETED,
  SIGNAL_BACKBUTTON_CONTINUE,
  SIGNAL_LAST, 
};

enum {
  PROP_BACKGROUND = 1,
  PROP_SEATON,
  PROP_DEFAULT_VIEW_NAME,
  PROP_CURRENT_VIEW_NAME,
  PROP_CLUTTER_STAGE,
  PROP_APP_NAME,
  PROP_SCREENSHOT_LOCATION,
  PROP_CLUTTER_MAIN, 
  PROP_VIEW_SCHEMA_LOCATION,
};

static guint view_manager_signals[SIGNAL_LAST] = { 0, };

ThornburyViewManager *thornbury_view_manager = NULL;
guint  view_manager_debug_flags = 0;

/* Set the environment variable in terminal to enable traces: export VIEW_MANAGER_DEBUG=view-manager */

typedef struct _ThornburyViewManagerPrivate
{
  GHashTable *viewsStructure;
  guint nNumViews;
  gchar *default_view;
  GHashTable *view_list;
  ClutterActor *app;
  ClutterActor *pGlobalAppBg;
  gchar *fpath;
  gchar *current_view;
  GList *viewHistory;
  CanterburyHardKeys *hardkeys_mgr_proxy;
  gchar *app_name;
  GHashTable *global_widgets;
  ThornburyApp_LangConfigData *langData;
  ClutterStage *app_stage;
  ClutterActor *trans_bg_img;
  SeatonPreference *pViewManagerPDI;
  gboolean bEnableScreenShot ;
  gchar *pScreenShotPath;

  gfloat fltAppBgX;
  gfloat fltAppBgY;

  GMutex lock;
  guint screen_width;
  guint screen_height;
  guint canterbury_bus_id;

  GSettingsSchema *schema;
  GSettings       *settings;

  GModule         *module;
 
} ThornburyViewManagerPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (ThornburyViewManager, thornbury_view_manager, G_TYPE_OBJECT)

static void thornbury_view_manager_constructed (GObject *obj); 
static gboolean thornbury_view_manager_backbutton_continue (ThornburyViewManager *self,
                                                            const gchar *view_name);
static ClutterActor *create_image (const gchar *path, gint width, gint height);

static ClutterActor *get_widget_by_name (ThornburyViewManager *view_manager,
                                         const gchar *widgetname);
static ThornburyViewManagerWidgetData * get_widget_data(ThornburyViewManager *self,
                                                        const gchar *widgetname);
ThornburyViewManagerViewData * thornbury_get_view_data (ThornburyViewManager *view_manager,
                                                        const gchar *viewname);
static ClutterActor * get_layer_container(guint layernum,ClutterActor* parent);
static ThornburyViewManagerError build_view (ThornburyViewManager *view_manager,
                                             ThornburyViewManagerViewData *viewdata,
                                             GHashTable *viewData);
static ThornburyViewManagerError view_manager_create_view(ThornburyViewManager* view_manager, GError **error);
static void add_to_view_history (ThornburyViewManager *view_manager, const gchar *viewname);
static gboolean remove_from_view_history (ThornburyViewManager *view_manager,
                                          const gchar *viewname);
static void reset_view_history (ThornburyViewManager *view_manager);
static void view_manager_hard_keys_name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner,gpointer user_data);
static void view_manager_hard_keys_name_vanished (GDBusConnection *connection, const gchar *name,gpointer user_data);
static void view_manager_hard_keys_proxy_clb( GObject *source_object, GAsyncResult *res,gpointer user_data);
static gboolean view_manager_back_press_status( CanterburyHardKeys *object, const gchar *arg_app_name,gpointer user_data );
static gchar * get_widget_view (ThornburyViewManager *self, gchar *widgetname);
//static void view_manager_pref_mgr_name_appeared(GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data);
//static void view_manager_preference_manager_proxy_clb(GObject *source_object, GAsyncResult *res, gpointer user_data);
//static void on_viewmgr_notify_language ( GObject    *object, GParamSpec *pspec, gpointer    user_data);
//static void view_manager_pref_mgr_name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data);
//static void notify_widgets(gchar *language);
//static void on_viewmgr_notify_theme(gchar *theme);
static void free_views_list_hash( gpointer value );
static void view_exit_animation_completed (ThornburyViewManager *view_manager,
                                           const gchar *current_view,
                                           const gchar *new_view,
                                           gpointer user_data);
static void view_entry_animation_completed (ThornburyViewManager *view_manager,
                                            const gchar *new_view,
                                            gpointer user_data);
static ClutterActor *get_widget_by_name_and_view (ThornburyViewManager *view_manager,
                                                  const gchar *pViewName,
                                                  const gchar *widgetname);

static gint i_view_manager_control_view_switch(
		ThornburyViewManager *pViewManager , gchar *pLoadingView);


#if ADD_DEBUG_BACK_BUTTON
gboolean b_view_manager_debug_back_button (ClutterActor *actor,
		ClutterEvent *event,
		gpointer      user_data);
#endif


/* Importable plug-in fucntions */

typedef void v_switch_view(ThornburyViewManager * view_manager,ThornburyViewManagerViewData *currentview,
		ThornburyViewManagerViewData *newview, gboolean animation_needed,guint iSelected_row);

typedef void v_plugin_build_view(ThornburyViewManager * view_manager,ThornburyViewManagerViewData *pViewData);

typedef ThornburyViewManagerError v_set_property(ThornburyViewManager *view_manager, ClutterActor *widget, va_list);

typedef void v_set_animation(ThornburyViewManager *view_manager,ClutterActor *widget,ThornburyViewManagerAnimData *anim_data);

typedef void v_on_exit_animation_complete(ThornburyViewManager *pViewManager,ThornburyViewManagerViewData *pCurrentViewData,
		ThornburyViewManagerViewData *pNewViewData );

typedef void v_on_entry_animation_complete(ThornburyViewManager *pViewManager,ThornburyViewManagerViewData *pCurrentViewData,
		ThornburyViewManagerViewData *pNewViewData );

typedef void v_vm_plugin_init(ThornburyViewManager * view_manager );

typedef gint i_action_on_back_press(ThornburyViewManager *view_manager , gchar *pLoadingView);

static void
view_manager_get_property (GObject    *object,
                           guint       property_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  ThornburyViewManager *view_manager = THORNBURY_VIEW_MANAGER (object);
  ThornburyViewManagerPrivate* priv = thornbury_view_manager_get_instance_private (view_manager);

  switch (property_id)
    {
    case PROP_BACKGROUND:
      g_value_set_object (value, priv->pGlobalAppBg);
      break;
    case PROP_SEATON:
      g_value_set_object (value, priv->pViewManagerPDI);
      break;
    case PROP_DEFAULT_VIEW_NAME:
      g_value_set_string (value, priv->default_view);
      break;
    case PROP_CURRENT_VIEW_NAME:
      g_value_set_string (value, priv->current_view);
      break;
    case PROP_CLUTTER_STAGE:
      g_value_set_object (value, priv->app_stage);
      break;
    case PROP_APP_NAME:
      g_value_set_string (value, priv->app_name);
      break;
    case PROP_SCREENSHOT_LOCATION:
      g_value_set_string (value, priv->pScreenShotPath);
      break;
    case PROP_CLUTTER_MAIN:
      g_value_set_object (value, priv->app);
      break;
    case PROP_VIEW_SCHEMA_LOCATION:
      g_value_set_string (value, priv->fpath);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
view_manager_set_property (GObject      *object,
                           guint         property_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  ThornburyViewManager *view_manager = THORNBURY_VIEW_MANAGER (object);
  ThornburyViewManagerPrivate* priv = thornbury_view_manager_get_instance_private (view_manager);

  switch (property_id)
    {
    case PROP_SEATON:
      g_set_object (&priv->pViewManagerPDI, g_value_get_object (value));
      break;
    case PROP_DEFAULT_VIEW_NAME:
      g_clear_pointer (&priv->default_view, g_free);
      priv->default_view = g_value_dup_string (value);
      break;
    case PROP_CURRENT_VIEW_NAME:
      g_clear_pointer (&priv->current_view, g_free);
      priv->current_view = g_value_dup_string (value);
      break;
    case PROP_CLUTTER_MAIN:
      g_set_object (&priv->app, g_value_get_object (value));
      break;
    case PROP_APP_NAME:
      g_clear_pointer (&priv->app_name, g_free);
      priv->app_name = g_value_dup_string (value);
      break;
    case PROP_VIEW_SCHEMA_LOCATION:
      g_clear_pointer (&priv->fpath, g_free);
      priv->fpath = g_value_dup_string (value);
      break;
    case PROP_CLUTTER_STAGE:
      g_clear_object (&priv->app_stage);
      g_set_object (&priv->app_stage, g_value_get_object (value));
      break;
    case PROP_SCREENSHOT_LOCATION:
      g_clear_pointer (&priv->pScreenShotPath, g_free);
      priv->pScreenShotPath = g_value_dup_string (value);
      break;
    case PROP_BACKGROUND:
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
view_manager_dispose (GObject *object)
{
  ThornburyViewManager *view_manager = THORNBURY_VIEW_MANAGER(object);
  ThornburyViewManagerPrivate *priv = thornbury_view_manager_get_instance_private (view_manager);

  if (priv->pViewManagerPDI != NULL)
    {
      seaton_preference_close (priv->pViewManagerPDI);
    }
  g_clear_object (&priv->pViewManagerPDI);

  g_clear_object (&priv->hardkeys_mgr_proxy);

  g_clear_object (&priv->settings);
  g_clear_pointer (&priv->schema, g_settings_schema_unref);

  G_OBJECT_CLASS (thornbury_view_manager_parent_class)->dispose (object);
}

static void
view_manager_finalize (GObject *object)
{
  ThornburyViewManager *view_manager = THORNBURY_VIEW_MANAGER (object);
  ThornburyViewManagerPrivate *priv = thornbury_view_manager_get_instance_private (view_manager);

  if (priv->canterbury_bus_id != 0)
    {
      g_bus_unwatch_name (priv->canterbury_bus_id);
      priv->canterbury_bus_id = 0;
    }

  if (priv->viewHistory != NULL)
    {
      g_list_free_full (priv->viewHistory, g_free);
      priv->viewHistory = NULL;
    }

  g_mutex_clear (&priv->lock);

  g_clear_pointer (&priv->module, (GDestroyNotify) g_module_close);
  g_clear_pointer (&priv->global_widgets, g_hash_table_destroy);
  g_clear_pointer (&priv->viewsStructure, g_hash_table_destroy);
  g_clear_pointer (&priv->view_list, g_hash_table_destroy);
  g_clear_pointer (&priv->langData->pBaseFileName, g_free);
  g_clear_pointer (&priv->langData->pBaseFolderPath, g_free);
  g_clear_pointer (&priv->langData, g_free);
  g_clear_pointer (&priv->default_view, g_free);
  g_clear_pointer (&priv->current_view, g_free);
  g_clear_pointer (&priv->app_name, g_free);
  g_clear_pointer (&priv->pScreenShotPath, g_free);

  G_OBJECT_CLASS (thornbury_view_manager_parent_class)->finalize (object);
}

static void
thornbury_view_manager_class_init (ThornburyViewManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = thornbury_view_manager_constructed;
  object_class->get_property = view_manager_get_property;
  object_class->set_property = view_manager_set_property;
  object_class->dispose = view_manager_dispose;
  object_class->finalize = view_manager_finalize;

  klass->backbutton_continue = thornbury_view_manager_backbutton_continue;

  g_object_class_install_property (object_class, PROP_BACKGROUND,
    g_param_spec_object ("background",
                         "Global Background Container",
                         "The ClutterActor which stores background images and its information",
                         CLUTTER_TYPE_ACTOR,
                         G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

   /**
    * ThornburyViewManager:seaton:
    *
    * The reference of Seaton preference, but this property will be removed.
    * https://phabricator.apertis.org/T779
    */
   g_object_class_install_property (object_class, PROP_SEATON,
    g_param_spec_object ("seaton",
                         "Seaton Preference",
                         "The SeatonPreference which stores user and app preferences",
                         SEATON_TYPE_PREFERENCE, 
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

   /**
    * ThornburyViewManager:default-view-name:
    *
    * The name of view to be displayed  when application is launched.
    */
  g_object_class_install_property (object_class, PROP_DEFAULT_VIEW_NAME,
    g_param_spec_string ("default-view-name",
                        "Default View",
                        "The name of default view which is set by schema",
                        "", 
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_STATIC_STRINGS));

   /**
    * ThornburyViewManager:current-view-name:
    *
    * The name of view which is displayed currently.
    * It should be read-only property, but it has set as writable temporarily
    * because it is updated by outside of the object.
    */
  g_object_class_install_property (object_class, PROP_CURRENT_VIEW_NAME,
    g_param_spec_string ("current-view-name",
                        "Current View",
                        "The name of  view which is currently used",
                        "", 
                        G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  /**
   * ThornburyViewManager:clutter-stage:
   *
   * The #ClutterStage object for a top level window
   */
  g_object_class_install_property (object_class, PROP_CLUTTER_STAGE,
    g_param_spec_object ("clutter-stage",
                         "Current Screen Stage",
                         "The ClutterStage which is a top level window of running program",
                         CLUTTER_TYPE_STAGE,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * ThornburyViewManager:app-name:
   *
   * The name of application
   */
  g_object_class_install_property (object_class, PROP_APP_NAME,
    g_param_spec_string ("app-name",
                        "App Name",
                        "The name of current application ui",
                        "", 
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * ThornburyViewManager:screenshot-location:
   *
   * The absolute path for storing screenshots.
   */
  g_object_class_install_property (object_class, PROP_SCREENSHOT_LOCATION,
    g_param_spec_string ("screenshot-location",
                        "Location of Screenshots",
                        "The location for storing screenshots",
                        "", 
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_STATIC_STRINGS));

  /**
   * ThornburyViewManager:clutter-main:
   *
   * The #ClutterActor object of main UI
   */
  g_object_class_install_property (object_class, PROP_CLUTTER_MAIN,
    g_param_spec_object ("clutter-main",
                         "Main ClutterActor",
                         "The ClutterActor which is used for main UI",
                         CLUTTER_TYPE_ACTOR,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * ThornburyViewManager:view-schema-loction:
   * 
   * The location of JSON-schema for views
   */
  g_object_class_install_property (object_class, PROP_VIEW_SCHEMA_LOCATION,
    g_param_spec_string ("view-schema-location",
                         "Location of View Schema",
                         "The location for view schema",
                         "",
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
 
                      
	/**
	 * ThornburyViewManager::view_mgr_init:
	 * @pObj: a #ThornburyViewManager object reference.
	 *
	 * #ThornburyViewManager::view_mgr_init is emitted upon the initialization of view-manager i.e, on parsing all View-Json files and
	 * building the required components independent of view.
	 */
  view_manager_signals[SIGNAL_VIEW_CREATED] =
    g_signal_new ("view_mgr_init",
                  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (ThornburyViewManagerClass, view_created),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 0,
                  NULL);

	/**
	 * ThornburyViewManager::view_switch_end:
	 * @pObj: a #ThornburyViewManager object reference.
	 *
	 * #ThornburyViewManager::view_switch_end is emitted after completion of view-switch animation.
	 */
  view_manager_signals[SIGNAL_SWITCH_END] =
    g_signal_new ("view_switch_end",
                  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (ThornburyViewManagerClass, switch_end),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);
/**
	 * ThornburyViewManager::view_switch_begin:
	 * @pObj: a #ThornburyViewManager object reference.
	 *
	 * #ThornburyViewManager::view_switch_begin is emitted before starting with view-switch animation.
	 */
  view_manager_signals[SIGNAL_SWITCH_BEGIN] =
    g_signal_new ("view_switch_begin",
                  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (ThornburyViewManagerClass, switch_begin),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);


/**
	 * ThornburyViewManager::view_switch_exit_anim_completed:
	 * @pObj: a #ThornburyViewManager object reference.
	 *
	 * #ThornburyViewManager::view_switch_exit_anim_completed is emitted after completion of view-switch animation
	 * of exiting view. Plugins can use this signal to fine tune the animation behavior.
	 */
  // FIXME: This signal is emitted by mildenhall 
  view_manager_signals[SIGNAL_EXIT_ANIMATION_COMPLETED] =
    g_signal_new ("view_switch_exit_anim_completed",
                  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (ThornburyViewManagerClass, exit_animation_completed),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 2,
                  G_TYPE_STRING, G_TYPE_STRING);
/**
	 * ThornburyViewManager::view_switch_entry_anim_completed:
	 * @pObj: a #ThornburyViewManager object reference.
	 *
	 * #ThornburyViewManager::view_switch_entry_anim_completed is emitted after completion of view-switch animation
	 * of entry view. Plugins can use this signal to carry out relevant task upon animation completion.
	 */
  view_manager_signals[SIGNAL_ENTRY_ANIMATION_COMPLETED] =
    g_signal_new ("view_switch_entry_anim_completed",
                  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (ThornburyViewManagerClass, entry_animation_completed),
                  NULL, NULL,
                  NULL,
                  G_TYPE_NONE, 1,
                  G_TYPE_STRING);

  /**
   * ThornburyViewManager::backbutton-continue:
   * @self: the view manager object that emitted this signal
   * @view_name: the name of view which is to be switched if view stack is not empty
   * 
   * Reports that the back switch button is pressed.
   * 
   * Returns: %TRUE if you wish view manager to continue switching view, if %FALSE,
   * view manager will ignore remaining actions. 
   */
  view_manager_signals[SIGNAL_BACKBUTTON_CONTINUE] =
    g_signal_new ("backbutton-continue",
                  G_TYPE_FROM_CLASS (object_class),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (ThornburyViewManagerClass, backbutton_continue),
                  NULL, NULL,
                  NULL,
                  G_TYPE_BOOLEAN, 1,
                  G_TYPE_STRING);
}

static void
thornbury_view_manager_init (ThornburyViewManager *self)
{
  const char *pEnvString;
  ThornburyViewManagerPrivate *priv;

  priv = thornbury_view_manager_get_instance_private (self);
  priv->viewHistory = NULL;
  priv->current_view = NULL;
  priv->pViewManagerPDI = NULL;
  priv->pScreenShotPath = NULL;
  priv->fltAppBgX = 0.0;
  priv->fltAppBgY = 0.0;
  priv->pGlobalAppBg = NULL;

  pEnvString = g_getenv("VIEW_MANAGER_DEBUG");
  if (pEnvString != NULL)
  {
	  view_manager_debug_flags = g_parse_debug_string(pEnvString, view_manager_debug_keys, G_N_ELEMENTS(view_manager_debug_keys));
  }

  g_mutex_init (&priv->lock);
  priv->canterbury_bus_id = 0;
  priv->viewsStructure = g_hash_table_new_full (g_str_hash, g_str_equal,
    g_free, (GDestroyNotify) g_hash_table_destroy);
  priv->view_list = g_hash_table_new_full (g_str_hash, g_str_equal,
    g_free, (GDestroyNotify) free_views_list_hash);
  priv->global_widgets = g_hash_table_new_full (g_str_hash, g_str_equal,
    g_free, g_free);
}

/**
 * thornbury_view_manager_new:
 * @app_data: A #ThornburyViewManagerAppData structure having application info.
 *
 * This function should be used to create and initialize the view manager.
 * In this function view-manager will only parse the JSON files. Views are NOT created here.
 * To create view, use thornbury_build_and_restore_last_view().
 *
 * Returns: A new #ThornburyViewManager object. Call g_object_unref() on the returned #ThornburyViewManager to free resources.
 */

ThornburyViewManager *
thornbury_view_manager_new (ThornburyViewManagerAppData *app_data)
{
	ThornburyViewManagerPrivate *priv;

#if ADD_DEBUG_BACK_BUTTON
	ClutterActor *pDebugBackButton;
#endif

	g_return_val_if_fail (app_data != NULL, NULL);
	g_return_val_if_fail (app_data->app != NULL, NULL);
	g_return_val_if_fail (app_data->default_view != NULL &&
                              *app_data->default_view != '\0',
	                      NULL);
	g_return_val_if_fail (app_data->app_name != NULL &&
	                      *app_data->app_name != '\0',
 	                      NULL);

	if(thornbury_view_manager)
	{
		thornbury_view_manager = NULL;
	}

	thornbury_view_manager = g_object_new (THORNBURY_TYPE_VIEW_MANAGER,
		"clutter-main", app_data->app,
		"clutter-stage", app_data->stage,
		"default-view-name", app_data->default_view,
		"app-name", app_data->app_name,
		"view-schema-location", app_data->viewspath,
		"screenshot-location", app_data->pScreenShotPath,
		NULL);

 	priv = thornbury_view_manager_get_instance_private (thornbury_view_manager);

	/* FIXME: Can't we use gettext and glib-I18N directly?
         * https://phabricator.apertis.org/T810
         */ 
	priv->langData = g_new0 (ThornburyApp_LangConfigData,1);

	priv->langData->pBaseFolderPath = g_strdup (app_data->lang_basefolderpath);
	priv->langData->pBaseFileName = g_strdup (app_data->lang_basefilename);

	VIEW_MANAGER_PRINT("VIEW MANAGER : language folder path = %s Language file = %s\n",app_data->lang_basefolderpath,app_data->lang_basefilename );

	thornbury_lang_configure_AppData (priv->langData);

#if ADD_DEBUG_BACK_BUTTON
	pDebugBackButton = clutter_actor_new ();
	clutter_actor_set_background_color(pDebugBackButton , CLUTTER_COLOR_DarkBlue);
	clutter_actor_add_child (CLUTTER_ACTOR (priv->app_stage), pDebugBackButton);
	clutter_actor_set_size(pDebugBackButton , 64.0 , 64.0);
	clutter_actor_set_position(pDebugBackButton , 0.0 , 400.0);
	clutter_actor_set_reactive(pDebugBackButton , TRUE);
	g_signal_connect(pDebugBackButton , "button-press-event",G_CALLBACK(b_view_manager_debug_back_button ), NULL );



#endif



   return thornbury_view_manager;
}

static ThornburyViewManagerError
parse_view_files (ThornburyViewManager *view_manager,
                  ThornburyView *view,
                  const gchar *child_path)
{
	ThornburyViewManagerPrivate *priv = thornbury_view_manager_get_instance_private (view_manager);
	g_debug ("VIEW MANAGER:%s", __FUNCTION__);

	g_return_val_if_fail (view != NULL, THORNBURY_VIEW_MANAGER_INVALID_APP_DATA);

	if (NULL != view->pViewId)
	{
		ThornburyViewManagerViewData *view_data = g_new0 (ThornburyViewManagerViewData, 1);
		/* create the view container and add name to it */
		ClutterActor *container = clutter_actor_new ();

		view_data->view_data = view;
		view_data->view_name = g_strdup (view->pViewId);
		view_data->view_file_path = g_strdup (child_path);
		view_data->entry_effect = NULL;
		view_data->exit_effect = NULL;

		g_debug ("VIEW MANAGER:id  = %s", view->pViewId);
		clutter_actor_set_name (container, g_strdup (view->pViewId));
		clutter_actor_set_size (container, priv->screen_width, priv->screen_height);
		view_data->view = container;

		/* add to the views list for this app */
		g_hash_table_insert (priv->view_list, g_strdup (view->pViewId), view_data);

		/* now, start parsing widgets */
		if (view->pWidgetTable)
		{
			/* created a new hashmap for widgets inside a view.Key is widget name */
			GHashTable *widget_list = g_hash_table_new_full (g_str_hash, g_str_equal ,
					g_free , g_free/* because struct mem holds only pointer referece */);

			GHashTable *widgets = view->pWidgetTable;

			/* for iterating the widgetlist from view file */
			GHashTableIter iter;
			gpointer key, value;

			/* initialise the iterator */
			g_hash_table_iter_init (&iter, widgets);

			/* start iteration and create widgets */
			while (g_hash_table_iter_next (&iter, &key, &value))
			{
				ThornburyViewManagerWidgetData *dummywidget_data = NULL;
				ThornburyViewManagerWidgetData *widget_data = NULL;
				ThornburyViewWidgets *widgetdata = NULL;

				if (NULL == key || NULL == value)
				{
					g_hash_table_destroy (widget_list);
					return THORNBURY_VIEW_MANAGER_INIT_ERROR;
				}
				/* check if the widget is present anywhere in the internal structure. */
				dummywidget_data = get_widget_data (view_manager, (gchar*) key);

				if (NULL != dummywidget_data)
				{
					gchar *pViewName = get_widget_view (view_manager, (gchar*)key) ;
					g_debug (G_STRLOC"VIEW MANAGER : Adding global widget - %s to view - %s", (gchar*) key, pViewName );
					g_hash_table_insert (priv->global_widgets, g_strdup (key), pViewName );
				}

				widget_data = g_new0 (ThornburyViewManagerWidgetData, 1);
				/* key is name and value is widget data */
				widgetdata = (ThornburyViewWidgets*) (value);
				widget_data->widget_data = widgetdata;
				widget_data->widgetname = (gchar*) key;
				/* NOTE : Widget will be created later on request from app */
				widget_data->widget = NULL;
				widget_data->anim_data = NULL;

				g_hash_table_insert (widget_list, g_strdup (key), widget_data);
			}
			/* done with all the children so, map the view name and widget list */
			g_hash_table_insert (priv->viewsStructure, g_strdup (view->pViewId), widget_list);

		}
		/* finally, add the view to application container */
		clutter_actor_add_child (priv->app, container);
		clutter_actor_hide (container);

	}
	else
	{
		return THORNBURY_VIEW_MANAGER_INVALID_FILE;
	}
	g_debug ("VIEW MANAGER:id  = end of functionn");
	return THORNBURY_VIEW_MANAGER_OK;
}

static ThornburyViewManagerError
view_manager_create_view (ThornburyViewManager *self,
                            GError **error)
{
	const gchar *filename = NULL;
	ThornburyViewManagerError err;
	ThornburyViewManagerPrivate *priv = NULL;
	ThornburyView *view = NULL;
	GError *tmp_error = NULL;
	gchar *child_path = NULL;
	GApplication *app = NULL;

	g_return_val_if_fail (error == NULL || *error == NULL, THORNBURY_VIEW_MANAGER_INVALID_APP_DATA);

	priv = thornbury_view_manager_get_instance_private (self);
	app = g_application_get_default ();
	err = THORNBURY_VIEW_MANAGER_OK;

	if (app)
	{
		guint i;
		gchar **filenames = NULL;

		/* gets registerd global resorce path */
		const gchar *pResourcePath = g_application_get_resource_base_path (app);
		g_debug ("VIEW MANAGER : %s", pResourcePath);
		filenames = g_resources_enumerate_children (pResourcePath, G_RESOURCE_LOOKUP_FLAGS_NONE, &tmp_error);

		if (tmp_error != NULL)
		{
			g_debug ("error = %s", tmp_error->message);
			g_propagate_error (error, tmp_error);
			return THORNBURY_VIEW_MANAGER_INVALID_FILE;
		}

		for (i = 0; filenames != NULL && filenames[i] != NULL; i++)
		{
			g_debug ("file name  %s", filenames[i]);

			if (g_str_has_suffix (filenames[i], strJSON))
			{
				g_debug ("VIEW MANAGER : %s", filenames[i]);
				child_path = g_strconcat (pResourcePath, filenames[i],NULL);
				g_debug ("VIEW MANAGER : %s", child_path);
				view = thornbury_view_parser_parse_resource (child_path, &tmp_error);

				if (tmp_error != NULL)
				{
				      g_debug ("VIEW MANAGER: error %s", tmp_error->message);
				      g_strfreev (filenames);
				      g_free (child_path);
				      g_propagate_error (error, tmp_error);
				      return err;
				}
				parse_view_files (self, view, child_path);
				g_free (child_path);
			}
		}
		g_strfreev (filenames);
	}
	else
	{
		GDir *dir = g_dir_open (priv->fpath, 0, &tmp_error);
		if (dir == NULL)
		{
			/* FIXME: commong logging macro is required */
			g_warning ("Directory path is not valid : %s", priv->fpath);
			g_debug ("%d %s", __LINE__, tmp_error->message);
			g_propagate_error (error, tmp_error);
			return THORNBURY_VIEW_MANAGER_INVALID_DIR;
		}
		while ((filename = g_dir_read_name (dir)))
		{
			if (!g_str_has_suffix (filename, strJSON))
				continue;
			else
			{
				g_debug (G_STRLOC"VIEW MANAGER : Found view file = %s", filename);
			}

			child_path = g_build_filename (priv->fpath, filename, NULL);
			view = thornbury_view_parser_parse_file (child_path);
			err = parse_view_files (self, view, child_path);
			if(err == THORNBURY_VIEW_MANAGER_INVALID_FILE)
			{
				g_free (child_path);
				g_dir_close (dir);
				return err;
			}
			g_free (child_path);
		}
		g_dir_close (dir);
		/* all views are parsed now */
	}
	g_signal_emit (G_OBJECT (self),
			view_manager_signals[SIGNAL_VIEW_CREATED], 0);
	return err;
}

static ClutterActor * get_layer_container(guint layernum,ClutterActor* parent)
{
	GList *layerlist = clutter_actor_get_children(parent);

	ClutterActor *container = NULL;
	ClutterActor * layercontainernew = NULL;


	if((0 == g_list_length(layerlist)))
	{
		if(0 == layernum)
		{
			gint layercount = 0;
			gchar * layername;
			layercontainernew = clutter_actor_new();

			layername = g_strdup_printf("%s%i",strlayer, layercount);

			clutter_actor_set_name(layercontainernew ,layername);

			clutter_actor_insert_child_at_index(parent, layercontainernew,0);
			container = layercontainernew;
			return layercontainernew;
		}
		else
		{
			guint layercount = 0;
			gchar *layername;
			for(layercount = 0 ; layercount <= layernum; layercount++)
			{
				layercontainernew = clutter_actor_new();

				layername = g_strdup_printf ("%s%i",strlayer, layercount);
				clutter_actor_set_name(layercontainernew ,layername);

				clutter_actor_insert_child_at_index(parent, layercontainernew,layercount);
			}

			container = clutter_actor_get_child_at_index (parent,layernum);
		}

	}

	else
	{
		if(layernum < g_list_length(layerlist))
			container = CLUTTER_ACTOR(g_list_nth_data(layerlist,layernum));
		else
		{
			guint layercount = 0;
			for(layercount = g_list_length(layerlist) ; layercount <= layernum; layercount++)
			{
				gchar * layername;

				layercontainernew = clutter_actor_new();
				layername = g_strdup_printf("%s%i",strlayer, layercount);
				clutter_actor_set_name(layercontainernew ,layername);

				clutter_actor_insert_child_at_index(parent, layercontainernew,layercount);
			}
			container = clutter_actor_get_child_at_index (parent,layernum);

		}
	}
	VIEW_MANAGER_PRINT("VIEW MANAGER : Returning layer - %s\n",clutter_actor_get_name(container));

	return container;
}

static ClutterActor *
get_widget_by_name (ThornburyViewManager *view_manager, const gchar *widgetname)
{
	ThornburyViewManagerPrivate* priv;
	ClutterActor *widget_by_name = NULL;
	gboolean bIsGlobalWidget = FALSE ;
	gchar *pViewName = NULL;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), NULL);
	g_return_val_if_fail (widgetname != NULL && *widgetname != '\0', NULL);

	priv = thornbury_view_manager_get_instance_private (view_manager);

	pViewName = g_hash_table_lookup (priv->global_widgets, widgetname);
	if(NULL != pViewName)
	{
			widget_by_name = get_widget_by_name_and_view (view_manager, pViewName ,widgetname ) ;
			bIsGlobalWidget = TRUE ;
	}

	if(FALSE == bIsGlobalWidget)
	{
		//view iteration
		GHashTableIter viewiter;
		gpointer viewkey = NULL;
		gpointer viewvalue = NULL;

		//widget iteration
		GHashTableIter widgetiter;
		gpointer strwidgetname = NULL;
		gpointer widget = NULL;

		GHashTable *widgets = NULL;

		//initialise the iterator
		g_hash_table_iter_init (&viewiter, priv->viewsStructure);

		//start iteration of views
		while (g_hash_table_iter_next (&viewiter, &viewkey, &viewvalue))
		{
			widgets = (GHashTable*)viewvalue;
			if(widgets)
			{
				g_hash_table_iter_init (&widgetiter, widgets);
				while (g_hash_table_iter_next (&widgetiter, &strwidgetname, &widget))
				{
					ThornburyViewManagerWidgetData *widgetdata = (ThornburyViewManagerWidgetData *)widget;
					if(widgetdata)
					{
						VIEW_MANAGER_PRINT("VIEW MANAGER : widget name = %s\n",widgetdata->widgetname);
						if(!g_strcmp0(strwidgetname,widgetname))
						{
							widget_by_name = CLUTTER_ACTOR(widgetdata->widget);
							return widget_by_name;
						}

					}
				}

			}
		}
	}
	return widget_by_name;
}


static ClutterActor *
get_widget_by_name_and_view (ThornburyViewManager *view_manager,
                             const gchar *pViewName,
                             const gchar* widgetname)
{
	ThornburyViewManagerPrivate* priv;
	ClutterActor *widget_by_name = NULL;
	GHashTable *pWidgetsHash;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), NULL);
	g_return_val_if_fail (pViewName != NULL && *pViewName != '\0', NULL);
	g_return_val_if_fail (widgetname != NULL && *widgetname != '\0', NULL);

	priv = thornbury_view_manager_get_instance_private (view_manager);
	pWidgetsHash = g_hash_table_lookup (priv->viewsStructure, pViewName);

	if(NULL != pWidgetsHash)
	{
		ThornburyViewManagerWidgetData *widgetdata = g_hash_table_lookup(pWidgetsHash ,widgetname );
		if(NULL != widgetdata)
		{
			widget_by_name = CLUTTER_ACTOR(widgetdata->widget);
			return widget_by_name;
		}
	}
	return widget_by_name;
}


/**
 * thornbury_get_vm_ptr:
 *
 * As View-Manager is a singleton instance, calling thornbury_get_vm_ptr() will return the #ThornburyViewManager object.
 *
 * Returns: (transfer none): #ThornburyViewManager
 *
 * Since: 1.0
 */

ThornburyViewManager *
thornbury_get_vm_ptr (void)
{
	VIEW_MANAGER_PRINT("VIEW MANAGER : %s\n",__FUNCTION__);
	return thornbury_view_manager;
}

/**
 * thornbury_set_widgets_models:
 * @view_manager:  view manager pointer
 * @models: Hash table containing a map of widget names and their models
 *
 * Call this function whenever models need to be assigned to widgets.
 * Note: Take care to keep widget names unique
 *
 *  |[<!-- language="C" -->
 *
 *	GHashTable *pListViewModels = g_hash_table_new(g_str_hash,g_str_equal);
 *	ThornburyModel *pModel = (ThornburyModel *)thornbury_list_model_new (2,
 *			G_TYPE_STRING, NULL,
 *			G_TYPE_POINTER, NULL,-1);
 *	g_hash_table_insert(pListViewModel,"WidgetName" ,pModel);
 *	// Model for multiple widgets can be passed by appending the details to hash table
 *	set_widgets_models(pViewMgrObj,pListViewModels);
 * ]|
 * Returns: ThornburyViewManagerError. If all is well the returns %THORNBURY_VIEW_MANAGER_OK
 *
 * Since: 1.0
 */


ThornburyViewManagerError thornbury_set_widgets_models(ThornburyViewManager *view_manager, GHashTable *models)
{
	ThornburyViewManagerPrivate* priv;
	ThornburyViewManagerError err = THORNBURY_VIEW_MANAGER_OK;

	ThornburyViewManagerWidgetData *wData = NULL;
	GHashTableIter iter;
	gpointer key = NULL;
	gpointer value = NULL;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager),
	                      THORNBURY_VIEW_MANAGER_INVALID_VIEW_MGR);
	g_return_val_if_fail (models != NULL, THORNBURY_VIEW_MANAGER_INVALID_APP_DATA);

	priv = thornbury_view_manager_get_instance_private (view_manager);

	//initialise the iterator
	g_hash_table_iter_init (&iter, models);
	//start iteration of models
	while (g_hash_table_iter_next (&iter, &key, &value))
	{
		gchar *ref_view;
		gchar *widgetname;

		if(NULL == key || NULL == value)
		{
			err = THORNBURY_VIEW_MANAGER_INVALID_APP_DATA;
			return err;
		}

		widgetname = key;
		ref_view = g_hash_table_lookup (priv->global_widgets, widgetname);

		if(NULL != ref_view)
		{
			wData = thornbury_get_widget_data_view (view_manager, widgetname, ref_view);
		}
		else
		{
			wData = get_widget_data (view_manager, widgetname);
		}

		if(NULL == wData) return err;

		if (wData->widget && G_IS_OBJECT (value))
		{
			GType type = G_OBJECT_TYPE(wData->widget);
			VIEW_MANAGER_PRINT("VIEW MANAGER : Found widget %s\n",g_type_name(type));
			g_object_set (wData->widget, "model", value, NULL);
		}
	}

	return err;
}

/**
 * thornbury_build_all_views:
 * @view_manager:  view manager pointer
 *
 * This function is used to create widgets in all views. Views are not constructed unless thornbury_build_all_views() is called.
 * If multiple views are present, default view will be shown. Calling this function at startup will affect the performance
 * as widgets and views which may be needed less often are also created and hence consume considerable program memory.
 * Call thornbury_build_and_restore_last_view() for restoring the view based on the history from previous session of application.
 *
 * Returns: A #ThornburyViewManagerError enum. %THORNBURY_VIEW_MANAGER_OK on no error.
 *
 */

ThornburyViewManagerError thornbury_build_all_views(ThornburyViewManager *view_manager)
{
	ThornburyViewManagerPrivate* priv;
	GHashTableIter iter;

	gpointer key = NULL;
	gpointer value = NULL;
	ThornburyViewManagerViewData *viewdata = NULL;
	ThornburyViewManagerError err = THORNBURY_VIEW_MANAGER_OK;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager),
	                      THORNBURY_VIEW_MANAGER_INVALID_VIEW_MGR);

	priv = thornbury_view_manager_get_instance_private (view_manager);

	//initialise the iterator
	g_hash_table_iter_init (&iter, priv->viewsStructure);

	//start iteration and create widgets
	while (g_hash_table_iter_next (&iter, &key, &value))
	{
		GHashTable *widgetdata;
		gchar *_viewname;
		if(NULL == key || NULL == value)
		{
			err = THORNBURY_VIEW_MANAGER_INIT_ERROR;
			return err;
		}

		_viewname = key;
		//key is view name and value is hashtable of widgets
		widgetdata = value;
		viewdata = thornbury_get_view_data (view_manager, _viewname);
		if(FALSE == viewdata->bViewCreated)
		{
			err = build_view (view_manager, viewdata, widgetdata);
		}
		else
		{
			/* TODO :  retain the view but unref the model and signal handlers */

			/* invoke destroy signal on all actor and disconnect all signal handlers associated with actor */
			/* model associated with the actor ha to be freed by application as it will be having the pointer reference */


			VIEW_MANAGER_PRINT(" \n\n Rebuilding %s View \n\n" ,viewdata->view_name );
			clutter_actor_destroy_all_children(viewdata->view);
			err = build_view (view_manager, viewdata, widgetdata);
		}
	}
	return err;

}


/**
 * thornbury_build_and_restore_last_view:
 * @view_manager:  view manager pointer
 * @pReturnRestoredView: (out callee-allocates): Return location for storing restored view name.
 * @pReturnAppDataHash : (out callee-allocates): #GHashTable Return location for storing application data .
 *
 * Calling this function will create the default view if application is launched for the very first time.
 * On subsequent sessions, last view from the previous session will be restored.
 *
 * It also returns the name of the view restored via @pReturnRestoredView and application data as preserved
 * during the previous session.
 *
 * Returns: A #ThornburyViewManagerError enum, %THORNBURY_VIEW_MANAGER_OK on no error.
 *
 */

ThornburyViewManagerError thornbury_build_and_restore_last_view(ThornburyViewManager *view_manager ,
		gchar **pReturnRestoredView , GHashTable **pReturnAppDataHash)
{
	ThornburyViewManagerPrivate* priv;
	ThornburyViewManagerError err = THORNBURY_VIEW_MANAGER_OK;
	gchar *pRestoredView = NULL ;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager),
	                      THORNBURY_VIEW_MANAGER_INVALID_VIEW_MGR);

	priv = thornbury_view_manager_get_instance_private (view_manager);

	/* create the application specific view manager DB */
	v_thornbury_view_manager_create_application_pdi (priv->app_name);

#if DISABLE_BUILD_BY_NAME
	err = thornbury_build_all_views(view_manager);
#endif
	if(err == THORNBURY_VIEW_MANAGER_OK)
	{
//		v_view_manager_map_gloabl_widgets(viewdata , widgetdata);
		/* restore the last viewed view by reading DB */
		pRestoredView =
			p_thornbury_view_manager_restore_any_non_default_view (thornbury_view_manager, 
			                                                       &priv->viewHistory);
		VIEW_MANAGER_PRINT("VIEW MANAGER : %s  %d pRestoredView = %s \n",__FUNCTION__ , __LINE__ , pRestoredView);
		if(NULL != pRestoredView)
		{
			if(NULL != pReturnAppDataHash)
				*pReturnAppDataHash = p_thornbury_view_manager_read_app_data_from_db();
		*pReturnRestoredView = pRestoredView ;
		}
		else
		{
			*pReturnRestoredView = g_strdup (priv->default_view);
		}
	}
	return err;
}


/**
 * thornbury_build_view_by_name:
 * @view_manager:  view manager pointer
 * @viewname : name of the view that needs to be created/ constructed
 *
 * This function is used to create a specific view. the view
 * to be created should be provided by the application via @viewname
 *
 * Returns: A #ThornburyViewManagerError enum, %THORNBURY_VIEW_MANAGER_OK on no error.
 *
 */


ThornburyViewManagerError thornbury_build_view_by_name(ThornburyViewManager *view_manager, gchar *viewname)
{
	ThornburyViewManagerPrivate* priv;
	GHashTableIter iter;
	gpointer key = NULL;
	gpointer value = NULL;
	ThornburyViewManagerViewData *viewdata = NULL;
	ThornburyViewManagerError err = THORNBURY_VIEW_MANAGER_OK;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager),
	                      THORNBURY_VIEW_MANAGER_INVALID_VIEW_MGR);
	g_return_val_if_fail (viewname != NULL && *viewname != '\0',
	                      THORNBURY_VIEW_MANAGER_SET_PROP_ERR);

	priv = thornbury_view_manager_get_instance_private (view_manager);

	//initialise the iterator
	g_hash_table_iter_init (&iter, priv->viewsStructure);


	//start iteration and create widgets
	while (g_hash_table_iter_next (&iter, &key, &value))
	{
		GHashTable *widgetdata;
		gchar *tmpviewname;

		if(NULL == key || NULL == value)
		{
			err = THORNBURY_VIEW_MANAGER_INIT_ERROR;
			return err;
		}

		tmpviewname = key;
		widgetdata = value;

		if(!g_strcmp0 (tmpviewname,viewname))
		{
			viewdata = thornbury_get_view_data (view_manager, viewname);
			err = build_view (view_manager, viewdata, widgetdata);
		}
	}
	return err;
}

/**
 * thornbury_switch_view:
 * @view_manager:  view manager pointer
 * @view_to_switch : name of the view that needs to be shown / switched to
 * @addtohistory: Indicates if the view should be retained in view switch history or not.
 * @iSelected_row: Row to be focused.
 *
 * Use this to switch between views in an  application with animation. When application
 * calls this, the @addtohistory should be set to %TRUE if view needs to be added to the view-history stack.
 * A view can be skipped from adding to the history if it is coupled/relevant to a particular view by
 * setting @addtohistory to %FALSE
 *
 * Since: 1.0
 */

void thornbury_switch_view(ThornburyViewManager *view_manager,gchar *view_to_switch, gboolean addtohistory,guint iSelected_row)
{
	v_switch_view *func = NULL;
	ThornburyViewManagerPrivate *priv;
	ThornburyViewManagerViewData *current_view_data;

	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager));

	priv = thornbury_view_manager_get_instance_private (view_manager);

	current_view_data = thornbury_get_view_data (view_manager, priv->current_view);

	if(current_view_data)
	{
		if(NULL == view_to_switch || NULL == priv->current_view)
		{
			g_warning("Cannot switch to / from empty view");
			return;
		}
		if(0 != g_strcmp0(view_to_switch,priv->current_view))
		{
			ThornburyViewManagerViewData *newview = thornbury_get_view_data (view_manager, view_to_switch);
			if(NULL == newview)
			{
				VIEW_MANAGER_PRINT("VIEW MANAGER : Invalid View");
			}
			else//TODO :  complex function for hide and show?
			{
				VIEW_MANAGER_PRINT("VIEW MANAGER : Switching to view %s\n",view_to_switch);

				//collapse spellers and drawers

				//put current view to history and then make it invisble
				if (addtohistory)
				{
					add_to_view_history (view_manager, newview->view_name);
				}

				g_mutex_lock (&priv->lock);

				g_signal_emit (G_OBJECT (view_manager),
					view_manager_signals[SIGNAL_SWITCH_BEGIN], 0,
					view_to_switch);

				if (g_module_symbol (priv->module, "v_switch_view", (gpointer *)&func))
				{

					func(view_manager,current_view_data,newview,TRUE,iSelected_row);
				}
				else
				{
					thornbury_clone_widgets (view_manager, current_view_data, newview);
					clutter_actor_show(newview->view);
					clutter_actor_hide(current_view_data->view);
					thornbury_remove_clone (view_manager, current_view_data->view);
					g_clear_pointer (&priv->current_view, g_free);
					priv->current_view = g_strdup(newview->view_name);
					g_signal_emit (G_OBJECT (view_manager),
						view_manager_signals[SIGNAL_SWITCH_END], 0,
						priv->current_view);
				}

/*
				priv->current_view = newview->view_name;
*/
				g_mutex_unlock (&priv->lock);

			}
			//g_signal_emit(view_manager,view_manager_signals[SIGNAL_SWITCH_END],0,view_manager->priv->current_view);
		}
		else
		{
			VIEW_MANAGER_PRINT("VIEW MANAGER : Current view same as new View");
		}
	}

}


/**
 * thornbury_switch_view_no_animation:
 * @view_manager:  view manager pointer
 * @view_to_switch : name of the view that needs to be shown / switched to
 * @addtohistory: Indicates if the view should be retained in view switch history or not.
 * @iSelected_row: Row to be focused.
 *
 *
 * Use this to switch between views in an application without any animation effects. When application
 * calls this, the @addtohistory should be set to %TRUE if view needs to be added to the view-history stack.
 * A view can be skipped from adding to the history if it is coupled/relevant to a particular view by
 * setting @addtohistory to %FALSE
 *
 * Since: 1.0
 */



void thornbury_switch_view_no_animation(ThornburyViewManager *view_manager,gchar *view_to_switch, gboolean addtohistory,guint iSelected_row)
{
	v_switch_view *func = NULL;
	ThornburyViewManagerViewData *current_view_data;
	ThornburyViewManagerPrivate *priv;

	VIEW_MANAGER_PRINT("VIEW MANAGER : %s %d view_to_switch = %s \n",__FUNCTION__ , __LINE__ ,view_to_switch);

	if(!view_manager)
	{
		g_warning("Not a valid view manager");
		return;
	}

	priv = thornbury_view_manager_get_instance_private (view_manager);
	if(!priv)
	{
		g_warning("Not a well formed view manager");
		return;
	}

	current_view_data = thornbury_get_view_data (view_manager, priv->current_view);

	if(current_view_data)
	{
		if(NULL == view_to_switch || NULL == priv->current_view)
		{
			g_warning("Cannot switch to / from empty view");
			return;
		}
		if(0 != g_strcmp0(view_to_switch,priv->current_view))
		{
			ThornburyViewManagerViewData *newview = thornbury_get_view_data (view_manager, view_to_switch);
			if(NULL == newview)
			{
				VIEW_MANAGER_PRINT("VIEW MANAGER : Invalid View");
			}
			else//TODO :  complex function for hide and show?
			{
				VIEW_MANAGER_PRINT("VIEW MANAGER : Switching to view %s\n",view_to_switch);



				//put current view to history and then make it invisble
				if (addtohistory)
				{
					add_to_view_history (view_manager, newview->view_name);
				}
				else //you have coem here from BACK so, remove previous view
				{
//					remove_from_view_history(priv->current_view);
				}

				g_mutex_lock (&priv->lock);

				g_signal_emit (G_OBJECT (view_manager),
					view_manager_signals[SIGNAL_SWITCH_BEGIN], 0,
					view_to_switch);

				if (g_module_symbol (priv->module, "v_switch_view", (gpointer *)&func))
				{
					func(view_manager,current_view_data,newview,FALSE,iSelected_row);
				}
				else
				{
					thornbury_clone_widgets (view_manager, current_view_data, newview);
					clutter_actor_hide(current_view_data->view);
					thornbury_remove_clone (view_manager, current_view_data->view);
					g_clear_pointer (&priv->current_view, g_free);
					priv->current_view = g_strdup(newview->view_name);

					clutter_actor_show(newview->view);
					g_signal_emit (G_OBJECT (view_manager),
						view_manager_signals[SIGNAL_SWITCH_END], 0,
						priv->current_view);
				}
/*
				thornbury_remove_clone(current_view_data->view);
				priv->current_view = newview->view_name;
*/
				g_mutex_unlock (&priv->lock);
			}
			//g_signal_emit(view_manager,view_manager_signals[SIGNAL_SWITCH_END],0,view_manager->priv->current_view);
		}
		else
		{
			VIEW_MANAGER_PRINT("VIEW MANAGER : Current view same as new View");
		}

	}

}


static gchar *p_view_manager_get_previous_view(ThornburyViewManager *pViewManager)
{
	ThornburyViewManagerPrivate* priv;
	gchar *prevViewName = NULL ;
	GList *pRecentView;
	gchar *pRecentViewName;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (pViewManager), NULL);

 	priv = thornbury_view_manager_get_instance_private (pViewManager);

	//current view is the default view. No BACK works but what should be done?
	if(NULL == priv->current_view || NULL == priv->default_view)
	{
		g_warning("Cannot switch to / from empty view");
		return NULL;
	}

	if (!g_strcmp0 (priv->current_view, priv->default_view))
	{
		VIEW_MANAGER_PRINT("VIEW MANAGER : Default view so , no BACK possible in VM");
		return NULL; //TODO: what should be done here?
	}

	pRecentView  = g_list_last (priv->viewHistory);
	pRecentViewName = pRecentView->data ;
	if(NULL != pRecentView && NULL != pRecentViewName)
	{
		if (0 == g_strcmp0 (pRecentViewName, priv->current_view))
		{
			/* Load Previous View */
			GList *pPreviousViewLink = pRecentView->prev;
			if(NULL != pPreviousViewLink && NULL != pPreviousViewLink->data)
			{
				prevViewName = g_strdup((gchar*)pPreviousViewLink->data) ;
				VIEW_MANAGER_PRINT("VIEW MANAGER : %s %d view_to_switch = %s \n",__FUNCTION__ , __LINE__ ,prevViewName);
				if(0 != g_strcmp0 (prevViewName, priv->default_view ))
				{
					/* Remove the view from view stack */
					priv->viewHistory = g_list_delete_link(priv->viewHistory ,pRecentView);
					g_free(pRecentViewName);
					pRecentViewName = NULL ;
				}
			}

		}
		else
		{
			/* Assume that the Current View might not be added to the View Stack by the User
			 * and load the most Recent View i.e, the last view in the stack
			 * Retain the view in the view stack
			 */
			 prevViewName = g_strdup((gchar*)pRecentView->data) ;
			 VIEW_MANAGER_PRINT("VIEW MANAGER : %s %d view_to_switch = %s \n",__FUNCTION__ , __LINE__ ,prevViewName);
		}
	}
	return prevViewName ;
}


/******
 * thornbury_load_previous_view:
 * @view_manager:  view manager pointer
 *
 * Call this API to handle BACK button. This function will load the previous view from history.
 * If the current view is the default view, then it returns the control to app manager to terminate the process.
 *
 * Inputs from Application:
 * Get the expected action from Application allowing it to decide on the behavior
 * 1. Ignore the back event temporarily if any important
 * 		task is being processed in the current view. Eg : server update.
 * 2. Alternative action on back press like hiding speller and other expandable widgets
 *
 *
 */
void thornbury_load_previous_view(ThornburyViewManager *view_manager)
{
	ThornburyViewManagerPrivate* priv;
	gboolean bbcontinue = TRUE;
	gchar *prevViewName;

	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager));

	prevViewName = p_view_manager_get_previous_view (view_manager);

	priv = thornbury_view_manager_get_instance_private (view_manager);

	VIEW_MANAGER_PRINT("############## found previous view %s \n",prevViewName );
	if(NULL != prevViewName)
	{
		/*
		 * Inputs from Plugin:
		 * Get the expected action from plugin allowing it to decide on the behavior
		 * when back is pressed.If Back is pressed repeatedly then control is given to plugin
		 * 1. to cancel and restore any on going view-switch animations.
		 * 2. Ignore the back press event untill completion of current animations
		 * 3. Proceed view switch with or without animation
		 * 4. Ignore the back event temporarily if any non cancelable animation
		 * 		is being processed in the current view  */
		gint inPluginAction = i_view_manager_control_view_switch(view_manager , prevViewName);

		if(THORNBURY_IGNORE_VIEW_SWITCH == inPluginAction)
			return;

		/*
		 * Input from Application
		 * We can proceed normal back button pressed action, if the value is TRUE.
		 * if FALSE, we can expect actions
		 *   1. Ignore back button event to handle more important event like update
		 *   2. Do alternative actions like hiding speller and the other widgets
		 */
		g_signal_emit (G_OBJECT (view_manager),
			view_manager_signals[SIGNAL_BACKBUTTON_CONTINUE], 0, prevViewName,
			&bbcontinue);

		if (bbcontinue)
		{
			if (!g_strcmp0 (prevViewName, priv->default_view))
			{
				reset_view_history (view_manager);
			}
			if(THORNBURY_PROCEED_WITHOUT_ANIMATION == inPluginAction)
			{
				thornbury_switch_view_no_animation(view_manager,prevViewName,FALSE,-1);
			}
			else if(THORNBURY_PROCEED_WITH_ANIMATION == inPluginAction)
			{
				thornbury_switch_view(view_manager,prevViewName,FALSE,-1);
			}
		}
	}
	else
		g_warning("Invalid previous view!");
	g_clear_pointer (&prevViewName, g_free);
}

static ThornburyViewManagerError
build_view (ThornburyViewManager *view_manager,
            ThornburyViewManagerViewData *viewdata,
            GHashTable *widgetdata)
{
	gboolean bIsGlobalWidget = FALSE ;
	ThornburyViewManagerError err = THORNBURY_VIEW_MANAGER_OK;
	v_plugin_build_view *pPluginFunc = NULL;
	ThornburyItemFactory *item_factory = NULL;
	ClutterActor *widgetnew = NULL;
	ThornburyViewManagerPrivate* priv;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), 
	                      THORNBURY_VIEW_MANAGER_INVALID_VIEW_MGR);

	priv = thornbury_view_manager_get_instance_private (view_manager);

	if(viewdata)
	{
		GHashTableIter iter;
		gpointer key = NULL;
		gpointer value = NULL;
		GHashTable *widgets = widgetdata;

		if(NULL == widgets)
		{
			err = THORNBURY_VIEW_MANAGER_INIT_ERROR;
			return err;
		}

		//initialise the iterator
		g_hash_table_iter_init (&iter, widgets);

		//start iteration and create widgets
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			ClutterActor *dummyWidget = NULL;
			gchar *pViewName;
			ThornburyViewManagerWidgetData *viewmanager_widgetdata = value;

			widgetnew = NULL ;

			if(NULL == key || NULL == value)
			{
				err = THORNBURY_VIEW_MANAGER_INIT_ERROR;
				return err;
			}

			if(NULL == viewmanager_widgetdata->widget_data->pWidgetPropertyFilePath)
			{
				g_warning("invalid wigdet file");
				err = THORNBURY_VIEW_MANAGER_INIT_ERROR;
				return err;
			}
			//check if there is already a widget with same name and same type
			pViewName = g_hash_table_lookup (priv->global_widgets, key);
			if(NULL != pViewName)
			{
				if(0 == g_strcmp0(viewdata->view_name , pViewName))
				{
					dummyWidget = viewmanager_widgetdata->widget ;
				}
				else
				{
					dummyWidget = get_widget_by_name_and_view (view_manager, pViewName, key) ;
				}
				bIsGlobalWidget = TRUE ;
			}
			else
			{
				bIsGlobalWidget = FALSE ;
				dummyWidget = get_widget_by_name (view_manager, key);
			}
			if(NULL != dummyWidget)
			{
				GType type_1 = thornbury_ui_utility_get_gtype_from_name (viewmanager_widgetdata->widget_data->pWidgetType);
				GType type_2 = G_OBJECT_TYPE(dummyWidget);
				if(type_1 == type_2)
				{
					VIEW_MANAGER_PRINT("VIEW MANAGER : %s  %d Detected Global widget  %s \n",__FUNCTION__ , __LINE__ ,(gchar*) key);
//					g_hash_table_replace (view_manager->priv->global_widgets,g_strdup(widgetdata->widgetname),g_strdup(viewdata->view_name));
					continue; //do not create widget here because it should be a singleton.
				}
			}
			else //no such widget so, create new from itemfactory
			{
				GObject *pObject = NULL;
				GType type = thornbury_ui_utility_get_gtype_from_name (viewmanager_widgetdata->widget_data->pWidgetType);

				//valid prop file is given so, append ".json" to it
				if(g_strcmp0 (viewmanager_widgetdata->widget_data->pWidgetPropertyFilePath, ""))
				{
					gchar* widgetpath = g_strconcat (priv->fpath,
						viewdata->view_name, "/",
						viewmanager_widgetdata->widget_data->pWidgetPropertyFilePath,
						strJSON,
						NULL);

					item_factory = thornbury_item_factory_generate_widget_with_props(type,widgetpath);
					g_free(widgetpath);
				}
				else //no valid prop file, so, create without properties.
				{
					item_factory = thornbury_item_factory_generate_widget(type);
				}
				g_object_get(item_factory, "object", &pObject, NULL);

				widgetnew = CLUTTER_ACTOR(pObject);
				clutter_actor_set_position (widgetnew, viewmanager_widgetdata->widget_data->inX,
					viewmanager_widgetdata->widget_data->inY);
				clutter_actor_set_name (widgetnew, viewmanager_widgetdata->widgetname);
				//got the widget now.
			}
			if(widgetnew)
			{
				ClutterActor *layercontainer;
				if(NULL == viewdata->view)
				{
					g_warning("Un-initialised view");
					err = THORNBURY_VIEW_MANAGER_INIT_ERROR;
					return err;
				}
				layercontainer = get_layer_container (viewmanager_widgetdata->widget_data->inLayer, viewdata->view);
				if(CLUTTER_IS_ACTOR(layercontainer))
				{
					VIEW_MANAGER_PRINT("VIEW MANAGER : Adding to layer container - %s\n",clutter_actor_get_name(layercontainer));
					clutter_actor_add_child(layercontainer, widgetnew);
					if(TRUE == bIsGlobalWidget )
					{
						/* store the view name to which the global widget is currently added */
						g_hash_table_replace (priv->global_widgets,
								g_strdup((gchar*)key), g_strdup(viewdata->view_name));
					}
					viewmanager_widgetdata->widget = widgetnew;
				}
			}
		}
	}
	viewdata->bViewCreated = TRUE ;

	if (!g_strcmp0 (viewdata->view_data->pViewId, priv->default_view))
	{
		//***********************************************************************************************
		GHashTable* widgets;
		GHashTableIter widgetiter;
		gpointer strwidgetname = NULL;
		gpointer widget_data = NULL;
		ThornburyViewManagerViewData *default_view_data = thornbury_get_view_data (view_manager, priv->default_view);

		//get the widget structure of default view
		widgets = g_hash_table_lookup (priv->viewsStructure, priv->default_view);

		if(widgets)
		{
			g_hash_table_iter_init (&widgetiter, widgets);
			while (g_hash_table_iter_next (&widgetiter, &strwidgetname, &widget_data))
			{
				//check fi any of them are global
				if (g_hash_table_contains (priv->global_widgets, strwidgetname))
				{
					//get their data..
					ThornburyViewManagerWidgetData *nv_widgetdata = NULL;
					ClutterActor *widget;
					gpointer view_name;
					ThornburyViewManagerWidgetData *ov_wdata;
					ClutterActor *layer;

					if(widget_data)
					{
						nv_widgetdata = (ThornburyViewManagerWidgetData *)(widget_data);
					}
					//get the view from where you have to clone the widget
					view_name = g_hash_table_lookup (priv->global_widgets, strwidgetname);

					//get data for the original widget
					ov_wdata = thornbury_get_widget_data_view (view_manager, strwidgetname, view_name);
					VIEW_MANAGER_PRINT("VIEW MANAGER : Found widget to be cloned %s from view %s  %d \n",(gchar*)strwidgetname, (gchar*)view_name , __LINE__);

					widget = ov_wdata->widget;
					ov_wdata->widget = NULL;

					if(widget)
					{
						ClutterActor *layer_old = clutter_actor_get_parent(widget);
						VIEW_MANAGER_PRINT("VIEW_MANAGER : Adding clone to layer %s\n",clutter_actor_get_name(layer_old));
						clutter_actor_remove_child(layer_old,widget);
					}

					layer = get_layer_container(nv_widgetdata->widget_data->inLayer,default_view_data->view);
					clutter_actor_set_position(widget,nv_widgetdata->widget_data->inX,nv_widgetdata->widget_data->inY);
					clutter_actor_add_child(layer,widget);
					nv_widgetdata->widget = widget;
					widget = NULL;

					g_hash_table_replace (priv->global_widgets, g_strdup (strwidgetname), g_strdup (default_view_data->view_name));

					VIEW_MANAGER_PRINT("VIEW MANAGER : moved widget %s to view %s\n",(gchar*)strwidgetname, default_view_data->view_name);

				}
			}
		}

		/*********************************************************************************************************/
		VIEW_MANAGER_PRINT("VIEW MANAGER : showing view : %s\n",viewdata->view_data->pViewId);
		clutter_actor_show(viewdata->view);
		g_clear_pointer (&priv->current_view, g_free);
		priv->current_view = g_strdup (viewdata->view_name);

		priv->viewHistory = g_list_append (priv->viewHistory, g_strdup (viewdata->view_name));
	}

	if (g_module_symbol (priv->module, "v_plugin_build_view", (gpointer *) &pPluginFunc))
	{
		/* Call Plugin function to implement any view specific changes */
		pPluginFunc(view_manager,viewdata );
	}
	return err;
}

/**
 * thornbury_set_widgets_controllers:
 * @view_manager:  view manager pointer
 * @signals: Hash table containing a map of widget names and their signals via #ThornburyViewManagerSignalData
 *
 * Call this function when ever signals need to be assigned to widgets.
 * Note: Take care to keep widget names unique
 *
 * Returns: A #ThornburyViewManagerError enum, %THORNBURY_VIEW_MANAGER_OK on no error.
 */

ThornburyViewManagerError thornbury_set_widgets_controllers(ThornburyViewManager *view_manager,GHashTable *signals)
{
	ThornburyViewManagerPrivate* priv;
	ThornburyViewManagerError err = THORNBURY_VIEW_MANAGER_OK;
	GHashTableIter iter;

	gpointer key = NULL;
	gpointer value = NULL;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager),
	                      THORNBURY_VIEW_MANAGER_INVALID_VIEW_MGR);
	g_return_val_if_fail (signals != NULL, THORNBURY_VIEW_MANAGER_INVALID_APP_DATA);

	priv = thornbury_view_manager_get_instance_private (view_manager);

	//initialise the iterator
	g_hash_table_iter_init (&iter, signals);
	//start iteration of models
	while (g_hash_table_iter_next (&iter, &key, &value))
	{
		gint signal_count = 0;
		gchar* widgetname = key;
		GList *lstSignals = value;
		ThornburyViewManagerWidgetData *wData = NULL;

		if(NULL == key || NULL == value)
		{
			err = THORNBURY_VIEW_MANAGER_INVALID_APP_DATA;
			return err;
		}

		VIEW_MANAGER_PRINT("VIEW MANAGER : Setting signals for widget %s\n",widgetname);

		if (g_hash_table_contains (priv->global_widgets, widgetname))
		{
			gchar *ref_view = g_hash_table_lookup (priv->global_widgets, widgetname);
			wData = thornbury_get_widget_data_view (view_manager, widgetname, ref_view);
		}
		else
		{
			wData = get_widget_data (view_manager, widgetname);
		}

		if(NULL == wData )
		{
			g_warning("VIEW MANAGER : Invalid Widget specified %s \n",widgetname);
			err =  THORNBURY_VIEW_MANAGER_INVALID_APP_DATA ;
			continue ;
		}
		while(g_list_nth_data(lstSignals, signal_count))
		{
			ThornburyViewManagerSignalData * wSignalData = (ThornburyViewManagerSignalData *)g_list_nth_data(lstSignals, signal_count);
			g_signal_connect(wData->widget,wSignalData->signalname,wSignalData->func, wSignalData->data);
			signal_count++;
		}
	}

	return err;
}

static ThornburyViewManagerWidgetData *
get_widget_data (ThornburyViewManager *view_manager, const gchar *widgetname)
{
  ThornburyViewManagerWidgetData *widget_data = NULL;
  ThornburyViewManagerPrivate* priv;

  //view iteration
  GHashTableIter viewiter;
  gpointer viewkey = NULL;
  gpointer viewvalue = NULL;

  //widget iteration
  GHashTableIter widgetiter;
  gpointer strwidgetname = NULL;
  gpointer widget = NULL;

  GHashTable *widgets = NULL;

  g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), NULL);
  g_return_val_if_fail (widgetname != NULL && *widgetname != '\0', NULL);

  priv = thornbury_view_manager_get_instance_private (view_manager);

  //initialise the iterator
  g_hash_table_iter_init (&viewiter, priv->viewsStructure);

  //start iteration of views
  while (g_hash_table_iter_next (&viewiter, &viewkey, &viewvalue))
    {
      widgets = viewvalue;
      if (widgets)
        {
          g_hash_table_iter_init (&widgetiter, widgets);
          while (g_hash_table_iter_next (&widgetiter, &strwidgetname, &widget))
            {
              if(!g_strcmp0 (strwidgetname, widgetname))
                {
                  widget_data = widget;
                  break;
                }
            }
	}
    }

  return widget_data;
}

ThornburyViewManagerViewData *
thornbury_get_view_data (ThornburyViewManager *view_manager, const gchar *viewname)
{
  ThornburyViewManagerPrivate* priv;
  ThornburyViewManagerViewData *viewdata = NULL;

  GHashTableIter viewiter;
  gpointer viewkey = NULL;
  gpointer viewvalue = NULL;

  g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), NULL);
  g_return_val_if_fail (viewname != NULL && *viewname != '\0', NULL);

  priv = thornbury_view_manager_get_instance_private (view_manager);

  //initialise the iterator
  g_hash_table_iter_init (&viewiter, priv->view_list);

  //start iteration of views
  while (g_hash_table_iter_next (&viewiter, &viewkey, &viewvalue))
    {
      //key is view name and value is view structure
      if(!g_strcmp0 (viewkey, viewname))
        {
          viewdata = viewvalue;
          break;
        }
    }

  return viewdata;
}


static void
add_to_view_history (ThornburyViewManager *view_manager, const gchar *viewname)
{
  ThornburyViewManagerPrivate *priv;

  g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager));
  g_return_if_fail (viewname != NULL && *viewname != '\0');

  priv = thornbury_view_manager_get_instance_private (view_manager);

  if (!remove_from_view_history (view_manager, viewname))
    {
      priv->viewHistory = g_list_append (priv->viewHistory, g_strdup (viewname));
    }
}


static gboolean
remove_from_view_history (ThornburyViewManager *view_manager, const gchar *pViewToSearch)
{
  ThornburyViewManagerPrivate *priv;
  guint inListLength = 0;

  g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), FALSE);
  g_return_val_if_fail (pViewToSearch != NULL && *pViewToSearch != '\0', FALSE);

  priv = thornbury_view_manager_get_instance_private (view_manager);

  for(; inListLength < g_list_length (priv->viewHistory); inListLength ++)
    {
      gchar *pViewName = g_list_nth_data (priv->viewHistory , inListLength);

      /* Search if the view already exits in the view stack */
      if(NULL != pViewName && 0 == g_strcmp0 (pViewToSearch, pViewName))
        {
          inListLength++;

          while (inListLength != g_list_length (priv->viewHistory))
            {
              /* If View Already exists in the view stack then , delete all history
               * of view traverse from that point.
               */
              GList *pListToDrop = g_list_nth (priv->viewHistory, inListLength);
              gchar *pViewToDrop = (gchar *)pListToDrop->data;
              VIEW_MANAGER_PRINT("%s Removed link = %p viewname = %s \n",__FUNCTION__, pListToDrop , pViewToDrop);
              /* Delete the Link and free the GList link */
              priv->viewHistory = g_list_delete_link(priv->viewHistory , pListToDrop);
              /* Delete the Dynamically Alloted memory */
              g_clear_pointer (&pViewToDrop, g_free);
            }

	  return TRUE;
        }
  }

  return FALSE;
}

static void
reset_view_history (ThornburyViewManager *view_manager)
{
  ThornburyViewManagerPrivate *priv;

  g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager));

  priv = thornbury_view_manager_get_instance_private (view_manager);

  if(NULL != priv->viewHistory)
    {
      /* Free the complete list */
      g_list_free_full (priv->viewHistory, g_free);
      /* Dereference the Old List pointer */
      priv->viewHistory = NULL ;
      /* Finally after removing all, set the list pointer to
       * NULL */
      priv->viewHistory = g_list_append (priv->viewHistory, g_strdup (priv->default_view));
    }
}

static void free_views_list_hash(  gpointer value)
{
	if(value)
	{
		ThornburyViewManagerViewData *vdata = value;
		VIEW_MANAGER_PRINT("freed view name %s\n",vdata->view_name);

		if(NULL != vdata)
		{
			if(NULL != vdata->view)
			{
				/* destroy parent and all its children */
				clutter_actor_destroy(CLUTTER_ACTOR(vdata->view));
				vdata->view = NULL;
			}

			if(NULL != vdata->view_data)
			{
				VIEW_MANAGER_PRINT("freeing..... parser view %s\n",vdata->view_data->pViewId);
				thornbury_view_parser_free_view(vdata->view_data);
				VIEW_MANAGER_PRINT("freed parser view %s\n",vdata->view_data->pViewId);
				vdata->view_data = NULL;

			}

			if(NULL!= vdata->view_file_path)
			{
				VIEW_MANAGER_PRINT("freed view file path - %s\n",vdata->view_file_path);
				g_free(vdata->view_file_path);
				vdata->view_file_path = NULL;
			}
			if(NULL!= vdata->view_name)
			{
				VIEW_MANAGER_PRINT("freed view name - %s\n",vdata->view_name);
				g_free(vdata->view_name);
				vdata->view_name = NULL;
			}

			if(NULL!= vdata->entry_effect)
			{
				VIEW_MANAGER_PRINT("freed view entry effect - %s\n",vdata->entry_effect);
				g_free(vdata->entry_effect);
				vdata->entry_effect = NULL;
			}
			if(NULL!= vdata->exit_effect)
			{
				g_print("freed view exit effect - %s\n",vdata->exit_effect);

				g_free(vdata->exit_effect);
				vdata->exit_effect = NULL;
			}
		}
	}
}


/**
 * thornbury_set_property:
 * @view_manager:  A #ThornburyViewManager object reference.
 * @widget_name: widget name for which a property has to be set.
 * @...: va_list of properties names and values that have to be set . E.g X, Y, type ..
 *
 * Sets widget properties with the given name. Parses the list of variable arguments
 * and sets fields to the value listed(E.g position of roller, Hide/show a widget).
 * Last variable argument should be %NULL.
 *
 * Returns: A #ThornburyViewManagerError enum, %THORNBURY_VIEW_MANAGER_OK on no error.
 */

ThornburyViewManagerError
thornbury_set_property (ThornburyViewManager *view_manager,
                        const gchar *widget_name,
                        ...)
{
	ThornburyViewManagerPrivate* priv;
	ClutterActor *widget;
	ThornburyViewManagerError err = THORNBURY_VIEW_MANAGER_SET_PROP_ERR;

	ThornburyViewManagerWidgetData *wData = NULL;
	v_set_property *func = NULL;
	va_list var_args;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager),
	                      THORNBURY_VIEW_MANAGER_INVALID_VIEW_MGR);
	g_return_val_if_fail (widget_name != NULL && *widget_name != '\0',
	                      THORNBURY_VIEW_MANAGER_SET_PROP_ERR);

	priv = thornbury_view_manager_get_instance_private (view_manager);

	if (g_hash_table_contains (priv->global_widgets, widget_name))
	{
		gchar *ref_view = g_hash_table_lookup (priv->global_widgets, widget_name);
		wData = thornbury_get_widget_data_view (view_manager, widget_name, ref_view);
	}
	else
	{
		wData = get_widget_data (view_manager, widget_name);
	}

	if(NULL == wData) return err;

	widget = wData->widget; //CLUTTER_ACTOR(get_widget_by_name(widgetname));

	if(!widget)
	{
		return err;
	}

	va_start (var_args, widget_name);

	if (g_module_symbol (priv->module, "v_set_property", (gpointer *) &func))
	{
		err = func(view_manager,widget,var_args);
	}
	else
	{
		ClutterActor *actor;
		const gchar *name = NULL;
		GObjectClass *klass = NULL;

		actor = widget; //CLUTTER_ACTOR(get_widget_by_name(widgetname));

		if(NULL != actor )
		{
			klass = G_OBJECT_GET_CLASS (actor);
		}
		else
		{
			g_warning("Invalid widget name!");
			err = THORNBURY_VIEW_MANAGER_INVALID_WIDGET;
			return err;
		}

		name = va_arg (var_args, gchar*);
		while(name)
		{
			GParamSpec *pspec;
			GValue *value;
			gchar *error = NULL;

			if(0 == g_strcmp0(name,"")) return err;

			value = g_new0( GValue , 1);
			pspec = g_object_class_find_property (klass, name);

			if (!pspec)
			{
				g_warning("Cannot set attribute!");
				err = THORNBURY_VIEW_MANAGER_INVALID_WIDGET;
				return err;
			}
			else
			{

				g_value_init (value, G_PARAM_SPEC_VALUE_TYPE (pspec));
				G_VALUE_COLLECT (value, var_args, 0, &error);

				g_object_set_property(G_OBJECT(actor),name,value);
			}


			name = va_arg (var_args, gchar*);
		}

	}
	va_end(var_args);
	return err;


}

static void
widget_property_value_free (GValue *value)
{
  g_value_unset (value);
  g_free (value);
}

/**
 * thornbury_get_property:
 * @view_manager: A #ThornburyViewManager object reference.
 * @widget_name: widget name for which a property has to be read
 * @...: va_list of properties names that have to be read . E.g X, Y, ..
 *
 * Gets widget properties with the given name. Parses the list of variable arguments.
 * Last variable argument should be %NULL.
 *
 * Returns: (transfer container): A newly allocated #GHashTable having key as property name and #GValue for the property.
 * Call g_hash_table_destroy() to free the hash table.
 *
 * Since: 1.0
 *
 */
GHashTable *
thornbury_get_property (ThornburyViewManager *view_manager,
                        const gchar *widget_name,
                        ...)
{
	ThornburyViewManagerPrivate *priv;
	ClutterActor *actor;
	va_list arg_list ;

	GHashTable *proptable;

	const gchar *property_name = NULL;
	GObjectClass *klass = NULL;
	GValue *propvalue = NULL;
	ThornburyViewManagerWidgetData *wData = NULL;

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), NULL);
	g_return_val_if_fail (widget_name != NULL && *widget_name != '\0', NULL);

	priv = thornbury_view_manager_get_instance_private (view_manager);

	proptable = g_hash_table_new_full (g_str_hash, g_str_equal,
		g_free,
		(GDestroyNotify) widget_property_value_free);

	if (g_hash_table_contains (priv->global_widgets, widget_name))
	{
		gchar *ref_view = g_hash_table_lookup (priv->global_widgets, widget_name);
		wData = thornbury_get_widget_data_view (view_manager, widget_name, ref_view);
	}
	else
	{
		wData = get_widget_data (view_manager, widget_name);
	}

	if(NULL == wData) return NULL;

	actor = wData->widget; //CLUTTER_ACTOR(get_widget_by_name(widgetname));
	klass = G_OBJECT_GET_CLASS (actor);

	va_start (arg_list,widget_name);

	property_name = va_arg (arg_list, const gchar *);

	while (property_name)
	{
		GParamSpec *pspec;

		if(g_strcmp0 (property_name, "") == 0)
		{
			return proptable;
		}

		VIEW_MANAGER_PRINT ("VIEW MANAGER : property : %s\n", property_name);

		pspec = g_object_class_find_property (klass, property_name);
		propvalue = g_new0( GValue , 1);


		if (!pspec)
		{
			g_warning ("invalid property name %s", property_name);

			g_free (propvalue);

			return proptable;
		}
		else
		{

			g_value_init (propvalue, G_PARAM_SPEC_VALUE_TYPE (pspec));
			g_object_get_property (G_OBJECT(actor), property_name, propvalue);
		}

		g_hash_table_insert (proptable, g_strdup (property_name), propvalue);

		property_name = va_arg (arg_list, const gchar *);
	}

	va_end(arg_list);

	return proptable ;
}

static void
view_manager_hard_keys_name_appeared (GDBusConnection *connection,
                                      const gchar *name,
                                      const gchar *name_owner,
                                      gpointer user_data)
{
  g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (user_data));

  canterbury_hard_keys_proxy_new (connection,
                                  G_DBUS_PROXY_FLAGS_NONE,
                                  CANTERBURY_BUS_NAME,
                                  CANTERBURY_HARDKEYS_OBJECT_PATH,
                                  NULL,
                                  view_manager_hard_keys_proxy_clb,
                                  user_data);
}

static void
view_manager_hard_keys_proxy_clb (GObject *source_object,
                                  GAsyncResult *res,
                                  gpointer user_data)
{
  ThornburyViewManager *view_manager = user_data;
  ThornburyViewManagerPrivate *priv;
  GError *error = NULL;

  g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager));

  priv = thornbury_view_manager_get_instance_private (view_manager);

  priv->hardkeys_mgr_proxy = canterbury_hard_keys_proxy_new_finish (res, &error);

  if (priv->hardkeys_mgr_proxy == NULL)
    {
      g_printerr("error %s", g_dbus_error_get_remote_error (error));
      g_clear_error (&error);
      return;
    }

  g_signal_connect (priv->hardkeys_mgr_proxy,
                    "back-press-register-response",
                    G_CALLBACK (view_manager_back_press_status),
                    user_data);

  canterbury_hard_keys_call_register_back_press_sync (priv->hardkeys_mgr_proxy,
                                                      priv->app_name,
                                                      NULL, NULL);
}



static gint i_view_manager_control_view_switch(
		ThornburyViewManager *pThornburyViewManager , gchar *pLoadingView)
{
	gint inPluginAction = THORNBURY_PROCEED_WITHOUT_ANIMATION ;
	i_action_on_back_press *i_plugin_action_on_back_press = NULL;
	ThornburyViewManagerPrivate *priv = thornbury_view_manager_get_instance_private (pThornburyViewManager);

	if (g_module_symbol (priv->module,"i_action_on_back_press",
			(gpointer *)&i_plugin_action_on_back_press))
	{
		inPluginAction = i_plugin_action_on_back_press(pThornburyViewManager ,pLoadingView );
	}
	return inPluginAction ;
}

static void
view_manager_check_for_back_handling (ThornburyViewManager *self)
{
  /* back button continue returned from app,if TRUE,Viewmanger handles back button or else ignores */
  gboolean bbcontinue = TRUE;
  gboolean is_default_view = FALSE;
  ThornburyViewManagerPrivate *priv;

  priv = thornbury_view_manager_get_instance_private (self);

  is_default_view = g_strcmp0 (priv->current_view, priv->default_view) == 0;

  if (is_default_view)
    {
      /*
       * Signal emmitted to  indicate that back is pressed.And it expects input from application,
       * which indicates viewmanger has to handle the back press or ignore it.
       * if the retuen value is TRUE, Viewmanager will handle by allowing app to get killed/background 
       * state, by canterbury, if it is default view.
       * If the returned value is FALSE, then viewmanger will ignore the back press, and it will 
       * send TRUE to canterbury, which will indicates that canterbury too will not handle back 
       * press.
       */

      g_signal_emit (G_OBJECT (self),
                     view_manager_signals[SIGNAL_BACKBUTTON_CONTINUE], 0,
                     NULL, &bbcontinue);

      if (bbcontinue && priv->bEnableScreenShot &&
          priv->pScreenShotPath != NULL)
        {
          /* Take screen shot of the application before going to background/killed state */
          v_thornbury_view_manager_take_app_screenshot ();
        }

      canterbury_hard_keys_call_back_press_consumed_sync (priv->hardkeys_mgr_proxy,
                                                          priv->app_name,
                                                          !bbcontinue, NULL,
                                                          NULL);
    }
  else
    {
      /*  
       * If not default view then canterbury should not handle the back press.
       * and then load the previous view considering "SIGNAL_BACKBUTTON_CONTINUE" return value.
       */
      canterbury_hard_keys_call_back_press_consumed_sync (priv->hardkeys_mgr_proxy,
                                                          priv->app_name,
                                                          TRUE, NULL, NULL);
      thornbury_load_previous_view (self);
    }

}

static gboolean
view_manager_back_press_status (CanterburyHardKeys *object,
                                const gchar *app_name,
                                gpointer user_data)
{
  ThornburyViewManager *view_manager = user_data;
  ThornburyViewManagerPrivate *priv;
  /* back button continue returned from app,if TRUE,Viewmanger handles back button or else ignores */
  gboolean bbcontinue = TRUE;
  gboolean is_default_view = FALSE;

  g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), FALSE);

  priv = thornbury_view_manager_get_instance_private (view_manager);

  g_return_val_if_fail (priv->app, FALSE);

  if (NULL == priv->current_view || NULL == priv->default_view)
    {
      g_printerr("No default view/current view set");
      return FALSE;
    }

  if (g_strcmp0 (app_name, priv->app_name) != 0)
    {
      /* FIXME: Ignoring signal is ok? */
      return FALSE;
    }
 
  /* check if viewmangaer has to handle the back presss or application */ 
  view_manager_check_for_back_handling (view_manager);

  return FALSE;
}

static void
view_manager_hard_keys_name_vanished (GDBusConnection *connection,
                                      const gchar *name,
                                      gpointer user_data)
{
  ThornburyViewManager *view_manager = user_data;
  ThornburyViewManagerPrivate *priv;

  g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager));

  priv = thornbury_view_manager_get_instance_private (view_manager);

  g_clear_object (&priv->hardkeys_mgr_proxy);
}

static ClutterActor *
create_image (const gchar *path, gint width, gint height)
{
  ClutterActor *box = NULL;
  ClutterContent *img = NULL;
  GdkPixbuf *pixbuf = NULL;
  GError *err = NULL;

  pixbuf = gdk_pixbuf_new_from_file_at_scale (path, width, height, FALSE, &err);
  if (!pixbuf)
    {
      /* FIXME: need a common method to print debugging message */
      g_warning ("Failed to create image: %s", err->message);
      g_clear_error (&err);
      goto finally;
    }

  img = clutter_image_new ();

  if (!clutter_image_set_data (CLUTTER_IMAGE (img),
                               gdk_pixbuf_get_pixels (pixbuf), 
                               gdk_pixbuf_get_has_alpha (pixbuf)
                                 ? COGL_PIXEL_FORMAT_RGBA_8888
                                 : COGL_PIXEL_FORMAT_RGB_888,
                               gdk_pixbuf_get_width (pixbuf),
                               gdk_pixbuf_get_height (pixbuf),
                               gdk_pixbuf_get_rowstride (pixbuf),
                               &err))
    {
      g_warning ("Failed to set image data: %s", err->message);
      g_clear_error (&err);

      goto finally;
    }

  box = clutter_actor_new ();
  clutter_actor_set_content (box, img);
  clutter_actor_set_size (box,
    gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf));
  
finally:
  g_clear_object (&img);
  g_clear_object (&pixbuf);

  return box;
}

static gchar *
get_widget_view (ThornburyViewManager *self, gchar *widgetname)
{
	ThornburyViewManagerPrivate *priv = thornbury_view_manager_get_instance_private (self);
	gchar *view_name = NULL;

	//view iteration
	GHashTableIter viewiter;
	gpointer viewkey = NULL;
	gpointer viewvalue = NULL;


	GHashTable *widgets = NULL;

	//initialise the iterator
	g_hash_table_iter_init (&viewiter, priv->viewsStructure);

	//start iteration of views
	while (g_hash_table_iter_next (&viewiter, &viewkey, &viewvalue))
	{
		widgets = (GHashTable*)viewvalue;
		if(widgets)
		{
			if(g_hash_table_contains (widgets,widgetname))
			{
				view_name = g_strdup(viewkey);
				break;
			}

		}

	}

	return view_name;
}

/**
 * thornbury_clone_widgets:
 * @view_manager: The instance of #ThornburyViewManager
 * @current_view_data: A #ThornburyViewManagerViewData struct having current view info.
 * @new_view_data: A #ThornburyViewManagerViewData struct having new view info.
 *
 * Clone widgets to old view or exiting view and adds the source widget to the entry view.
 * Helpful while view switching
 *
 * thornbury_clone_widgets() is a Plugin convenient API and should not be called by the application.
 *
 * Since: 1.0
 */
void
thornbury_clone_widgets (ThornburyViewManager *view_manager,
                         ThornburyViewManagerViewData *current_view_data,
                         ThornburyViewManagerViewData *new_view_data)
{
	ThornburyViewManagerPrivate *priv;

	GHashTableIter widgetiter;
	gpointer strwidgetname = NULL;
	gpointer widget_data = NULL;
	GHashTable* widgets;

	g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager));
	g_return_if_fail (current_view_data != NULL);
	g_return_if_fail (new_view_data != NULL);

	priv  = thornbury_view_manager_get_instance_private (view_manager);

	widgets = g_hash_table_lookup (priv->viewsStructure, new_view_data->view_name);

	if(widgets)
	{
		g_hash_table_iter_init (&widgetiter, widgets);
		while (g_hash_table_iter_next (&widgetiter, &strwidgetname, &widget_data))
		{
			gchar *temp_viewname = g_hash_table_lookup (priv->global_widgets, strwidgetname);
			if(NULL != temp_viewname)
			{
				ThornburyViewManagerWidgetData *nv_widget_data = (ThornburyViewManagerWidgetData *)widget_data;
				//get the current widget and it's position
				ThornburyViewManagerWidgetData *ov_widget_data = thornbury_get_widget_data_view (view_manager,
				                                                                                 strwidgetname,
				                                                                                 temp_viewname);
				ClutterActor *widget = ov_widget_data->widget;
				ClutterActor *layer;
				ClutterActor *layer_old;

				ov_widget_data->widget = NULL;

				if(NULL == widget) return;

				layer_old = clutter_actor_get_parent (widget); //get_layer_container(nv_widget_data->inLayer,current_view_data->view);

				//replace the actual widget by it's clone
				if(layer_old)
				{
					if(!g_strcmp0((gchar*)temp_viewname,priv->current_view))
					{
						//add clone only if the original widget is present in previous view
						gchar *name;
						ClutterActor *clone = clutter_clone_new(widget);

						//set the position to cloned widget
						clutter_actor_set_position(clone,ov_widget_data->widget_data->inX,ov_widget_data->widget_data->inY);
						clutter_actor_set_size(clone,clutter_actor_get_width(widget),clutter_actor_get_height(widget));

						name = g_strdup_printf("%s%s", ov_widget_data->widgetname, "_CLONE");
						clutter_actor_set_name(clone, name);
						g_free(name);
						clutter_actor_remove_child(layer_old,widget);
						clutter_actor_add_child(layer_old,clone);
						ov_widget_data->widget = clone;

						VIEW_MANAGER_PRINT("VIEW MANAGER : Adding clone to layer %s\n",clutter_actor_get_name(layer_old));

					}
					else
						clutter_actor_remove_child(layer_old,widget);
				}
				//now add the actual widget to the new view - at position defined in new view.
				layer = get_layer_container (nv_widget_data->widget_data->inLayer, new_view_data->view);
				clutter_actor_add_child(layer,widget);
				clutter_actor_set_position(widget,nv_widget_data->widget_data->inX,nv_widget_data->widget_data->inY);

				//changing the internal strcuture
				nv_widget_data->widget = widget;
				widget = NULL;
				g_hash_table_replace (priv->global_widgets, g_strdup (strwidgetname), g_strdup (new_view_data->view_name));

			}
		}
	}
}

/**
 * thornbury_remove_clone:
 * @view_manager: The instance of #ThornburyViewManager
 * @view: A #ThornburyViewManagerViewData struct having current view info.
 *
 * Remove clone from the old view which was added on calling thornbury_clone_widgets().
 * Helpful after view switching.
 *
 * Since: 1.0
 */

void thornbury_remove_clone (ThornburyViewManager *view_manager, ClutterActor *view)
{
  guint child_count = 0;
  GList *children;

  g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager));
  g_return_if_fail (CLUTTER_IS_ACTOR (view));

  children = clutter_actor_get_children (view);

  for(child_count = 0; child_count < g_list_length (children); child_count++)
    {
      ClutterActor *child = CLUTTER_ACTOR (g_list_nth_data (children, child_count));

      if (CLUTTER_IS_CLONE (child))
        {
          clutter_actor_remove_child(view,child);
        }

      if(CLUTTER_IS_ACTOR (child))
        {
          thornbury_remove_clone (view_manager, child);
        }
    }
}

/**
 * thornbury_get_widget_data_view:
 * @view_manager: The instance of #ThornburyViewManager
 * @widgetname: Widget name for which the data has to be retrieved
 * @viewname: View name for which the widget belongs.
 *
 * thornbury_get_widget_data_view() is a Plugin convenient API and should not be called by the application.
 *
 * Returns: (transfer none): A reference pointer to #ThornburyViewManagerWidgetData.
 * Returned value is owned by the view manager and  should not be freed.
 *
 * Since: 1.0
 */
ThornburyViewManagerWidgetData *
thornbury_get_widget_data_view (ThornburyViewManager *view_manager,
                                const gchar *widgetname,
                                const gchar *viewname)
{
  ThornburyViewManagerPrivate *priv;
  GHashTableIter widgetiter;
  gpointer strwidgetname = NULL;
  gpointer widget = NULL;
  GHashTable *widgets = NULL;

  ThornburyViewManagerWidgetData *widget_data = NULL;

  g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), NULL);
  g_return_val_if_fail (widgetname != NULL && *widgetname != '\0', NULL);
  g_return_val_if_fail (viewname != NULL && viewname != '\0', NULL);

  priv = thornbury_view_manager_get_instance_private (view_manager);

  widgets = g_hash_table_lookup (priv->viewsStructure, viewname);

  if (widgets == NULL)
    return NULL;

  g_hash_table_iter_init (&widgetiter, widgets);

  while (g_hash_table_iter_next (&widgetiter, &strwidgetname, &widget))
    {
      if (!g_strcmp0 (strwidgetname, widgetname))
        {
          widget_data = widget;
          break;
        }
    }

  return widget_data;
}

ThornburyViewManagerError thornbury_set_widgets_animation(ThornburyViewManager *view_manager,GHashTable *animations)
{
	ThornburyViewManagerPrivate *priv = thornbury_view_manager_get_instance_private (view_manager);
	ThornburyViewManagerError err = THORNBURY_VIEW_MANAGER_OK;
	GHashTableIter widgetiter;
	gpointer strwidgetname = NULL;
	gpointer anim = NULL;

	v_set_animation *func = NULL;

	g_hash_table_iter_init (&widgetiter, animations);
	while (g_hash_table_iter_next (&widgetiter, &strwidgetname, &anim))
	{
		ThornburyViewManagerAnimData *anim_data = (ThornburyViewManagerAnimData *)anim;
		ThornburyViewManagerWidgetData *wdata = thornbury_get_widget_data_view (view_manager,
		                                                                        strwidgetname,
		                                                                        anim_data->view_name);
		ClutterActor *widget = wdata->widget;

		wdata->anim_data = anim_data;

		//if animation should start on signal, the come here else just assign the anim data to widget
		if((widget != NULL) && (NULL != anim_data->signal_name))
		{
			if (g_module_symbol (priv->module, "v_set_animation", (gpointer *)&func))
			{
				func(view_manager,widget,anim_data);
			}
		}
	}
	return err;
}

static void
view_exit_animation_completed (ThornburyViewManager *view_manager,
                               const gchar *current_view,
                               const gchar *new_view,
                               gpointer user_data)
{
  ThornburyViewManagerPrivate *priv;
  ThornburyViewManagerViewData *current_view_data;
  ThornburyViewManagerViewData *newview;

  v_on_exit_animation_complete *v_plugin_exit_animation_complete = NULL;

  /* FIXME: This signal is emitted by Canterbury.
   * This might be G_SIGNAL_ACTION.
   */
  g_return_if_fail (current_view != NULL && *current_view != '\0');
  g_return_if_fail (new_view != NULL && *new_view != '\0');

  priv = thornbury_view_manager_get_instance_private (view_manager);

  current_view_data = thornbury_get_view_data (view_manager, priv->current_view);
  newview = thornbury_get_view_data (view_manager, new_view);

  /* FIXME: v_on_exit_animation_complete can be g_signal */
  if (g_module_symbol (priv->module,"v_on_exit_animation_complete",
                       (gpointer *) &v_plugin_exit_animation_complete))
    {
      v_plugin_exit_animation_complete (view_manager, current_view_data, newview);
    }
  else
    {
      thornbury_clone_widgets (view_manager, current_view_data, newview);

      if (NULL != priv->trans_bg_img)
        clutter_actor_show (priv->trans_bg_img);

      clutter_actor_hide (current_view_data->view);
    }
}

static void
view_entry_animation_completed (ThornburyViewManager *view_manager,
                                const gchar *new_view,
                                gpointer user_data)
{
  ThornburyViewManagerPrivate *priv;
  ThornburyViewManagerViewData *current_view_data;
  ThornburyViewManagerViewData *newview;

  v_on_entry_animation_complete *v_plugin_entry_animation_complete = NULL;

  /* FIXME: This signal is emitted by Canterbury */
  g_return_if_fail (new_view != NULL && *new_view != '\0');

  priv = thornbury_view_manager_get_instance_private (view_manager);

  /* Todo:
   * check if this signal is emited twice
   */
  current_view_data = thornbury_get_view_data (view_manager, priv->current_view);
  newview = thornbury_get_view_data (view_manager, new_view);

  /* FIXME: v_on_entry_animation_complete can be g_signal */
  if (g_module_symbol (priv->module,"v_on_entry_animation_complete",
                       (gpointer *) &v_plugin_entry_animation_complete))
    {
      v_plugin_entry_animation_complete (view_manager, current_view_data, newview );
    }
  else
    {
      thornbury_remove_clone (view_manager, current_view_data->view);
      if (NULL != priv->trans_bg_img)
        clutter_actor_hide (priv->trans_bg_img);
      clutter_actor_show (newview->view);
    }

  g_clear_pointer (&priv->current_view, g_free);
  priv->current_view = g_strdup (newview->view_name);
  g_signal_emit (G_OBJECT (view_manager),
                 view_manager_signals[SIGNAL_SWITCH_END], 0,
                 priv->current_view);
}

/**
 * thornbury_set_view_switch_animation:
 * @view_manager: A #ThornburyViewManager object reference.
 * @view_switch_animations: #GHashTable having key as view name and #ThornburyViewManagerViewSwitchAnimData as value.
 *
 * Sets the animation mode for view specified. Can be used to mention exit and entry view animations.
 *
 * Since: 1.0
 */
void  thornbury_set_view_switch_animation(ThornburyViewManager *view_manager,GHashTable *view_switch_animations)
{
	GHashTableIter widgetiter;
	gpointer strwidgetname = NULL;
	gpointer anim = NULL;

	g_hash_table_iter_init (&widgetiter, view_switch_animations);
	while (g_hash_table_iter_next (&widgetiter, &strwidgetname, &anim))
	{
		ThornburyViewManagerViewSwitchAnimData *anim_data = (ThornburyViewManagerViewSwitchAnimData *)anim;
		ThornburyViewManagerViewData *view_data = thornbury_get_view_data (view_manager, strwidgetname);
		if(NULL != view_data)
		{
			if(NULL != anim_data)
			{
				if(NULL != anim_data->entry_effect)
				{
					g_clear_pointer (&view_data->entry_effect, g_free);
					view_data->entry_effect = g_strdup(anim_data->entry_effect);
				}
				if(NULL != anim_data->exit_effect)
				{
					g_clear_pointer (&view_data->exit_effect, g_free);
					view_data->exit_effect = g_strdup(anim_data->exit_effect);
				}
			}
		}
	}

}


/**
 * thornbury_get_current_view:
 * @view_manager: The instance of #ThornburyViewManager
 *
 * Function to get the current view name.
 *
 * Returns: (nullable): The current view name.
 * The returned value is owned by #ThornburyViewManager Object and should not be freed.
 *
 * Since: 1.0
 *
 */
const gchar *
thornbury_get_current_view (ThornburyViewManager *view_manager)
{
  ThornburyViewManagerPrivate *priv;

  g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), NULL);

  priv = thornbury_view_manager_get_instance_private (view_manager);

  return priv->current_view;
}




#if ADD_DEBUG_BACK_BUTTON

gboolean b_view_manager_debug_back_button (ClutterActor *actor,
		ClutterEvent *event,
		gpointer      user_data)
{
	ThornburyViewManager *view_manager = thornbury_get_vm_ptr();

	g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), FALSE);

        /* check if viewmangaer has to handle the back presss or application */
        view_manager_check_for_back_handling (view_manager);

	return TRUE;
}

#endif

/*
 * default backbutton-continue signal handler
 *
 * When it returns FALSE, remaining actions will be ignored.
 */
static gboolean
thornbury_view_manager_backbutton_continue (ThornburyViewManager *self,
                                            const gchar *view_name)
{
  return TRUE;
}

static void
thornbury_view_manager_constructed (GObject *obj)
{
  ThornburyViewManager *self = THORNBURY_VIEW_MANAGER (obj); 
  ThornburyViewManagerPrivate* priv = thornbury_view_manager_get_instance_private (self);
  gchar *default_background_path = NULL;
  gchar *transition_background_path = NULL;
  gchar *library_path = NULL;
  v_vm_plugin_init *fp_plugin_init_func = NULL;
  GError *tmp_error = NULL;

  G_OBJECT_CLASS (thornbury_view_manager_parent_class)->constructed (obj);

  priv->settings = g_settings_new (THORNBURY_VIEWMANAGER_SCHEMA_ID);
  g_object_get (priv->settings,
    "settings-schema", &priv->schema,
     NULL);

  default_background_path = g_settings_get_string (priv->settings, VM_SCHEMA_KEY_BACKGROUND_IMG);

  priv->fltAppBgX = (gfloat) g_settings_get_uint (priv->settings, VM_SCHEMA_KEY_VIEW_BG_X);
  priv->fltAppBgY = (gfloat) g_settings_get_uint (priv->settings, VM_SCHEMA_KEY_VIEW_BG_Y);

  priv->pGlobalAppBg = create_image (default_background_path, -1, -1);

  if (priv->pGlobalAppBg != NULL)
    {
      clutter_actor_set_position (priv->pGlobalAppBg,
                                  priv->fltAppBgX,
                                  priv->fltAppBgY);

      clutter_actor_set_name (priv->pGlobalAppBg, "background");

      clutter_actor_add_child (priv->app, priv->pGlobalAppBg);
    }

  g_free (default_background_path);

  transition_background_path = g_settings_get_string (priv->settings, VM_SCHEMA_KEY_VIEW_BACKGROUND_IMG);

  priv->trans_bg_img = create_image (transition_background_path, -1, -1);

  if (priv->trans_bg_img != NULL)
    {
      clutter_actor_set_position (priv->trans_bg_img, 0.0, 0.0);
      clutter_actor_set_name (priv->trans_bg_img, "transition_background");
      clutter_actor_add_child (priv->app, priv->trans_bg_img);
      clutter_actor_hide (priv->trans_bg_img);
    }

  g_free (transition_background_path);

  priv->screen_width = g_settings_get_uint (priv->settings, VM_SCHEMA_KEY_SCREEN_WIDTH);
  priv->screen_height = g_settings_get_uint (priv->settings, VM_SCHEMA_KEY_SCREEN_HEIGHT);

  /* FIXME: Should we handle enabling screenshot by settings? property is not enough? */
  priv->bEnableScreenShot = g_settings_get_boolean (priv->settings, VM_SCHEMA_KEY_ENABLE_SCREENSHOT);

  priv->canterbury_bus_id =
    g_bus_watch_name (G_BUS_TYPE_SESSION,
                      CANTERBURY_BUS_NAME,
                      G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                      view_manager_hard_keys_name_appeared,
                      view_manager_hard_keys_name_vanished,
                      g_object_ref (self), g_object_unref);

  g_signal_connect (self, "view_switch_exit_anim_completed",
    G_CALLBACK (view_exit_animation_completed),
    NULL);

  g_signal_connect (self, "view_switch_entry_anim_completed",
    G_CALLBACK (view_entry_animation_completed),
    NULL);

  /* FIXME: needs to implement GInitable */
  view_manager_create_view (self, &tmp_error);

  if (tmp_error != NULL)
    {
      g_warning ("VIEW MANAGER: error %s", tmp_error->message);
      g_error_free (tmp_error);
      return;
    }

  /* FIXME: Can't we use signal? */
  library_path = g_settings_get_string (priv->settings, VM_SCHEMA_KEY_LIBPATH);
  priv->module = g_module_open (library_path, G_MODULE_BIND_MASK);

  if (priv->module != NULL
      && g_module_symbol(priv->module, "v_vm_plugin_init", (gpointer *)&fp_plugin_init_func))
    {
      /* Give the control to the view manager plugin to carry out any particular task on init*/
      fp_plugin_init_func (self);
    }

  g_free (library_path);
}

/**
 * thornbury_view_manager_close_widgets:
 * @view_manager: The instance of #ThornburyViewManager
 * @view_name: The name of view
 *
 * Set close property as 1 if the type of containing widgets is one of
 * `MildenhallViewsDrawer` and `MildenhallContextDrawer`
 */
void
thornbury_view_manager_close_widgets (ThornburyViewManager *view_manager,
                                      const gchar *view_name)
{
  ThornburyViewManagerPrivate* priv;
  GHashTableIter widgetiter;
  gpointer strwidgetname = NULL;
  gpointer widget = NULL;
  GHashTable *widgets = NULL;

  const gchar *widget_type_name = NULL;
  ThornburyViewManagerWidgetData *widget_data = NULL;

  g_return_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager));
  g_return_if_fail (view_name != NULL && *view_name != '\0');

  priv = thornbury_view_manager_get_instance_private (view_manager);

  widgets = g_hash_table_lookup (priv->viewsStructure, view_name);

  if (widgets == NULL)
    {
      /* FIXME: more error handling is required. */
      return;
    }

  g_hash_table_iter_init (&widgetiter, widgets);

  while (g_hash_table_iter_next (&widgetiter, &strwidgetname, &widget))
    {
      widget_data = widget;

      if (widget_data == NULL || widget_data->widget == NULL)
        {
          continue;
        }

      widget_type_name = G_OBJECT_TYPE_NAME (widget_data->widget);

      /* FIXME: Can we assume there are only two types? */
      if (!g_strcmp0 ("MildenhallViewsDrawer", widget_type_name)
        || !g_strcmp0 ("MildenhallContextDrawer", widget_type_name))
        {
          g_object_set (G_OBJECT (widget_data->widget), "close", 1, NULL);
        }
    }
}

/**
 * thornbury_view_manager_get_actor_by_typename:
 * @view_manager: The instance of #ThornburyViewManager
 * @view_name: The name of view
 * @type_name: The type name of widget
 *
 * Getting #ClutterActor by given view information
 *
 * Returns: (transfer none): #ClutterActor
 */
ClutterActor *
thornbury_view_manager_get_actor_by_typename (ThornburyViewManager *view_manager,
                                              const gchar *view_name,
                                              const gchar *type_name)
{
  ThornburyViewManagerPrivate* priv;
  GHashTableIter iter;
  gpointer key = NULL;
  gpointer value = NULL;
  GHashTable *widgets = NULL;
  ThornburyViewManagerWidgetData *widget_data = NULL;

  g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), NULL);
  g_return_val_if_fail (view_name != NULL && *view_name != '\0', NULL);
  g_return_val_if_fail (type_name != NULL && *type_name != '\0', NULL);

  priv = thornbury_view_manager_get_instance_private (view_manager);

  widgets = g_hash_table_lookup (priv->viewsStructure, view_name);

  g_hash_table_iter_init (&iter, widgets);

  while (g_hash_table_iter_next (&iter, &key, &value))
    {
      widget_data = (value);

      /* FIXME: Shouldn't we need to try again? */
      if (NULL == key || NULL == value)
        {
          return NULL;  
        }

      if (widget_data->widget == NULL)
        {
          continue;
        }

      /* FIXME: Can we use GObject? */
      if (!g_strcmp0 (widget_data->widget_data->pWidgetType, type_name))
        {
          return widget_data->widget;
        }
    }

  return NULL;
}

/**
 * thornbury_view_manager_get_actor_list_by_typename:
 * @view_manager: The instance of #ThornburyViewManager
 * @view_name: The name of view
 * @type_name: The type name of widget
 *
 * Getting #ClutterActor by given view information
 *
 * Returns: (element-type ClutterActor) (transfer container) (nullable): The list of #ClutterActor
 */
GList *
thornbury_view_manager_get_actor_list_by_typename (ThornburyViewManager *view_manager,
                                                   const gchar *view_name,
                                                   const gchar *type_name)
{
  ThornburyViewManagerPrivate* priv;
  GHashTableIter iter;
  gpointer key = NULL;
  gpointer value = NULL;
  GHashTable *widgets = NULL;
  ThornburyViewManagerWidgetData *widget_data = NULL;
  GList *actor_list = NULL;

  g_return_val_if_fail (THORNBURY_IS_VIEW_MANAGER (view_manager), NULL);
  g_return_val_if_fail (view_name != NULL && *view_name != '\0', NULL);
  g_return_val_if_fail (type_name != NULL && *type_name != '\0', NULL);

  priv = thornbury_view_manager_get_instance_private (view_manager);

  widgets = g_hash_table_lookup (priv->viewsStructure, view_name);

  g_hash_table_iter_init (&iter, widgets);

  while (g_hash_table_iter_next (&iter, &key, &value))
    {
      widget_data = (value);

      if (NULL == key || NULL == value || widget_data->widget == NULL)
        {
          continue;
        }

      /* FIXME: Can we use GObject? */
      if (!g_strcmp0 (widget_data->widget_data->pWidgetType, type_name))
        {
          actor_list = g_list_append (actor_list, widget_data->widget);
        }
    }

  return actor_list;
}
