/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:thornbury-viewmanager/libthornbury-viewmanager.h
 * @title: ThornburyViewManager
 */

#ifndef __THORNBURY_VIEW_MANAGER_H__
#define __THORNBURY_VIEW_MANAGER_H__

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include <glib-object.h>
#include "libthornbury-viewparser.h"
#include "libthornbury-itemfactory.h"
#include "libthornbury-model.h"
//#include "preference_manager_fi.h"
#include "libthornbury-language.h"
#include "seaton-preference.h"

G_BEGIN_DECLS

#define THORNBURY_TYPE_VIEW_MANAGER thornbury_view_manager_get_type ()
G_DECLARE_DERIVABLE_TYPE (ThornburyViewManager, thornbury_view_manager, THORNBURY, VIEW_MANAGER, GObject) 


/**
 * ThornburyViewManagerClass:
 *
 * Base class of #ThornburyViewManager widget .
*/
struct _ThornburyViewManagerClass {
  /*< private >*/
  GObjectClass parent_class;

  /*< public >*/
  void (*view_created)                  (ThornburyViewManager *self);
  void (*switch_begin)                  (ThornburyViewManager *self,
                                         const gchar *new_view);
  void (*switch_end)                    (ThornburyViewManager *self,
                                         const gchar *new_view);
  void (*exit_animation_completed)      (ThornburyViewManager *self,
                                         const gchar *current_view,
                                         const gchar *new_view);
  void (*entry_animation_completed)     (ThornburyViewManager *self,
                                         const gchar *new_view);
  gboolean (*backbutton_continue)       (ThornburyViewManager *self,
                                         const gchar *view_name);
} ;

/**
 * ThornburyViewManagerAppData:
 * @app: main #ClutterActor object of application
 * @default_view: #gchar string which indicates default view that has to be created on start-up
 * @app_name: string containing Application run time name
 * @viewspath: Resource directory path to find the View-Json files
 * @lang_basefolderpath: Directory path where language files are found
 * @lang_basefilename: Base file name of language directory
 * @stage: a #ClutterActor stage of application
 * @pAppBackFunc: Call back function for controlling BACK press
 * @pScreenShotPath: Path where the screen shot needs to be stored
 *
 *
 * Required data which needs to be provided by the application while creating the #ThornburyViewManager object
 */
typedef struct _ThornburyViewManagerAppData {

	ClutterActor *app;
	gchar *default_view;
	gchar *app_name;
	gchar *viewspath;
	gchar *lang_basefolderpath;
	gchar *lang_basefilename;
	ClutterStage *stage;
	gpointer pAppBackFunc;
	gchar *pScreenShotPath;
 

} ThornburyViewManagerAppData;


/*
 * ThornburyVmActionOnBackPress:
 * @THORNBURY_PROCEED_WITH_ANIMATION: Load previous view with pre-defined animations.
 * @THORNBURY_PROCEED_WITHOUT_ANIMATION: Load previous view without any animations.
 * @THORNBURY_IGNORE_VIEW_SWITCH: Send back-consumed-ack while ignoring the request to switch view in case of any
 * on-going view switch.
 *
 * Enum values which define the view-behavior when back-button is pressed.
 *
 */
typedef enum {
	THORNBURY_PROCEED_WITH_ANIMATION,
	THORNBURY_PROCEED_WITHOUT_ANIMATION,
	THORNBURY_IGNORE_VIEW_SWITCH
} ThornburyVmActionOnBackPress;

/**
 * ThornburyViewManagerSignalData:
 * @signalname: Name of the signal to connect.
 * @func: Callback function for the signal.
 * @data: User data that can be passed while connecting to signal.
 *
 * Data structure having necessary information for signal registration with widgets.
 */
typedef struct _ThornburyViewManagerSignalData {
	gchar *signalname;
	GCallback func;
	gpointer data;
} ThornburyViewManagerSignalData;


/**
 * ThornburyViewManagerAnimData:
 * @view_name: Name of the view.
 * @signal_name: Widget Signal .
 * @effect: Animation effect.
 *
 * Data structure having necessary information for animation customization for view and widget.
 *
 */
typedef struct _ThornburyViewManagerAnimData {
	gchar *view_name;
	gchar *signal_name;
	gchar *effect;
} ThornburyViewManagerAnimData;

/**
 * ThornburyViewManagerViewSwitchAnimData:
 * @view_name: Name of the view.
 * @exit_effect: Specify the exit effect to be carried while performing view exit.
 * @entry_effect: Specify the entry effect to be carried while performing view entry.
 *
 * Animation customization for view specifying the entry and exit animations.
 */

typedef struct _ThornburyViewManagerViewSwitchAnimData {
	gchar *view_name;
	gchar *exit_effect;
	gchar *entry_effect;
} ThornburyViewManagerViewSwitchAnimData;

/**
 * ThornburyViewManagerWidgetData:
 * @widgetname: Name of the widget.
 * @widget: a #ClutterActor pointer of  widget.
 * @widget_data: #ThornburyViewWidgets having info about widget position,name and layer.
 * @anim_data: Pointer to #ThornburyViewManagerAnimData having animation data.
 *
 * View Manager Internal widget data helpful for developing project customization plugins.
 */
typedef struct _ThornburyViewManagerWidgetData {
	gchar *widgetname;
	ClutterActor *widget;
	ThornburyViewWidgets *widget_data;
	ThornburyViewManagerAnimData *anim_data;
} ThornburyViewManagerWidgetData;

/**
 * ThornburyViewManagerViewData:
 * @view_data: #ThornburyView structure having Information about the view as parsed from View-Json file.
 * @view: a #ClutterActor pointer of  view.
 * @view_file_path: Path of the parsed View-Json file.
 * @view_name: Name of the view as parsed in the View-Json file.
 * @entry_effect: Entry effect associated with the View as provided by the application at run-time.
 * @exit_effect: Exit effect associated with the View as provided by the application at run-time.
 * @bViewCreated: #gboolean indicating whether the view is created or not
 *
 * View Manager Internal View related data helpful for developing project customization plugins.
 */
typedef struct _ThornburyViewManagerViewData {
	ThornburyView *view_data;
	ClutterActor *view;
	gchar *view_file_path;
	gchar *view_name;
	gchar *entry_effect;
	gchar *exit_effect;
	gboolean bViewCreated ;
} ThornburyViewManagerViewData;

/**
 * ThornburyViewManagerError:
 * @THORNBURY_VIEW_MANAGER_OK: No Error.
 * @THORNBURY_VIEW_MANAGER_INVALID_DIR: Invalid Directory.
 * @THORNBURY_VIEW_MANAGER_INVALID_FILE: Invalid File.
 * @THORNBURY_VIEW_MANAGER_INIT_ERROR: Error initializing the #ThornburyViewManager object.
 * @THORNBURY_VIEW_MANAGER_INVALID_VIEW_MGR: Invalid #ThornburyViewManager.
 * @THORNBURY_VIEW_MANAGER_INVALID_APP_DATA: Invalid parameters passed by the application.
 * @THORNBURY_VIEW_MANAGER_SET_PROP_ERR: Error setting the property to  widget.
 * @THORNBURY_VIEW_MANAGER_INVALID_WIDGET: Invalid widget name specified.
 *
 * View Manager Error codes.
 */

typedef enum  {
	THORNBURY_VIEW_MANAGER_OK,
	THORNBURY_VIEW_MANAGER_INVALID_DIR,
	THORNBURY_VIEW_MANAGER_INVALID_FILE,
	THORNBURY_VIEW_MANAGER_INIT_ERROR,
	THORNBURY_VIEW_MANAGER_INVALID_VIEW_MGR,
	THORNBURY_VIEW_MANAGER_INVALID_APP_DATA,
	THORNBURY_VIEW_MANAGER_SET_PROP_ERR,
	THORNBURY_VIEW_MANAGER_INVALID_WIDGET

} ThornburyViewManagerError;

ThornburyViewManager *thornbury_view_manager_new (ThornburyViewManagerAppData *app_data);
ThornburyViewManagerError thornbury_set_widgets_models(ThornburyViewManager *view_manager,GHashTable *models);
ThornburyViewManagerError thornbury_set_widgets_controllers(ThornburyViewManager *view_manager,GHashTable *signals);
ThornburyViewManagerError thornbury_set_widgets_animation(ThornburyViewManager *view_manager,GHashTable *animations);
void thornbury_set_view_switch_animation(ThornburyViewManager *view_manager,GHashTable *view_switch_animations);
ThornburyViewManagerError thornbury_build_all_views(ThornburyViewManager *view_manager);
ThornburyViewManagerError thornbury_build_view_by_name(ThornburyViewManager *view_manager, gchar *viewname);
void thornbury_switch_view(ThornburyViewManager *view_manager,gchar *view_to_switch, gboolean addtohistory,guint iSelected_row);
void thornbury_switch_view_no_animation(ThornburyViewManager *view_manager,gchar *view_to_switch, gboolean addtohistory,guint iSelected_row);
void thornbury_load_previous_view(ThornburyViewManager *view_manager);
void thornbury_enable_back_handling(ThornburyViewManager *view_manager,gboolean flag);

GHashTable *thornbury_get_property                              (ThornburyViewManager *view_manager,
                                                                 const gchar *widget_name,
                                                                 ...) G_GNUC_NULL_TERMINATED;

ThornburyViewManagerError thornbury_set_property                (ThornburyViewManager *view_manager,
                                                                 const gchar *widget_name,
                                                                 ...) G_GNUC_NULL_TERMINATED;

ThornburyViewManagerWidgetData * thornbury_get_widget_data_view (ThornburyViewManager *view_manager,
                                                                 const gchar *widgetname,
                                                                 const gchar *viewname);

ThornburyViewManager *thornbury_get_vm_ptr (void);

const gchar *thornbury_get_current_view (ThornburyViewManager *view_manager);

void thornbury_clone_widgets (ThornburyViewManager *view_manager,
                              ThornburyViewManagerViewData *current_view_data,
                              ThornburyViewManagerViewData *new_view_data);

void thornbury_remove_clone (ThornburyViewManager *view_manager, ClutterActor *view);


ThornburyViewManagerError thornbury_build_and_restore_last_view(ThornburyViewManager *view_manager , gchar **pReturnRestoredView , GHashTable **pReturnAppDataHash);
void thornbury_view_manager_store_view_order_before_finishing(const GHashTable *pStoreAppDataHash) ;

void thornbury_view_manager_close_widgets (ThornburyViewManager *view_manager,
                                           const gchar *view_name);

ClutterActor *thornbury_view_manager_get_actor_by_typename (ThornburyViewManager *view_manager,
                                                            const gchar *view_name,
                                                            const gchar *type_name);


GList * thornbury_view_manager_get_actor_list_by_typename (ThornburyViewManager *view_manager,
                                                           const gchar *view_name,
                                                           const gchar *type_name);

G_END_DECLS
#endif /* __VIEW_MANAGER_H__ */
