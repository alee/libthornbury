/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "libthornbury-viewmanager.h"
#include "libthornbury-viewmanager-internal.h"

static const gchar* pViewManagerDbFields[] = {
		VM_DBKEY_VIEW_ORDER ,
		VM_DBKEY_VIEW_NAME ,
		NULL
};

static const gchar* pVmAppDataDbFields[] = {
		VM_APPDATA_DBKEY_KEY ,
		VM_APPDATA_DBKEY_VALUE ,
		NULL
};

/********************************************************
 * Function : v_view_manager_create_view_stack_db_table
 * Parameters: @pAppName :  Application  name
 *
 * Description:  This function will create the table in application specific
 * 				 view manager db for storing the view stack of app before
 * 				 going to background/killed state
 *
 ********************************************************/

static void v_view_manager_create_view_stack_db_table(gchar *pAppName)
{
	ThornburyViewManager *pViewManager = thornbury_get_vm_ptr();
	SeatonFieldParam *pDBFieldName;
	SeatonPreference *seaton;
	gint iInstallResult;

	/* create DB for settings app. */
	guint uinTotalFields = 0 , uinDbFields = 0;
	while(NULL != pViewManagerDbFields[uinDbFields])
	{
		uinDbFields++ ;
	}
	uinTotalFields = uinDbFields ;
	VIEW_MANAGER_PRINT("Total rows = %d \n",uinTotalFields);
	uinDbFields = 0;
	pDBFieldName = g_malloc_n (sizeof (SeatonFieldParam), uinTotalFields);
	for(  ; NULL != pViewManagerDbFields[uinDbFields] ; uinDbFields++)
	{
		pDBFieldName[uinDbFields].pKeyName = g_new0(gchar, strlen(pViewManagerDbFields[uinDbFields]) + 3);
		strcpy(pDBFieldName[uinDbFields].pKeyName,pViewManagerDbFields[uinDbFields]);
		pDBFieldName[uinDbFields].inFieldType= SQLITE_TEXT;
	}

	g_object_get (pViewManager, "seaton", &seaton, NULL);
	iInstallResult =  seaton_preference_install (seaton,
		VM_DBKEY_VIEW_STACK_TABLE,
		pDBFieldName, uinTotalFields);

	if( iInstallResult )
	{
		g_warning("View Manager DB creation failed for %s Application\n" , pAppName);
	}
	g_free (pDBFieldName);
	g_object_unref (seaton);
}

/********************************************************
 * Function : v_view_manager_create_app_data_db_table
 * Parameters: @pAppName :  Application  name
 *
 * Description:  This function will create the table in application specific
 * 				 view manager db for storing the app data of app before
 * 				 going to background/killed state.
 *
 ********************************************************/
static void v_view_manager_create_app_data_db_table(gchar *pAppName)
{
	ThornburyViewManager *pViewManager = thornbury_get_vm_ptr();
	guint uinTotalFields = 0 , uinDbFields = 0;
	SeatonFieldParam *pDBFieldName;
	SeatonPreference *seaton;
	gint iInstallResult;

	while(NULL != pVmAppDataDbFields[uinDbFields])
	{
		uinDbFields++ ;
	}
	uinTotalFields = uinDbFields ;
	VIEW_MANAGER_PRINT("Total rows = %d \n",uinTotalFields);
	uinDbFields = 0;
	pDBFieldName = g_malloc_n (sizeof (SeatonFieldParam), uinTotalFields);
	for(  ; NULL != pVmAppDataDbFields[uinDbFields] ; uinDbFields++)
	{
		pDBFieldName[uinDbFields].pKeyName = g_new0(gchar, strlen(pVmAppDataDbFields[uinDbFields]) + 3);
		strcpy(pDBFieldName[uinDbFields].pKeyName,pVmAppDataDbFields[uinDbFields]);
		pDBFieldName[uinDbFields].inFieldType= SQLITE_TEXT;
	}

	g_object_get (pViewManager, "seaton", &seaton, NULL);
	iInstallResult = seaton_preference_install (seaton,
		VM_DBKEY_APP_DATA_TABLE,
		pDBFieldName, uinTotalFields);

	if( iInstallResult )
	{
		g_warning("View Manager DB creation failed for %s Application\n" , pAppName);
	}

	g_free (pDBFieldName);
	g_object_unref (seaton);
}

/********************************************************
 * Function : v_view_manager_free_retrived_db_contents
 * Parameters: @gpArray :  GPtrArray pointer reference returned by PDI calls.
 *
 * Description:  This function will free the contents retrieved during PDI calls.
 *
 ********************************************************/
static void v_view_manager_free_retrived_db_contents(GPtrArray *gpArray)
{
	/*free The memory after the use	 */
	if(NULL != gpArray)
	{
		int inTabSze = gpArray->len;
		GHashTable		*pHashChk = NULL;
		gint inNumRows ;

		for (inNumRows = 0; inNumRows < inTabSze; inNumRows++)
		{
			pHashChk = g_ptr_array_index(gpArray , inNumRows);
			if(pHashChk != NULL)
			{
				/* TODO :
				 * key value pair has to be cleared explicitly because free function is not defined while creating Hashtable in PDI
				 */
				g_hash_table_destroy(pHashChk);
				pHashChk = NULL;
			}
		}
		/* frees the Gpointer */
		g_ptr_array_free (gpArray , TRUE);
	}
}


/********************************************************
 * Function : v_view_manager_free_retrived_db_contents
 * Parameters:
 * Return value: pointer to GList containing the view order as strored in DB.
 * Description:  This function will read the view order from the DB and will
 * 				 return the same in linked list.
 *
 ********************************************************/
static GList *
p_view_manager_read_view_order_from_db (void)
{
	ThornburyViewManager *pViewManager = thornbury_get_vm_ptr();
	SeatonPreference *seaton;
	GList *pViewOrderList = NULL ;
	gchar *pViewName = NULL ;
	GPtrArray *gpArray = NULL;

	g_object_get (pViewManager, "seaton", &seaton, NULL);
	/* Read the entire Table from db */
	gpArray = seaton_preference_get_multiple(seaton,
			VM_DBKEY_VIEW_STACK_TABLE, NULL, NULL);

	/* check if the array size is 0 */
	if(gpArray != NULL && 0 != gpArray->len)
	{
		GHashTable	*pHashInArray;
		guint inCurrentRow;

		for (inCurrentRow = 0; inCurrentRow < (gpArray->len); inCurrentRow++)
		{
			pHashInArray = g_ptr_array_index(gpArray , inCurrentRow);
			pViewName  =  g_hash_table_lookup(pHashInArray,VM_DBKEY_VIEW_NAME);
			if(NULL != pViewName)
			{
				/* TODO : Call free function during dispose or after usage with g_free */
				pViewOrderList = g_list_prepend(pViewOrderList , g_strdup(pViewName));
			}
		}
		/* free The memory after the use */
		v_view_manager_free_retrived_db_contents(gpArray);
	}
	g_object_unref (seaton);

	return pViewOrderList ;
}



/********************************************************
 * Function : v_thornbury_view_manager_create_application_pdi
 * Parameters: @pAppName : Application Name
 * Return value: void
 * Description:  	This function will create the view manager specific
 * 					PDI for Application	to store the view stack of the
 * 					application and any app-data with key value pair
 * 					before termination.
 *
 ********************************************************/

void v_thornbury_view_manager_create_application_pdi(gchar *pAppName)
{
	ThornburyViewManager *pViewManager = thornbury_get_vm_ptr();
	SeatonPreference *seaton;
	gint iDBStatus;
	gchar *pDbName;

	seaton = seaton_preference_new();
	g_object_set (pViewManager, "seaton", seaton, NULL);
	pDbName = g_strdup_printf("%s%s" , pAppName , VIEW_MANAGER_PDI_SUFFIX );
	iDBStatus = seaton_preference_open(seaton,
		pAppName, pDbName,
		SEATON_PREFERENCE_APP_USER_DB);

	if (iDBStatus)
	{
		g_warning("\n View Manager Data Base Creation failed for %s application !\n" , pAppName);
	}
	v_view_manager_create_view_stack_db_table(pAppName);
	v_view_manager_create_app_data_db_table(pAppName);
	g_free(pDbName);
	pDbName = NULL ;
	g_object_unref (seaton);
}




/********************************************************
 * Function : p_thornbury_view_manager_restore_any_non_default_view
 * Parameters: @pAppName : Application Name
 * Return value: name of the view shown.
 * Description:  	This function will look in view stack and will
 * 					build and show the top in stack view.
 *
 ********************************************************/

gchar *
p_thornbury_view_manager_restore_any_non_default_view (ThornburyViewManager *pViewManager,
                                                       GList **view_history)
{
	SeatonPreference *seaton;
	gchar *default_view;
	gchar *pRestoredView = NULL ;
	GList *pViewOrderList = p_view_manager_read_view_order_from_db();
	gint inQueryResult;
	gchar *pViewInTopStack = NULL ;

	/* clear the view stack if any after reading it once */
	g_object_get (pViewManager, "seaton", &seaton, NULL);
	inQueryResult = seaton_preference_remove (seaton,
		VM_DBKEY_VIEW_STACK_TABLE,
		NULL, NULL);

	if(inQueryResult)
	{
		g_warning(" FAILED to remove the view stack after fetch \n");
	}

	if(NULL != pViewOrderList )
	{
		gint inStackSize = g_list_length(pViewOrderList);
		pViewInTopStack = g_list_nth_data(pViewOrderList , inStackSize -1);
		VIEW_MANAGER_PRINT("VIEW MANAGER : %s  %d  pViewInTopStack = %s  \n",__FUNCTION__ , __LINE__ , pViewInTopStack);

		/* check if last view was same as that of default view */
#if DISABLE_BUILD_BY_NAME
		g_object_get (pViewManager, "default-view-name", &default_view, NULL);
		if (NULL != pViewInTopStack && 0 != g_strcmp0 (pViewInTopStack, default_view))
#endif
		{
			VIEW_MANAGER_PRINT("VIEW MANAGER : %s  %d \n",__FUNCTION__ , __LINE__);
#if DISABLE_BUILD_BY_NAME
			/* If different load the last view by overriding default view */
			thornbury_switch_view_no_animation(pViewManager , pViewInTopStack , FALSE , -1 ) ;
#else
			build_view_by_name(pViewManager , pViewInTopStack);
			pViewData = get_view_data(pViewInTopStack);
			v_view_manager_map_gloabl_widgets(pViewData);
			clutter_actor_show(pViewData->view );

#endif
			/* restore this view order for further need */
			g_object_set (pViewManager, "current-view-name", pViewInTopStack, NULL);
			/* check if the list is non empty and free the list itself before reassigning */
			g_list_free_full (*view_history, g_free);
			*view_history = g_list_first (pViewOrderList);
		}
		pRestoredView = g_strdup(pViewInTopStack) ;

#if DISABLE_BUILD_BY_NAME
		g_free (default_view);
#endif
	}

	g_object_unref (seaton);

	return pRestoredView ;
}


/********************************************************
 * Function : p_thornbury_view_manager_read_app_data_from_db
 * Parameters:
 * Return value: GHashTable containing key value pair as read from the DB.
 * Description:  	This function will read the AppData table from DB and will
 * 					provide the App data in Key value pair.
 *
 ********************************************************/
GHashTable *
p_thornbury_view_manager_read_app_data_from_db (void)
{
	ThornburyViewManager *pViewManager = thornbury_get_vm_ptr();
	GHashTable *pAppDataHash = g_hash_table_new_full(g_str_hash,g_str_equal , g_free , g_free );
	SeatonPreference *seaton;
	GPtrArray *gpArray;
	gchar *pKey = NULL ;
	gchar *pValue = NULL ;

	g_object_get (pViewManager, "seaton", &seaton, NULL);
	gpArray = seaton_preference_get_multiple (seaton,
		VM_DBKEY_APP_DATA_TABLE,
		NULL, NULL);

	/* check if the array size is 0 */
	if(gpArray != NULL && 0 != gpArray->len)
	{
		GHashTable *pHashInArray;
		guint inCurrentRow;
		gint inQueryResult;

		for (inCurrentRow = 0; inCurrentRow < (gpArray->len); inCurrentRow++)
		{
			pHashInArray = g_ptr_array_index(gpArray , inCurrentRow);
			pKey  =  g_hash_table_lookup(pHashInArray,VM_APPDATA_DBKEY_KEY);
			pValue = g_hash_table_lookup(pHashInArray,VM_APPDATA_DBKEY_VALUE);

			/* Value may be null but if in any case key is null then ignore adding */
			if(NULL != pKey)
				g_hash_table_insert(pAppDataHash ,pKey , pValue);
		}
		/* free The memory after the use */
		v_view_manager_free_retrived_db_contents(gpArray);

		inQueryResult = seaton_preference_remove (seaton,
			VM_DBKEY_APP_DATA_TABLE,
			NULL, NULL);

		if(inQueryResult)
		{
			g_warning(" FAILED to remove the app data table  after fetch \n");
		}


	}

	g_object_unref (seaton);

	return pAppDataHash ;
}

void
v_thornbury_view_manager_take_app_screenshot (void)
{
	ThornburyViewManager *pViewManager = thornbury_get_vm_ptr();
	ClutterStage *stage;
	gchar *screenshot_location;

	g_object_get (pViewManager, "clutter-stage", &stage, NULL);
	g_object_get (pViewManager, "screenshot-location", &screenshot_location, NULL);
	if (stage != NULL)
	{
		gfloat fltWidth , fltHeight ;
		gint inXStartPos = 0 , inYStartPos = 0 ;
		gchar *pScreenShotName;
		gchar* pFileName;
		gchar *app_name;

		g_object_get (pViewManager, "app-name", &app_name, NULL);
		clutter_actor_get_size (CLUTTER_ACTOR (stage), &fltWidth, &fltHeight);
		/* FIXME: Can we assume that 'app_name' is always valid? */
		pScreenShotName = g_strdup_printf ("/%s_screenshot.png", app_name) ;
		pFileName = g_build_filename (screenshot_location, pScreenShotName, NULL);

		thornbury_texture_get_stage_screenshot (stage,
			inXStartPos, inYStartPos,
			(gint)fltWidth, (gint)fltHeight,
			pFileName);
		g_clear_pointer (&pScreenShotName, g_free);
		g_clear_pointer (&pFileName, g_free);
		g_free (app_name);
	}
	g_free (screenshot_location);
	g_object_unref (stage);
}

