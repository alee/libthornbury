/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:thornbury-widgetstyler/libthornbury-widgetstyler.h
 * @title: ThornburyWidgetStyler
 * @short_description: Parser for widget styler.
 *
 * This is a json parser for widget style sheet. Style sheet
 * is used for theming of widget and background image,color which varies only
 * for a Thornbury variant.
 */

#ifndef _STYLE_H
#define _STYLE_H

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include <glib-object.h>
#include "libthornbury-widgetparser.h"

G_BEGIN_DECLS

GHashTable *thornbury_style_set (const gchar *style_file);

void thornbury_style_free(GHashTable *pStyleHash);

GHashTable *thornbury_style_set_from_resource (const gchar *path);

G_END_DECLS

#endif /* _STYLE_H */
