/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


/**
 * SECTION:thornbury-webtexture/libthornbury-webtexture.h
 * @title: ThornburyWebTexture
 * @short_description: Library to create texture from web(http images)
 *
 * This is the library which allows to create a texture from
 * web. it downloads the image using libcurl. 
 * It is more efficient as the image content creation is ocuuring in the thread
 * and sets the content in the main thread .
 *
 * > Make sure to call these API's in main loop.
 *
 * ## Sample C Code
 * ```C
 * //callback function
 * static void body_thumb_first_cb(ClutterActor *pActor, GError *pError, gpointer pUserData)
 * {
 *   // do something 
 *   // i can also have new image 
 *   if(pFile != NULL)
 *   {
 *     if (g_file_has_uri_scheme (pFile, "http"))
 *     {
 *       thornbury_web_texture_set_from_file(pActor, pFile, 86, 86, NULL);
 *     }
 *   }
 *   if(pActor)
 *     clutter_actor_insert_child_at_index(pThumbBodyBox, pActor, 0);
 * }
 * // call the function 
 * else if(g_file_has_uri_scheme (pFile, "http"))
 * {
 *   thornbury_web_texture_create_async (pThumbPath, 0, 0, pTile, body_thumb_first_cb);
 * }
 * ```
 */

#ifndef _WEB_TEXTURE_H
#define _WEB_TEXTURE_H

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

G_BEGIN_DECLS
typedef void(*web_image_created_cb)(ClutterActor *pimage, GError *pError, gpointer pUserData);
typedef void(*web_image_content_created_cb)(ClutterContent *pimage, GError *pError, gpointer pUserData);

void thornbury_web_texture_create_async (gchar *pFilePath, gfloat flWidth, gfloat flHeight, gpointer pData, web_image_created_cb pFunCB);
void thornbury_web_texture_create_content_async (gchar *pFilePath, gfloat flWidth, gfloat flHeight, gpointer pData, web_image_content_created_cb pFunc);
void thornbury_web_texture_set_from_file (ClutterActor *pBox, gchar *pFilePath, gfloat flWidth, gfloat flHeight, GError **pErr);
void thornbury_web_texture_cancel_request(ClutterActor *pBox, gchar *pFilePath);
G_END_DECLS
#endif /* _WEB_TEXTURE_H */
