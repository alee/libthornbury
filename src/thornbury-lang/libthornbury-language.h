/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:thornbury-lang/libthornbury-language.h
 * @title: ThornburyLanguage
 * @short_description: Language translation API
 *
 * These functions translate text into given language text.
 * Idealy In chalgrove.Service notifies any lang change from Settings
 * to ViewManager.
 *
 * There by #ThornburyViewManager trigger widgets languge property hence by
 * changing the model data of corresponding widget.
 *
 * Currently #ThornburyViewManager configures the path to look for the translation
 * file i,e .mo, using thornbury_lang_configure_AppData().
 * thornbury_lang_set_current_language() sets the locale and environment.
 *
 * thornbury_get_text() function needs to used in possible places where text has to be
 * translated. Hence whereever thornbury_get_text() is used language translation
 * occurs.
 */

#ifndef THORNBURY_LANGAUGE_H_
#define THORNBURY_LANGAUGE_H_

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include <glib.h>
#include <glib-object.h>

#ifndef THORNBURY_DISABLE_DEPRECATED
#include <libintl.h>
#endif /* THORNBURY_DISABLE_DEPRECATED */

G_BEGIN_DECLS

#ifndef THORNBURY_DISABLE_DEPRECATED

/* macro to simplfy the gettext() api usage */
/*< private >*/
#define _(STRING) gettext(STRING)

#define THORNBURY_ENGLISH_LANGUAGE    "en_US"
#define THORNBURY_ENGLISH_LOCALE_US   "en_US.utf8"

#define THORNBURY_GERMAN_LANGUAGE     "de_DE"
#define THORNBURY_GERMAN_LOCALE_DE    "de_DE.utf8"


/**
 * sApp_LangConfigData:
 *
 *  Structure to hold the relevant data associated to language path
 */
typedef struct
{
/*<public>*/
/* laguage folder path */
   gchar *pBaseFolderPath;
/* laguage file name */
   gchar *pBaseFileName;
} ThornburyApp_LangConfigData;

G_DEPRECATED_FOR(bindtextdomain) 
void thornbury_lang_configure_AppData (ThornburyApp_LangConfigData *rLangConfigData);

G_DEPRECATED_FOR(setlocale) 
void thornbury_lang_set_current_language (const gchar *pNewLanguage,
                                const gchar *pNewLocale);

G_DEPRECATED_FOR(gettext) 
gchar* thornbury_get_text( gchar* pText );

#endif /* THORNBURY_DISABLE_DEPRECATED */

G_END_DECLS

#endif /* THORNBURY_LANGUAGE_H_ */
