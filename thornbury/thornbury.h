/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __THORNBURY_H__
#define __THORNBURY_H__

#define __THORNBURY_H_INSIDE__

#include "libthornbury-ui-texture.h"
#include "libthornbury-jsonparser.h"
#include "libthornbury-webtexture.h"
#include "libthornbury-model.h"
#include "libthornbury-widgetstyler.h"
#include "libthornbury-texture.h"
#include "libthornbury-language.h"
#include "libthornbury-widgetparser.h"
#include "libthornbury-itemfactory.h"
#include "libthornbury-viewmanager.h"
#include "libthornbury-viewparser.h"
#include "libthornbury-ui-utility.h"

#endif /* __THORNBURY_H__ */
