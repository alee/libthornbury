General Information
===================

Thornbury is a library responsible for managing the applications ui utilities.

Installation
============

See the file 'INSTALL'

How to report bugs
==================

Bugs should be reported to the bug tracking system.
(http://bugs.apertis.org/, product thornbury.) You will need
to create an account for yourself.

In the bug report please include:

* Information about your system. For instance:

   - What operating system and version
   - For Linux, what version of the C library

  And anything else you think is relevant.

* How to reproduce the bug.

  If you can reproduce it with one of the test programs that are built
  in the tests/ subdirectory, that will be most convenient.  Otherwise,
  please include a short test program that exhibits the behavior.
  As a last resort, you can also provide a pointer to a larger piece
  of software that can be downloaded.

* If the bug was a crash, the exact text that was printed out
  when the crash occured.

* Further information such as stack traces may be useful, but
  is not necessary.
